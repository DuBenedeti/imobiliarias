<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'Frontend\IndexController@index')->name('home');

Route::get('/integ-portal-da-cidade-xml', 'Frontend\IntegrationsController@xml')->name('integrations.portaldacidade');


Route::get('/imoveis', 'Frontend\IndexController@properties')->name('site.imoveis');
Route::get('/imoveis/{slug}', 'Frontend\IndexController@propertyShow')->name('site.imoveis.show');
Route::get('/trabalhe-conosco', 'Frontend\IndexController@trabalheConosco')->name('site.trabalheConosco');
Route::get('/contato', 'Frontend\IndexController@contato')->name('site.contato');
Route::post('/contato-envia', 'Frontend\IndexController@contactSave')->name('site.contactSave');
Route::post('/trabalhe-conosco-envia', 'Frontend\IndexController@trabalheConoscoSave')->name('site.trabalheConoscoSave');

Route::post('/imoveis/enviar-contato/{id}', 'Frontend\IndexController@sendContactProperty')->name('send.property.contact');

Route::get('/pd/{slug}', 'Frontend\IndexController@dynamicPage')->name('site.dynamic-page.show');


Route::get('/sistema', 'Backend\LoginController@logar')->name('sistema');


Route::group(['prefix' => 'sistema'], function () {
  Route::get('/login', 'Backend\LoginController@login')->middleware('guest')->name('login');
  Route::post('/login', 'Backend\LoginController@logar');
  Route::get('/', 'Backend\DashboardController@index')->middleware('auth')->name('dashboard.index');
  Route::get('/logout', 'Backend\LoginController@logout')->middleware('auth');

  Route::group(['middleware' => 'auth'], function () {
    Route::get('/users/{cod}/edit-permission', 'Backend\UserController@editPermission')->name('users.edit.permission');
    Route::patch('/users/{cod}/edit-permission', 'Backend\UserController@updatePermission')->name('users.edit.permissionPost');
    Route::get('/users/status', 'Backend\UserController@status')->middleware('auth')->name('users.status');


    //Criando rotas para Usuários do sistema (User)
    Route::resource('/users', 'Backend\UserController', [
      'names' => [
        'index' => 'users.index',
        'store' => 'users.store',
        'edit' => 'users.edit',
        'create' => 'users.create',
        'update' => 'users.update',
        'destroy' => 'users.destroy',
      ],
      'except' => ['show']
    ]);
    /* - */

    Route::get('/users/{id}/pass', 'Backend\UserController@alt_pass')->middleware('auth')
      ->name('users.alt_pass');

    Route::patch('/users/{id}/upload', 'Backend\UserController@update_pass')->middleware('auth')
      ->name('users.update_pass');


    /*configuração*/
    Route::resource('/settings', 'Backend\SettingsController', [
      'names' => [
        'index' => 'settings.index',
        'update' => 'settings.update',
        'edit' => 'settings.edit',

      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    /* modules */
    Route::resource('/settings/ajustes', 'Backend\AjustesController', [
      'names' => [
        'index' => 'ajustes.index',
        'store' => 'ajustes.store',
        'edit' => 'ajustes.edit',
        'create' => 'ajustes.create',
        'update' => 'ajustes.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);


    /* modules */
    Route::resource('/modules', 'Backend\ModulesController', [
      'names' => [
        'index' => 'modules.index',
        'store' => 'modules.store',
        'edit' => 'modules.edit',
        'create' => 'modules.create',
        'update' => 'modules.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    Route::get('/modules/status', 'Backend\ModulesController@status')->middleware('auth')->name('modules.status');
    Route::get('/modules/{id}/destroy', 'Backend\ModulesController@destroy')->middleware('auth')->name('modules.destroy');
    Route::post('/modules/order', 'Backend\ModulesController@order')->middleware('auth')->name('modules.order');

    /* modules Sub*/

    Route::get('/modules/submodules/{id?}', 'Backend\ModulesController@indexSub')->middleware('auth')->name('modules.indexSub');
    Route::get('/modules/submodules/create/{id?}', 'Backend\ModulesController@createSub')->middleware('auth')->name('modules.createSub');
    Route::post('/modules/submodules/store', 'Backend\ModulesController@storeSub')->middleware('auth')->name('modules.storeSub');
    Route::get('/modules/submodules/edit/{module_id?}/{id?}', 'Backend\ModulesController@editSub')->middleware('auth')->name('modules.editSub');
    Route::post('/modules/submodules/order', 'Backend\ModulesController@orderSub')->middleware('auth')->name('modules.orderSub');
    Route::patch('/modules/submodules/update/{id?}', 'Backend\ModulesController@updateSub')->middleware('auth')->name('modules.updateSub');
    Route::get('/modules/submodules/{id?}/status', 'Backend\ModulesController@statusSub')->middleware('auth')->name('modules.statusSub');
    Route::get('/modules/submodules/{id?}/destroy', 'Backend\ModulesController@destroySub')->middleware('auth')->name('modules.destroySub');

    /* dados */
    Route::resource('/data', 'Backend\DataController', [
      'names' => [
        'index' => 'data.index',
        'store' => 'data.store',
        'edit' => 'data.edit',
        'create' => 'data.create',
        'update' => 'data.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    /* Menus */
    Route::resource('/menu', 'Backend\MenuController', [
      'names' => [
        'index' => 'menu.index',
        'store' => 'menu.store',
        'edit' => 'menu.edit',
        'create' => 'menu.create',
        'update' => 'menu.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    Route::get('/menu/status', 'Backend\MenuController@status')->middleware('auth')->name('menu.status');
    Route::get('/menu/{id}/destroy', 'Backend\MenuController@destroy')->middleware('auth')->name('menu.destroy');
    Route::post('/menu/order', 'Backend\MenuController@order')->middleware('auth')->name('menu.order');

    /* menu Sub*/

    Route::get('/menu/submenu/{id?}', 'Backend\MenuController@indexSub')->middleware('auth')->name('menu.indexSub');
    Route::get('/menu/submenu/create/{id?}', 'Backend\MenuController@createSub')->middleware('auth')->name('menu.createSub');
    Route::post('/menu/submenu/store', 'Backend\MenuController@storeSub')->middleware('auth')->name('menu.storeSub');
    Route::get('/menu/submenu/edit/{menu_id?}/{id?}', 'Backend\MenuController@editSub')->middleware('auth')->name('menu.editSub');
    Route::post('/menu/submenu/order', 'Backend\MenuController@orderSub')->middleware('auth')->name('menu.orderSub');
    Route::patch('/menu/submenu/update/{id?}', 'Backend\MenuController@updateSub')->middleware('auth')->name('menu.updateSub');
    Route::get('/menu/submenu/{id?}/status', 'Backend\MenuController@statusSub')->middleware('auth')->name('menu.statusSub');
    Route::get('/menu/submenu/{id?}/destroy', 'Backend\MenuController@destroySub')->middleware('auth')->name('menu.destroySub');
    /* - */

    /* colors */
    Route::resource('/colors', 'Backend\ColorsController', [
      'names' => [
        'index' => 'colors.index',
        'store' => 'colors.store',
        'edit' => 'colors.edit',
        'create' => 'colors.create',
        'update' => 'colors.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    /* Midias Sociais */
    Route::resource('/socialMedia', 'Backend\SocialMediaController', [
      'names' => [
        'index' => 'socialMedia.index',
        'store' => 'socialMedia.store',
        'edit' => 'socialMedia.edit',
        'create' => 'socialMedia.create',
        'update' => 'socialMedia.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);
    Route::get('/socialMedia/{id}/destroy', 'Backend\SocialMediaController@destroy')->middleware('auth')->name('socialMedia.destroy');

    /* Links do Topo */
    Route::resource('/linksTopo', 'Backend\LinksTopoController', [
      'names' => [
        'index' => 'linksTopo.index',
        'store' => 'linksTopo.store',
        'edit' => 'linksTopo.edit',
        'create' => 'linksTopo.create',
        'update' => 'linksTopo.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);
    Route::get('/linksTopo/{id}/destroy', 'Backend\LinksTopoController@destroy')->middleware('auth')->name('linksTopo.destroy');

    /* Telefones */
    Route::resource('/phones', 'Backend\PhonesController', [
      'names' => [
        'index' => 'phones.index',
        'store' => 'phones.store',
        'edit' => 'phones.edit',
        'create' => 'phones.create',
        'update' => 'phones.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);
    Route::get('/phones/{id}/destroy', 'Backend\PhonesController@destroy')->middleware('auth')->name('phones.destroy');

    /* Banners */
    Route::resource('/banners', 'Backend\BannerController', [
      'names' => [
        'index' => 'banners.index',
        'store' => 'banners.store',
        'edit' => 'banners.edit',
        'create' => 'banners.create',
        'update' => 'banners.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    Route::get('/banners/{id}/destroy', 'Backend\BannerController@destroy')->middleware('auth')->name('banners.destroy');
    /* - */
    Route::get('/banners/status', 'Backend\BannerController@status')->middleware('auth')
      ->name('banners.status');

    Route::post('/banners/order', 'Backend\BannerController@order')->middleware('auth')
      ->name('banners.order');


    /* Tipos de imóveis */
    Route::resource('/type-property', 'Backend\TipoImovelController', [
      'names' => [
        'index' => 'tipoImovel.index',
        'store' => 'tipoImovel.store',
        'edit' => 'tipoImovel.edit',
        'create' => 'tipoImovel.create',
        'update' => 'tipoImovel.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    /* Atributos dos imóveis */
    Route::resource('/properties-attribute', 'Backend\PropertiesAttributeController', [
      'names' => [
        'index' => 'propertiesAttribute.index',
        'store' => 'propertiesAttribute.store',
        'edit' => 'propertiesAttribute.edit',
        'create' => 'propertiesAttribute.create',
        'update' => 'propertiesAttribute.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);
    Route::get('/properties-attribute/{id}/destroy', 'Backend\PropertiesAttributeController@destroy')->middleware('auth')->name('propertiesAttribute.destroy');

    /* Imóveis */
    Route::resource('/properties', 'Backend\PropertiesController', [
      'names' => [
        'index' => 'properties.index',
        'store' => 'properties.store',
        'edit' => 'properties.edit',
        'create' => 'properties.create',
        'update' => 'properties.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);
    Route::get('/properties/{id}/destroy', 'Backend\PropertiesController@destroy')->middleware('auth')->name('properties.destroy');
    Route::get('/properties/delete-images', 'Backend\PropertiesController@deletePropertyTrashImages')->middleware('auth')->name('properties.delete-images');
    Route::get('/properties/status', 'Backend\PropertiesController@status')->middleware('auth')->name('properties.status');


    /* Mensagem contato */
    Route::resource('/contact', 'Backend\ContactController', [
      'names' => [
        'index' => 'contact.index',
        'store' => 'contact.store',
        'edit' => 'contact.edit',
        'create' => 'contact.create',
        'update' => 'contact.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    /* Contato dos imóveis */
    Route::resource('/property-contacts', 'Backend\PropertyContactsController', [
      'names' => [
        'index' => 'propertyContact.index',
        'store' => 'propertyContact.store',
        'edit' => 'propertyContact.edit',
        'create' => 'propertyContact.create',
        'update' => 'propertyContact.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);

    /* Trabalhe Conosco */
    Route::resource('/work-with-us', 'Backend\TrabalheConoscoController', [
      'names' => [
        'index' => 'trabalheConosco.index',
        'store' => 'trabalheConosco.store',
        'edit' => 'trabalheConosco.edit',
        'create' => 'trabalheConosco.create',
        'update' => 'trabalheConosco.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);
    Route::get('/trabalheConosco/{file}', 'Backend\TrabalheConoscoController@download')->name('trabalheConosco');

    Route::get('/type-property/{id}/destroy', 'Backend\TipoImovelController@destroy')->middleware('auth')->name('tipoImovel.destroy');

    /* Bairros */
    Route::resource('/district', 'Backend\DistrictController', [
      'names' => [
        'index' => 'district.index',
        'store' => 'district.store',
        'edit' => 'district.edit',
        'create' => 'district.create',
        'update' => 'district.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);
    Route::get('/district/{id}/destroy', 'Backend\DistrictController@destroy')->middleware('auth')->name('district.destroy');

    /* Condomínio */
    Route::resource('/condominium', 'Backend\CondominiumController', [
      'names' => [
        'index' => 'condominium.index',
        'store' => 'condominium.store',
        'edit' => 'condominium.edit',
        'create' => 'condominium.create',
        'update' => 'condominium.update',
      ],
      'except' => ['show', 'destroy', 'restore']
    ]);
    Route::get('/condominium/{id}/destroy', 'Backend\CondominiumController@destroy')->middleware('auth')->name('condominium.destroy');

    /* Bairros */
    Route::resource('/dynamic-pages', 'Backend\DynamicPagesController', [
        'names' => [
          'index' => 'dynamic-pages.index',
          'store' => 'dynamic-pages.store',
          'edit' => 'dynamic-pages.edit',
          'create' => 'dynamic-pages.create',
          'update' => 'dynamic-pages.update',
        ],
        'except' => ['show', 'destroy', 'restore']
      ]);
      Route::get('/dynamic-pages/{id}/destroy', 'Backend\DynamicPagesController@destroy')->middleware('auth')->name('dynamic-pages.destroy');
      Route::post('/dynamic-pages/saveimage','Backend\DynamicPagesController@saveImage')->name('dynamic-pages.saveimage');
      Route::get('/dynamic-pages/status', 'Backend\DynamicPagesController@status')->name('dynamic-pages.status');


    // AJAX
    Route::get('/get-cidades/{idState}', 'Backend\DistrictController@getCidades');
    Route::get('/get-bairros/{idCity}', 'Backend\DistrictController@getBairros');
    Route::get('/get-condominios/{idDistrict}', 'Backend\DistrictController@getCondominios');
    Route::get('/get-property-attributes/type/{typePropertyId}', 'Backend\PropertiesController@getTypesProperty')->name('getPropertyAttributes');
    Route::get('/get-attributes-property/{propertyId}', 'Backend\PropertiesController@getAttributesProperty')->name('getAttributesProperty');

    Route::get('/properties/{id}/photos', 'Backend\PropertiesController@photos')
      ->name('properties.photos');

    Route::post('/properties/{id}/photos', 'Backend\PropertiesController@upload')
      ->name('properties.upload');

    Route::post('/properties/caption/{photo}', 'Backend\PropertiesController@caption')
      ->name('properties.photos.caption');

    Route::delete('/properties/delete/{photo}', 'Backend\PropertiesController@delPhoto')
      ->name('properties.photos.delete');

    Route::post('/properties/photos/order', 'Backend\PropertiesController@order');

    Route::post('/properties/cover', 'Backend\PropertiesController@cover');

    Route::post('/properties/saveimage', 'Backend\PropertiesController@saveImage')->name('properties.saveimage');
  });
});
