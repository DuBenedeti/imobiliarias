<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <title>Painel de Controle</title>

    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex, nofollow">

    <!-- BEGIN PLUGIN CSS -->
    <link href="{{ asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css"
          media="screen"/>
    <link href="{{ asset('assets/plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/login.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/stylesheet.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PLUGIN CSS -->

    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="{{ asset('webarch/css/webarch.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('webarch/css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END CORE CSS FRAMEWORK -->
    <link href="{{ asset('assets/css/login.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assts/css/stylesheet.css') }}" rel="stylesheet" type="text/css"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="gradient-bg no-top">

<div class="container">
    <div class="col-md-4 col-md-offset-4">
        <div class="a-center row">
            <a href="http://prestige.com.br/" target="_blank">
                <img src="{{ asset('assets/images/w-imob-white.png') }}" class="logo" style="height: 100px;"/>
            </a>
        </div>
        <div class="clear" style="height:90px"></div>
        <div class="a-center row"><img src="{{ asset('assets/images/titulo-login.png') }}"/></div>
        <div class="clear" style="height:20px"></div>


        <div class="row">
            <div class="row login">

                @yield('content')
            </div>

        </div>
    </div>

    <div class="clear" style="margin-top: 100px;"></div>
    <div id="rodape">
        <a href="http://prestige.com.br/site/" target="_blank">www.prestige.com.br</a>
    </div>
    <!-- END CONTAINER -->

    <!-- CORE JS FRAMEWORK-->
    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>

    <!-- JS DEPENDECENCIES-->
    <script src="{{ asset('assets/plugins/jquery/jquery-1.11.3.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrapv3/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-block-ui/jqueryblockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}"
            type="text/javascript"></script>
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select2/select2.min.js') }}" type="text/javascript"></script>

    <!-- CORE TEMPLATE JS -->
    <script src="{{ asset('webarch/js/webarch.js') }}" type="text/javascript"></script>

    <!-- PAGE JS -->
@yield('js')
</body>
</html>