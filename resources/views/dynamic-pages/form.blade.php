<div class="grid-body no-border">

    <div class="row">
        <div class="form-group col-md-12 {{ $errors->first('title')? 'has-errors' : '' }}">
            {!! Form::label('title', 'Título', ['class' => 'form-label required']) !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título da página', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('title') }}</small>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12 {{ $errors->first('description')? 'has-errors' : '' }}">
            {!! Form::label('description', 'Descrição', ['class' => 'form-label required']) !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('description') }}</small>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-12 {{ $errors->first('text')? 'has-errors' : '' }}">
            {!! Form::label('text', 'Conteúdo da página', ['class' => 'form-label required']) !!}
            {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'ckeditor', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('text') }}</small>
        </div>
    </div>

    <div class="form-actions m-b-5">
        <div class="pull-right">
            <button class="btn btn-success" type_id="submit">
                <i class="fa fa-check"></i>
                {{ $submitButtonText }}
            </button>
            <a class="btn btn-white" href="{{ route('banners.index') }}">Cancelar</a>
        </div>
    </div>
</div>

@section('js')
    <link href="{{ asset('assets/css/summernote.css') }}" rel="stylesheet">
    <script src="{{asset('assets/js/summernote.min.js')  }}"></script>
    <script src="{{ asset('assets/js/summernote-pt-BR.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#ckeditor').summernote({
                lang: 'pt-BR',
                height: 610,
                toolbar: [ 
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['#616161']],
                    ['insert', ['picture','link','table','hr', 'video']],
                    ['fontsize', ['fontsize']],
                    ['font', ['Arial']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                ],
                fontSizes: ['12', '14', '16', '18', '20', '22'],
                callbacks: {
                    onImageUpload: function(files, editor, welEditable) {
                        sendFile(files[0],editor,welEditable);
                    },
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                },
            });
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: "/sistema/dynamic-pages/saveimage",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    $('#ckeditor').summernote('insertImage', url);
                }
            });
        }
        
    </script>

    <script>
        $('.fileinput').on('change.bs.fileinput', function (event) {
            $('.fileinput-preview img').cropper({
                aspectRatio: 1 / 1,
                zoomable: false,
                minCropBoxWidth: 150,
                minCropBoxHeight: 150,
                crop: function (e) {
                    $('#x').val(e.x);
                    $('#y').val(e.y);
                    $('#width').val(e.width);
                    $('#height').val(e.height);
                }
            });
        });
    </script>
@endsection