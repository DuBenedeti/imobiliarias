@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('dynamic-pages.index') }}">Páginas dinâmicas</a></li>
                <li><a class="active">Alterar</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('dynamic-pages.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Alterar <span class="semi-bold">Página</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">

                        <div class="grid-body no-border">


                            {!! Form::model($page, [
                                                    'method' => 'PATCH',
                                                    'route' => ['dynamic-pages.update', $page->id],
                                                    'files' => true
                                                ]) !!}


                            @include('dynamic-pages.form', ['submitButtonText' => 'Alterar',
                                                            'var' => $page])


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

