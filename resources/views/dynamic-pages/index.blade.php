@extends('layouts.app')

@section('content')
    <style>
        .text-description{
            padding: 10px;
            margin-bottom: 10px;
        }
    </style>

    <div class="page-content">
        <div class="content">
            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a class="active">Páginas dinâmicas</a></li>
            </ul>

            <!-- TITLE -->
            <div class="page-title">
                <div class="row">
                    <div class="col-md-6">
                        <a href="./">
                            <i class="icon-custom-left"></i>
                        </a>
                        <h3>Páginas dinâmicas</h3>
                    </div>

                    <div class="col-md-6 p-t-15">
                        <div class="text-right text-center-xs">

                            <a href="{{ route('dynamic-pages.create') }}"
                                class="btn btn-success btn-df-xs btn-small no-ls">
                                <span class="fa fa-plus"></span> Cadastrar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <!-- LISTING camp -->
            <div class="grid simple">
                <div class="grid-title no-border">
                    <div class="pull-left">
                        <h4>
                            Lista de <span class="semi-bold">Páginas</span>
                            @if (isset($trash)) excluídos @endif
                        </h4>
                    </div>
                    @if (!isset($trash))
                        <div class="pull-left m-l-15">
                            <div class="selected-options inline-block" style="visibility:hidden">
                                <a href="#" class="btn btn-small btn-white delete" data-toggle="tooltip"
                                    data-original-title="Excluir selecionados">
                                    <i class="fa fa-fw fa-trash"></i>
                                    {{ csrf_field() }}
                                </a>
                            </div>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                </div>

                <div class="grid-body no-border">
                    <!-- if there is no results -->
                    @if (count($pages) == 0)
                        <h5>Nenhuma página encontrada.</h5>
                    @else
                    <!-- the table -->
                        <table class="table table-striped table-hover table-flip-scroll cf">
                            <thead class="cf">
                            <tr>
                                <th >Título</th>
                                <th width="96">Status</th>
                                <th width="96">Opções</th>
                            </tr>
                            </thead>
                            <tbody id="sortable" class="ui-sortable">
                            @foreach ($pages as $page)
                                <tr id="item_{{ $page->id }}">
                                    <td>{{ $page->title }}</td>
                                    <td>
                                    <input type="checkbox" data-id="{{ $page->id }}"  class="js-switch"
                                                value="@if ($page->status == 1) Ativo @else Inativo @endif"
                                                @if ($page->status == 1) checked @endif />
                                    </td>
                                    <td>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-window-restore"></i> Ações
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{  route('site.dynamic-page.show', [$page->slug]) }}" target="_blank">
                                                    <i class="fa fa-eye"></i> Visualizar
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{  route('dynamic-pages.edit', [$page->id]) }}">
                                                    <i class="fa fa-edit"></i> Editar
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{  route('dynamic-pages.destroy', [$page->id]) }}">
                                                    <i class="fa fa-trash"></i> Excluir
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- paginator -->
                        {{--@include('helpers.paginator', ['var' => $hotels])--}}
                    @endif
                </div> <!-- /.grid-body -->
            </div> <!-- /.grid -->
            <!-- LISTING END -->
        </div>
    </div>
@endsection

@section('js')
    <script>
    $(document).ready(function() {
        $('#sortable').sortable({
        handle: '.handle',
        update: function(event, ui) {
            var url = "/sistema/banners/order";
            var data = $(this).sortable('serialize');

            console.log(data);

            $.post(url, data, function(res) {
            console.log(res);
            if (res != 200) {
                noty({text: "Ocorreu um erro! Tente novamente.", type: 'error'});
            }
            });
        }
        });
    });
    </script>

    <script>
        $(document).on('change', '.js-switch', function () {
            var url = window.location.pathname + "/status";
            console.log(url)

            var id = $(this).attr('data-id');
            var status = $(this).prop('checked');


            if (status == true) status = 1;
            else status = 0;

            $.get(url, {id: id, status: status}, function (code) {
                if (code != 200)
                    noty({
                        text: "Ocorreu um problema! Tente novamente mais tarde.",
                        type: 'error'
                    });
                if (code == 200)
                    location.replace(window.location);
            });
        });
    </script>

@endsection