<script>
  $('.js-state').states({

    @if (old('state_id'))
        'default': '{{ old('state_id') }}',
    @endif

    onChange: function(state) {
      $('.js-city').cities({
        state: state,

        @if (old('city_id'))
            'default': '{{ old('city_id') }}',
        @endif
      });
    }
  });
</script>
