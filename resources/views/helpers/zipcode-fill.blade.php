<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->

<script type="text/javascript">
	$("#zipcode").focusout(function(){
        var states = document.getElementById("state").options;
        var cities = [];
        var districts = [];
		$.ajax({
            url: 'https://viacep.com.br/ws/'+$(this).val().replace(/([^\d])+/gim, '')+'/json/unicode/',
			dataType: 'json',
			success: function(resposta){
                Object.entries(states).forEach(([key, state]) => {
                    if(state.innerHTML == resposta.uf){
                        $("#state").val(state.value).change();
                        cities = document.getElementById("city").options;
                    }
                })
                setTimeout(function(){ 
                    Object.entries(cities).forEach(([key, city]) => {
                        if(city.innerHTML == resposta.localidade){
                            $("#city").val(city.value).change();
                            districts = document.getElementById("district").options;
                        }
                    })
                    setTimeout(function(){
                        Object.entries(districts).forEach(([key, district]) => {
                            if(district.innerHTML == resposta.bairro){
                                $("#district").val(district.value).change();
                            }
                        })
                    }, 2000);
                }, 2000);
                
				$("#place").val(resposta.logradouro);
			}
		});
	});
</script>