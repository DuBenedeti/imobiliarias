<div class="grid-title no-border">
    <h4> Dados do Interessado</h4>
</div>

<div class="grid-body no-border">

    <div class="row">
        <div class="form-group col-md-12 {{ $errors->first('name')? 'has-errors' : '' }}">
            {!! Form::label('name', 'Nome', ['class' => 'form-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('name') }}</small>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-6 {{ $errors->first('email')? 'has-errors' : '' }}">
            {!! Form::label('email', 'E-mail', ['class' => 'form-label']) !!}
            {!! Form::text('email', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('email') }}</small>
        </div>
        <div class="form-group col-md-6 {{ $errors->first('phone')? 'has-errors' : '' }}">
            {!! Form::label('phone', 'Telefone', ['class' => 'form-label']) !!}
            {!! Form::text('phone', null, ['class' => 'form-control', 'disabled' ]) !!}
            <small class="error">{{ $errors->first('phone') }}</small>
        </div>
    </div>
    <div class="row">
    <div class="col-md-2">
            {!! Form::label('file', 'Currículo', ['class' => 'form-label required']) !!}
            <a class="btn btn-success" href="{{ route('trabalheConosco', [$trabalheConosco->id]) }}">Download</a>
        </div>
    </div>

</div>