@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('trabalheConosco.index') }}">Leads</a></li>
                <li><a class="active">Visualizar: {{ $trabalheConosco->name }}</a></li>

            </ul>


            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('trabalheConosco.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Visualizar <span class="semi-bold">Interessados</span>
                </h3>


            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">

                        <div class="grid-body no-border">


                            {!! Form::model($trabalheConosco, [
                                                    'method' => 'PATCH',
                                                    'route' => ['trabalheConosco.update', $trabalheConosco->id],
                                                    'files' => true
                                                ]) !!}


                            @include('trabalheConosco.form', ['submitButtonText' => 'Alterar',
                                                            'var' => $trabalheConosco])


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

