<div class="grid-body no-border">
    <div class="row ">
        <br>
        <div class="form-group col-md-2 {{ $errors->first('icon')? 'has-errors' : '' }}">
            {!! Form::label('icon', 'Ícone', ['class' => 'form-label required']) !!}
            {!! Form::text('icon', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('icon') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('name')? 'has-errors' : '' }}">
            {!! Form::label('name', 'Nome', ['class' => 'form-label required']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('name') }}</small>
        </div>
        <div class="form-group col-md-6 {{ $errors->first('link')? 'has-errors' : '' }}">
            {!! Form::label('link', 'Link', ['class' => 'form-label required']) !!}
            {!! Form::text('link', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('link') }}</small>

        </div>
        <div class="form-group col-md-2 " style="margin-top: 2%">

            @if($submitButtonText == 'Cadastrar')
            <button class="btn btn-success" type_id="submit">
                <i class="fa fa-check"></i>
            </button>
            @endif
            @if($submitButtonText != 'Cadastrar')
            <a class="btn btn-danger" href="{{ route('linksTopo.destroy', $var->id) }}">
                <i class="fas fa-trash"> </i> </a>
            @endif
        </div>
    </div>
</div>


@section('js')
<link href="{{ asset('assets/css/summernote.css') }}" rel="stylesheet">
<script src="{{asset('assets/js/summernote.min.js')  }}"></script>
<script src="{{ asset('assets/js/summernote-pt-BR.js') }}"></script>
<script>
    window.onload = function() {
        $('#ckeditor').summernote({
            lang: 'pt-BR',
            height: 250,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['color', ['#616161']],
                ['insert', ['picture', 'link', 'table', 'hr', 'video']],
                ['fontsize', ['fontsize']],
                ['font', ['Arial']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
            ],
            fontSizes: ['14', '16', '18'],
            callbacks: {
                onImageUpload: function(files, editor, welEditable) {
                    sendFile(files[0], editor, welEditable);
                },
                onPaste: function(e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            }
        });
    };

    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "POST",
            url: "/sistema/press/noticias/saveimage",
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                $('#ckeditor').summernote('insertImage', url);
            }
        });
    }

    $('#cnpj').mask("99.999.999/9999-99");
    $('#phone').mask("(99) 99999-9999");
</script>
@endsection