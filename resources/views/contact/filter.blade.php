<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h4>Filtros</h4>
            </div>

            <div class="grid-body no-border">
                <form action="{{ route('contact.index') }}" method="get" id="filter-form">
                    

                    <div class="row">
                        {{-- keywords --}}
                        <div class="col-md-6 m-t-10">
                            <input type="text" name="keyword" class="form-control" placeholder="Nome ou Assunto">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 m-t-10">
                            <!-- remove filters button -->
                            @if ($_GET)
                            <a href="{{ route('contact.index') }}" class="btn btn-mini btn-default">
                                <i class="fa fa-times"></i> Limpar filtros
                            </a>
                            @endif

                            <!-- submit filtering -->
                            <button type="submit" class="btn btn-primary btn-small btn-cons pull-right">
                                <i class="fa fa-search"></i> &nbsp; Filtrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>