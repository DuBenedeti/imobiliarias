@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a class="active">Mensagens de contato</a></li>
            </ul>

            <!-- TITLE -->
            <div class="page-title">
                <div class="row">

                    <div class="col-md-6">
                        <a href="./">
                            <i class="icon-custom-left"></i>
                        </a>
                        <h3>Mensagens de contato</h3>
                    </div>

                    
                </div>
            </div>

            <!-- FILTERS -->
        @include('contact.filter')


        <!-- LISTING camp -->
            <div class="grid simple">
                <div class="grid-title no-border">
                    <div class="pull-left">
                        <h4>
                            Lista de <span class="semi-bold">Interessados</span>
                            @if (isset($trash)) excluídos @endif
                        </h4>
                    </div>

                    @if (!isset($trash))
                        <div class="pull-left m-l-15">
                            <div class="selected-options inline-block" style="visibility:hidden">
                                <a href="#" class="btn btn-small btn-white delete" data-toggle="tooltip"
                                   data-original-title="Excluir selecionados">
                                    <i class="fa fa-fw fa-trash"></i>
                                    {{ csrf_field() }}
                                </a>
                            </div>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                </div>

                <div class="grid-body no-border">
                    <!-- if there is no results -->
                    @if (count($contact) == 0)
                        <h5>Nenhuma Mensagem encontrada.</h5>
                    @else

                    <!-- the table -->
                        <table class="table table-striped table-hover table-flip-scroll cf">
                            <thead class="cf">
                            <tr>

                                <th>Data</th>
                                <th>Nome</th>
                                <th>Assunto</th>

                                <th width="96">Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($contact as $msg)
                                <tr>
                                    <td>
                                        <i class="fa fa-calendar"></i> {{ date('d/m/Y', strtotime($msg->created_at)) }}
                                        <br>
                                        <i class="fa fa-clock-o"></i> {{ date('H:i:s', strtotime($msg->created_at)) }}
                                    </td>
                                    <td>{{ $msg->name }}</td>
                                    <td>{{ $msg->subject }}</td>

                                    <td>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-window-restore"></i> Ações
                                            <span class="caret"></span></button>

                                        <ul class="dropdown-menu">

                                            <li>
                                                <a href="{{  route('contact.edit', [$msg->id]) }}">
                                                    <i class="fa fa-eye"></i> Visualizar
                                                </a>
                                            </li>

                                            

                                        </ul>

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        
                        <!-- paginator -->
                        {{--@include('helpers.paginator', ['var' => $hotels])--}}

                    @endif

                </div> <!-- /.grid-body -->
            </div> <!-- /.grid -->
            <!-- LISTING END -->

        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).on('change', '.js-switch', function () {
            var url = window.location.pathname + "/status";

            var id = $(this).attr('data-id');
            var status = $(this).prop('checked');


            if (status == true) status = 'Ativo';
            else status = 'Inativo';

            $.get(url, {id: id, status: status}, function (code) {
                if (code != 200)
                    noty({
                        text: "Ocorreu um problema! Tente novamente mais tarde.",
                        type: 'error'
                    });
                if (code == 200)
                    location.replace(window.location);
            });
        });
    </script>

@endsection