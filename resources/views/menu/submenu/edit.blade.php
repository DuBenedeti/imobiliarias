@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('menu.index') }}">Submenu</a></li>
                <li><a class="active">Alterar Submenu: {{$menu->name}}</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('menu.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Alterar <span class="semi-bold">Submenu</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">

                        <div class="grid-body no-border">

                            {!! Form::model($menu, [
                                                    'method' => 'PATCH',
                                                    'route' => ['menu.updateSub', $menu->id],
                                                    'files' => true
                                                ]) !!}

                            @include('menu.submenu.form', ['submitButtonText' => 'Alterar',
                                                                'var' => $menu])

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

