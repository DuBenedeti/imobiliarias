@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="content">

        <!-- BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <p>VOCÊ ESTÁ AQUI</p>
            </li>
            <li><a href="{{ route('users.index') }}">Usuários</a></li>
        </ul>

        <!-- TITLE -->
        <div class="page-title">
            <div class="row">
                <div class="col-md-6">
                    @if((Auth::user()->hasRole('Admin')))
                    <a href="./">
                        <i class="icon-custom-left"></i>
                    </a>
                    @else
                    <i class="icon-custom-left"></i>
                    @endif
                    <h3>Usuários</h3>
                </div>

                <div class="col-md-6 p-t-15">
                    <div class="text-right text-center-xs">
                        @if (!isset($trash))

                        {{-- <a href="{{ route('users.trash') }}" class="btn btn-small btn-df-xs m-r-5" data-toggle="tooltip" data-original-title="Lixeira">
                        <span class="fa fa-trash"></span>
                        </a> --}}
                        @if((Auth::user()->hasRole('Admin')))
                        <a href="{{ route('users.create') }}" class="btn btn-success btn-df-xs btn-small no-ls">
                            <span class="fa fa-plus"></span> Cadastrar
                        </a>
                        @endif

                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- FILTERS -->
        @if((Auth::user()->hasRole('Admin')))
        @include('user.filter')
        @endif


        <!-- LISTING USERS -->
        <div class="grid simple">
            <div class="grid-title no-border">
                <div class="pull-left">
                    <h4>
                        Lista de <span class="semi-bold">Usuários</span>
                        @if (isset($trash)) excluídos @endif
                    </h4>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="grid-body no-border">
                <!-- if there is no results -->
                @if (count($users) == 0)
                <h5>Nenhum usuário encontrado.</h5>
                @else

                <!-- the table -->
                <table class="table table-striped table-hover table-flip-scroll cf">
                    <thead class="cf">
                        <tr>
                            <th width="15"></th>
                            <th>Data</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            @if((Auth::user()->hasRole('Admin')))
                            <th width="72">Status</th>
                            <th width="96">Opções</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td><br>
                                <span data-toggle="popover" data-placement="right" data-html="true" data-content=" @foreach(\Spatie\Activitylog\Models\Activity::where('subject_id', $user->id)->where('log_name', 'Usuários')->
                                        where('description', 'created')->get() as $ac)
                                              @foreach(\App\User::where('id', $ac->causer_id)->get() as $user_log)
                                                      Criado por: {{ $user_log->name }} - {{ date('d/m/Y H:i:s', strtotime($ac->created_at)) }}


                                              @endforeach
                                              @endforeach
                                                      <br/>

                                            @foreach(\Spatie\Activitylog\Models\Activity::where('subject_id', $user->id)->where('log_name', 'Usuários')->
                                        where('description', 'updated')->OrderBy('id', 'desc')->limit(1)->get() as $ac)
                                              @foreach(\App\User::where('id', $ac->causer_id)->get() as $user_log)
                                                      Editado por: {{ $user_log->name }} - {{ date('d/m/Y H:i:s', strtotime($ac->created_at)) }}

                                              @endforeach
                                              @endforeach"><i class="fa fa-info-circle"></i>&nbsp;
                                </span>
                            </td>
                            <td>
                                <i class="fa fa-calendar"></i> {{ date('d/m/Y', strtotime($user->created_at)) }}
                                <br>
                                <i class="fa fa-clock-o"></i> {{ date('H:i:s', strtotime($user->created_at)) }}

                            </td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            @if((Auth::user()->hasRole('Admin')))
                            <td>

                                <input type="checkbox" data-id="{{ $user->id }}" class="js-switch" value="@if ($user->status == 'Ativo') Ativo @else Inativo @endif" @if ($user->status == 'Ativo') checked @endif />
                                {{--@if($user->status == 'Ativo')--}}
                                {{--<span class="label label-success">Ativo</span>--}}
                                {{--@else--}}
                                {{--<span class="label label-danger">Inativo</span>--}}
                                {{--@endif--}}
                            </td>

                            <td>
                                @if (!isset($trash))
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-window-restore"></i> Ações
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">

                                    <li>
                                        <a href="{{ route('users.edit', $user->id) }}">
                                            <i class="fa fa-edit"></i> Editar
                                        </a>
                                    </li>

                                    {{--@if(auth()->user()->hasPermission('add permission users'))--}}
                                    <li class="divider"></li>
                                    <li>
                                        <a href="{{ route('users.edit.permission', $user->id) }}">
                                            <i class="fa fa-edit"></i> Editar Permissões
                                        </a>
                                    </li>
                                    {{--@endif--}}

                                </ul>
                                @else
                                <a href="{{ route('users.restore', $user->id) }}" class="btn btn-success btn-small">Restaurar</a>
                                @endif
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <!-- paginator -->
                {{--@include('helpers.paginator', ['var' => $users])--}}

                @endif

            </div> <!-- /.grid-body -->
        </div> <!-- /.grid -->
        <!-- LISTING END -->

    </div>
</div>
@endsection

@section('js')
<script>
    $(document).on('change', '.js-switch', function() {
        var url = window.location.pathname + "/status";

        var id = $(this).attr('data-id');
        var status = $(this).prop('checked');

        if (status == true) status = 'Ativo';
        else status = 'Inativo';

        $.get(url, {
            id: id,
            status: status
        }, function(code) {
            if (code != 200)
                noty({
                    text: "Ocorreu um problema! Tente novamente mais tarde.",
                    type: 'error'
                });
        });
    });
</script>

@endsection