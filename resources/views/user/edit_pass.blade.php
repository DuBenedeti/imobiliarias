@extends('layouts.app')
<?php $url = explode('/',Request::url());?>
@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('users.index') }}">Usuários</a></li>
                <li><a class="active">Alterar senha: {{ $user->name }}</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('users.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Alterar Senha <span class="semi-bold"> {{ $user->name }}</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">
                        <div class="grid-title no-border">

                        </div>
                        <div class="grid-body no-border">


                            {!! Form::model($user, [
                                                    'method' => 'PATCH',
                                                    'route' => ['users.update_pass', $user->id],
                                                    'files' => true
                                                ]) !!}


                            @include('user.form_pass', ['submitButtonText' => 'Alterar',
                                                            'var' => $user])


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

