@extends('frontend.master')
@section('title', $property->getFrontTitle().' '. $property->getFrontAddress().' | '. $data->name )
@section('description', 'Classificados de imóveis em '. $data->city->name .'/'.$data->city->state->initials)
@section('og_image', asset('assets/images/property/thumb/'.$property->image))

@section('content')

@include('frontend.partials.filter')

<div class="container">
    <div class="text-center col col-md-12 mt-4" style="font-size: 2.2em;"><strong>{{ $property->getFrontTitle() }}</strong></div>
    <div class="text-center col col-md-12" style="font-size: 1.1em;">{{ $property->getFrontAddress() }}</div>
    <div class="text-center col col-md-12" style="font-size: 2em;"><strong>{{ floatFormat($property->price) }}</strong></div>

    <div class="row mt-5 info-property">
        <div class="col-md-7 col-xs-12">
            {{-- Property photos --}}

            @if($property->photos != "")
            <div class="slick-news margin-bottom">
                @if($property->video != null)
                <div class="video-news">
                    <iframe width="610" height="240" style="display: block;" class="videoM" src="https://www.youtube.com/embed/{{ $property->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="610" height="350" style="display: block;" class="videoD" src="https://www.youtube.com/embed/{{ $property->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                @endif
                <div class="img-news">
                    <a href="{{asset('assets/images/property/thumb/'.$property->image)}}" data-fancybox="gallery">
                        <img src="{{asset('assets/images/property/thumb/'.$property->image)}}" class="img-fluid w-100" alt="legenda">
                    </a>
                </div>
                @foreach($property->photos as $photo)
                @if(isset($photo->image) || ($photo->image != ""))
                <div class="img-news">
                    <a href="{{asset('assets/images/property/'.$photo->image)}}" data-fancybox="gallery">
                        <img src="{{asset('assets/images/property/'.$photo->image)}}" class="img-fluid w-100" alt="legenda">
                    </a>
                    <div class="img-news--subtitle text-center text-sm">
                        {{ $photo->subtitle }}
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            @endif

            <div class="texto-junto">
                @if($property->external_id != null)
                Código: {{ $property->external_id }} <br>
                @endif
                @if($property->meters != null)
                Área construída: {{ $property->meters }}m²<br>
                @endif
                @if($property->total_area != null)
                Área total: {{ $property->total_area }}m²<br>
                @endif
                @if($property->condominium != null)
                Condomínio: {{ $property->condominium->name }}<br>
                @endif
            </div>
            <div class="property-attributes mt-2 texto-junto">
                @if(count($propertyAttributes) > 0)
                <strong>Características:</strong><br>
                    @foreach ($propertyAttributes as $attribute)
                        @switch($attribute->attribute->type_field)
                        @case('boolean')   
                            {{$attribute->attribute->name}}: Sim<br>
                            @break
                        @case('money')   
                            {{$attribute->attribute->name}}: R${{$attribute->value}}<br>
                            @break
                        @case('quantity')   
                            {{$attribute->attribute->name}}: {{$attribute->value}}<br>
                            @break
                        @case('text')   
                            {{$attribute->attribute->name}}: {{$attribute->value}}<br>
                            @break
                        @endswitch

                    @endforeach
                @endif
            </div>

            <div class="property-details mt-2 texto-junto">
                <strong>Observações:</strong>
                <p>
                    {!! $property->details !!}
                </p>
            </div>
            <div class="clearfix"></div>
        </div>


        <div class="col-md-5 col-xs-12">
            <div class="contactProperty">
                @if(session()->has('success'))
                    <div class="col col-md-12 col-xs-12 contactPropertyMessage" style="background-color: #28a745;">
                        {{session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="col col-md-12 col-xs-12 contactPropertyMessage" style="background-color: #dc3545">
                        {{session()->get('error') }}
                    </div>
                @endif
                <div class="col col-md-12 col-xs-12">
                    <span style="font-size: 1.3em;"> <strong> Tenho interesse </strong> </span>
                </div>
                <form action="{{ route('send.property.contact', $property->id) }}" method="post">
                    @csrf
                    <div class="col col-md-12 col-xs-12 mt-3">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nome completo" required />
                    </div>
                    <div class="col col-md-12 col-xs-12 mt-2">
                        <input type="text" name="email" id="email" class="form-control" placeholder="E-mail" required />
                    </div>
                    <div class="col col-md-12 col-xs-12 mt-2">
                        <input type="text" name="phone" id="phone" class="form-control phone" placeholder="Telefone" required />
                    </div>
                    <!-- <div class="col col-md-12 col-xs-12 mt-2">
                        <span style="font-size: 1em;"> <strong> Agende uma visita. </strong> </span>
                    </div>
                    <div class="col col-md-12 col-xs-12 mt-2">
                        <input type="date" name="dateVisit" id="dateVisit" class="form-control dateVisit" placeholder="Data para visita" />
                    </div> -->
                    <div class="col col-md-12 col-xs-12 mt-2">
                        <textarea name="message" id="message" class="form-control" rows="5" style="resize: false;" required>Olá, tenho interesse neste imóvel, código {{$property->external_id}}. Aguardo seu retorno, obrigado!
                    </textarea>
                    </div>
                    <div class="col col-md-6 col-xs-12 mt-2" style="float: right">
                        <button type="submit" class="btn buttonColor btn-contactProperty">E-MAIL <i class="fas fa-envelope"></i></button>
                    </div>
                </form>

                <div class="col col-md-6 col-xs-12">
                    <a href="{{ whatsProperty($data, $property, "+55", "Me interessei pelo imóvel: ".Request::url() ) }}" class="colorWhite" target="_blank">
                        <button class="btn buttonColor btn-contactProperty mt-2">
                            WHATSAPP <i class="fab fa-whatsapp"></i>
                        </button>
                    </a>
                </div>
            </div>
            <div class="col-md-12 pl-0 pr-0 d-flex">
                <div class="col col-md-6 col-xs-12 pl-0">
                    <a href="#" onClick="window.print()" class="colorWhite">
                        <button class="btn buttonColor btn-contactProperty mt-2">
                            Imprimir <i class="fas fa-print"></i>
                        </button>
                    </a>
                </div>
                <div class="col col-md-6 col-xs-12 pr-0 shareButtons">
                    Compartilhe 
                    <a href="https://www.facebook.com/sharer.php?u={{ Request::url() . '&quote=' . $property->getFrontTitle() }}" target="_blank">
                            <i class="fab fa-facebook-square" id="shareFacebook"></i>
                    </a>
                    <a href="https://api.whatsapp.com/send?text={{ urlencode($property->getFrontTitle() . ': ' . Request::url()) }}" target="_blank">
                            <i class="fab fa-whatsapp-square" id="shareWhats"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@if(count($properties) > 0)
<div class="divider margin-top"></div>

<div class="container relationalProperties">
    <div class="text-center col col-md-12 mt-5" style="font-size: 2em;">Imóveis relacionados</div>
    <div class="property-list">
        @foreach ($properties as $item)
        <div class="property-content margin-top col-md-3">
            <a href="{{ route('site.imoveis.show', $item->slug) }}" class="hover-img" title="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">

                @if(!isset($item->image) || ($item->image) == "")
                <img src="{{ asset('assets/images/property/sem.jpg')}}" class="img-fluid img-blog" alt="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">
                @else
                <img src="{{ asset('assets/images/property/thumb/'.$item->image)}}" class="img-fluid img-blog" alt="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">
                @endif
                <div class="property-info">
                    <div class="font-weight-bold">
                        {{ $item->getFrontTitle() }}
                    </div>
                    <span>{{$item->district->name}}</span><br>
                    <span>{{$item->district->city->name}}-{{$item->district->city->state->initials}}</span><br>
                    <p>Cod: {{$item->external_id}}</p>
                    <div class="priceItem">
                        @if ($item->typeBusiness->id == 1)
                        Venda: 
                        @else
                        Locação:
                        @endif
                         <span class="font-weight-bold">{{ floatFormat($item->price) }}</span>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>

</div>
@endif

@endsection

@section('js')
<script src="{{ asset('site/js/form.min.js') }}"></script>

<script>
    $(".phone").mask("(00) 00000-0000")}
    var i = 0;
    $(".post-text img").each(function() {
        i++;
        var imageLink = this.src;
        $(this).addClass("image" + i);

        $(".image" + i).wrap("<a href='" + imageLink + "' data-fancybox='gallery' class='fancy' id='linkFancy" + i + "'></a>");

        var removeInQuote = $(".comment--img");
        if (removeInQuote.parent().is("#linkFancy" + i)) {
            removeInQuote.unwrap();
        }

    });
</script>
<script>
    /* Slick
        ========================================================================== */
    $(document).ready(function() {
        $('.slick-news').slick({
            infinite: true,
            arrows: false,
            dots: true,
            adaptiveHeight: true,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
    });
</script>

<script>
     $('.icons').css('color', '{{$colors->footerIconesMidiasSociais}}');
     $('#shareFacebook').css('color', '{{$colors->botoes}}');
     $('#shareWhats').css('color', '{{$colors->botoes}}');
     function hover(x) {

        $(x).css('color', '{{$colors->footerIconesMidiasSociaisHover}}');
    }

    function noHover(x) {
        $(x).css('color', '{{$colors->footerIconesMidiasSociais}}');
    }

    
</script>
@endsection
@section('css')
<style>
    @media (max-width: 700px) {
        .videoD {
            display: none !important;
        }

        .videoM {
            display: block;
        }
    }

    @media (min-width: 701px) {
        .videoM {
            display: none !important;
        }

        .videoD {
            display: block;
        }
    }
</style>
@endsection