@extends('frontend.master')
@section('title', 'Imóveis em '.$data->city->name.'/'.$data->city->state->initials.' - '.$data->name)
@section('description', 'Encontre os melhores anúncios de imóveis à venda ou para locação em '. $data->city->name .'/'.$data->city->state->initials.'. Temos '.$properties->total().' imóveis cadastrados.')

@section('content')

@include('frontend.partials.filter')

<div class="container">
    <div class="text-center col col-md-12 mt-5" style="font-size: 1.7em;">Sua <strong>busca</strong> resultou em <strong>{{ $properties->total() }}</strong> imóveis</div>

    <div class="form-orderby col right mt-3">

    <form action="#" method="get" id="formOrderBy">
        <select name="orderBy" id="orderBy" class="form-control form-control-sm" onchange="filterOrderProperties()">
            <option value="0" disabled selected >Ordenar</option>
            <option value="2">Cadastro decrescente</option>
            <option value="1">Cadastro crescente</option>
            <option value="3">Preço crescente</option>
            <option value="4">Preço decrescente</option>
        </select>
    </form>
    </div>
    <div class="property-list">
        @foreach ($properties as $item)
        <div class="property-content margin-top col-md-3">
            <a href="{{ route('site.imoveis.show', $item->slug) }}" class="hover-img" title="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">

                @if(!isset($item->image) || ($item->image) == "")
                <img src="{{ asset('assets/images/property/sem.jpg')}}" class="img-fluid img-blog" alt="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">
                @else
                <img src="{{ asset('assets/images/property/thumb/'.$item->image)}}" class="img-fluid img-blog" alt="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">
                @endif
                <div class="property-info">
                    <div class="font-weight-bold">
                        {{ $item->getFrontTitle() }}
                    </div>
                    @if($item->condominium != null)
                    <span>Condomínio {{$item->condominium->name}}</span><br>
                    @endif
                    <span>{{$item->district->city->name}}-{{$item->district->city->state->initials}}</span><br>
                    <p>Cod: {{$item->external_id}}</p>
                    <div class="font-weight-bold priceItem">
                        {{ floatFormat($item->price) }}
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
    <div class="text-center margin-top">
        {!! $properties->appends(Request::except('page'))->links() !!}
    </div>
</div>

@endsection

@section('js')
<script>

    const urlParams = new URLSearchParams(window.location.search);
    params = urlParams.get('orderBy')
    if(params != null) {
        document.getElementById('orderBy').value = params;
    }

    function filterOrderProperties(){

        url =  new URL(window.location.href);
        url_params = new URLSearchParams(url.search);
        url_path = window.location.href.replace(url_params, '')
        val = $('#orderBy').val()

        if(!url_params.has('orderBy')){
            if(url.search == ''){
                url_path = url_path+'?'
            }
            url_params.append('orderBy', val)
        }else{
            url_params.delete('orderBy')
            url_params.append('orderBy', val)
        }
        console.log(url_path+url_params)
        window.location.href = url_path+url_params

    }
</script>
@endsection