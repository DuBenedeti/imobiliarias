@extends('frontend.master')
@section('title', 'Trabalhe Conosco - '. $data->name)

@section('content')
<div class="container">
    <div class="conteudo">

        <div class="cabecalho">
            <h1>Trabalhe Conosco</h1>
            <p class="linhaFina">Preencha o formulário e envie sua mensagem</p>
        </div>
        @if (session()->has('success'))
        <div class="row justify-content-md-center" style="padding: 25px;">
            <div class="col-md-6 col-xs-12 contatoEnviado" >
                {{ session()->get('success') }}
            </div>
        </div>
        @endif
        <div class="row justify-content-md-center">
            <div class="col-md-6 col-xs-12">
                <form action="{{route('site.trabalheConoscoSave')}}" onSubmit='verifica()' method="POST" id="contact-form" class="contact-form" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <div class="row">
                        <div class="form-group col-md-12 no-margin">
                            <label for="name" class="text-sm">Nome Completo</label>
                            <input type="text" class="form-control item" id="name" name="name" value="{{ old('name') }}" required>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 no-margin">
                            <label for="email" class="text-sm">E-mail</label>
                            <input type="email" class="form-control item" id="email" name="email" value="{{ old('email') }}" required>
                            <label id="email-error" class="error" for="email"></label>
                        </div>
                        <div class="form-group col-md-6 no-margin">
                            <label for="phone" class="text-sm">Telefone/Celular</label>
                            <input type="phone" class="form-control phone item" id="phone" name="phone" value="{{ old('phone') }}" required>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="archive" class="text-sm">Currículo</label>
                            <label class="input-file">
                                <b class="btn btn-primary">Escolha um arquivo</b>
                                <input name="attachment" id="attachment" style="display: none" type="file" class="fileInput" required>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 no-margin">
                            @include('frontend.partials.recaptcha')
                        </div>
                    </div>

                    <div class="text-center margin-bottom ml-3" id="loading">
                        <button class="btn  buttonColor
                            text-uppercase medium-button font-weight-bold" id='call-to-action'>
                            Enviar
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('site/js/form.min.js') }}"></script>
<script language="javascript">
    $(document).ready(function() {
        $('#data').mask('99/99/9999');
        $('#cpf').mask('999.999.999-61');
        return false;
    });

    $('.fileInput').change(function() {
        var numfiles = $(this)[0].files.length;
        var parent = $(this).closest('.input-file');
        parent.find('ins').remove();
        for (i = 0; i < numfiles; i++) {
            parent.append('<ins>' + $(this)[0].files[i].name + '</ins>')
        }
    });

    function verifica() {
        if (document.getElementById("attachment").value == "") {
            alert("Por favor, algum campo ficou sem preencher, volte e preencha todos os campos!");
            windows.history.back()
            return;
        }
    }
</script>

@endsection