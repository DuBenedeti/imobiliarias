<div class="header" id="headerDesktop">
    <div class="container topo-header">
        <!-- <div class="col-md-4 left">
            <span class="show-navbar-button" id="showNavbarButton"><i class="fas fa-bars"></i></span>
        </div> -->
        <div class="logo col-md-6 left">
            <a href="{{route('home')}}"><img src="{{ asset('assets/images/data/'.$data->logo_d) }}" alt="" srcset="" style="max-height: 90px; max-width: 380px"></a>
        </div>
        <div class="redesSociais col-md-6 right">
            @foreach($socialMedia as $sm)
            <a href="{{ $sm->link }}"  target="_blank"><i class="{{ $sm->icon }}"></i></a>
            @endforeach
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbarDesktop" style="height:2.5em;">
    <div class="collapse navbar-collapse container">
        <ul class="navbar-nav itensMenu" style="padding:15px">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">Home</a>
            </li>
            @foreach ($dynamicPages as $page)
            <li class="nav-item">
                <a class="nav-link" href="{{ route('site.dynamic-page.show', $page->slug) }}">{{$page->title}}</a>
            </li>
            @endforeach
            @foreach ($typeBusiness as $type)
            @if(count($type->properties) > 0)
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('site.imoveis', ['type_business_id' => $type->id]) }}">{{ $type->name }}</a>
                </li>
            @endif
            @endforeach
            @if($data->workwithus)
            <li class="nav-item">
                <a class="nav-link" href="{{ route('site.trabalheConosco') }}">Trabalhe Conosco</a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link" href="{{ route('site.contato') }}">Contato</a>
            </li>
        </ul>
    </div>
</nav>
<div class="header" id="headerMobile">
    <div class="container topo-header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="display:contents">
            <div class="logoMobile col-md-6 left">
                <a href="{{route('home')}}"><img src="{{ asset('assets/images/data/'.$data->logo_d) }}" alt="" srcset="" ></a>
            </div>
            <div class="col-md-6 right">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <!-- <div class="redesSociais col-md-4 right">
                @foreach($socialMedia as $sm)
                <a href="{{ $sm->link }}"  target="_blank"><i class="{{ $sm->icon }}"></i></a>
                @endforeach
            </div> -->
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav itensMenu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home') }}">Home</a>
                    </li>
                    @foreach ($dynamicPages as $page)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('site.dynamic-page.show', $page->slug) }}">{{$page->title}}</a>
                    </li>
                    @endforeach
                    @foreach ($typeBusiness as $type)
                    @if(count($type->properties) > 0)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('site.imoveis', ['type_business_id' => $type->id]) }}">{{ $type->name }}</a>
                        </li>
                    @endif
                    @endforeach
                    @if($data->workwithus)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('site.trabalheConosco') }}">Trabalhe Conosco</a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('site.contato') }}">Contato</a>
                    </li>
                </ul>
                <div class="foneMenu">
                    <div>
                        {{$data->phone}}
                    </div>
                    <div>
                        @foreach($phones as $phone)
                        <span>{{ $phone->phone }}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<script>
    $("#showNavbarButton").on("click", function() {
        $("#navbarDesktop").slideToggle('slow');
    });
</script>