<a onclick="showHideFilters()" class="showFilters" id="showFilters">Filtros</a>

<div class="propertiesFilter" id="propertiesFilter">
    <div class="container">
        <form action="{{ route('site.imoveis') }}" method="get" id="filter-form">

            <div class="row center p-3 propertiesFilter-form">
                <div class="col-md-2 col-xs-12 ">
                    <select name="type_business_id" id="type_business_id" class="form-control">
                    <option value="">
                            Tipo de negócio
                        </option>
                        @foreach ($typeBusiness as $type)
                        @if(count($type->properties) > 0)
                        <option value="{{ $type->id }}">
                            {{ $type->name }}
                        </option>
                        @endif
                        @endforeach
                    </select>
                </div>

                <div class="col-md-2 col-xs-12">
                    <select name="type_property_id" id="type_property_id" class="form-control" data-url="{{ route('getPropertyAttributes', 1) }}">
                        <option value="">
                            Tipo de imóvel
                        </option>
                        @foreach ($tipoImovel as $type)
                        <option value="{{ $type->id }}">
                            {{ $type->name }}
                        </option>
                        @endforeach
                    </select>

                </div>
                <div class="col-md-2 col-xs-12">
                    <select name="district_id" id="district_id" class="form-control" data-url="{{ route('getPropertyAttributes', 1) }}">
                        <option value="">
                            Bairro
                        </option>
                        @foreach ($districts as $district)
                        <option value="{{ $district->id }}">
                            {{ $district->name }} | {{ $district->city->name }} - {{ $district->city->state->initials }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 col-xs-12" id="price-container">
                    <input type="text" name="min_price" id="min_price" class="form-control money-mask" data-mask="000.000.000,00" data-mask-reverse="true" placeholder="Preco mínimo" />
                </div>
                <div class="col-md-2 col-xs-12" id="price-container">
                    <input type="text" name="max_price" id="max_price" class="form-control money-mask" data-mask="000.000.000,00" data-mask-reverse="true" placeholder="Preco máximo" />
                </div>
                <div class="col-md-2 col-xs-12">
                    <button class="btn btn-success btn-lg buttonColor" type_id="submit" style="width: 100%;">
                        <i class="fa fa-search"></i>
                        Buscar
                    </button>
                </div>
            </div>

        </form>
    </div>
</div>
<script>
    $('.money-mask').mask('000.000.000,00', {reverse: true});
    function showHideFilters(){
        $('#propertiesFilter').slideToggle('slow')
    }
</script>