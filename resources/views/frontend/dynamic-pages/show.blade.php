@extends('frontend.master')
@section('title', $page->title .' - '. $data->name)
@section('description', $page->description)

@section('content')

<div class="container col-md-5">
    <h2 class="mt-5 text-center">{{ $page->title }}</h2>
    <h6 class="mb-5 text-center">{{ $page->description }}</h6>
    <div class="contentDP"> 
    {!! $page->text !!}
    </div>
</div>
@endsection