@php echo '<?xml version="1.0" encoding="UTF-8"?>
<Carga xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"> '@endphp
	<Imoveis>
        @foreach ($properties as $property)
		<Imovel>
			<CodigoImovel>{{$property->external_id}}</CodigoImovel>
			<Titulo>{{$property->getTitle()}}</Titulo>
			@switch($property->type_business_id)
				@case(1)
					<PrecoVenda>{{$property->price}}</PrecoVenda>
					@break
				@case(2)
					<PrecoLocacao>{{$property->price}}</PrecoLocacao>
					@break
				@case(3)
					<PrecoLocacaoTemporada>{{$property->price}}</PrecoLocacaoTemporada>
					@break
			@endswitch
			<Endereco>{{$property->place}}</Endereco>
			<Bairro>{{$property->district->name}}</Bairro>
			<Cidade>{{$property->district->city->name}}</Cidade>
			<UF>{{$property->district->city->state->initials}}</UF>
			<CEP>{{$property->zipcode}}</CEP>
			<AreaUtil>{{$property->meters}}</AreaUtil>
            <AreaTotal>{{$property->total_area}}</AreaTotal>
			<Observacao>
				<![CDATA[{!! $property->details !!}]]>
			</Observacao>
			<TipoImovel>{{$property->typeProperty->name}}</TipoImovel>
			

			<Fotos>
				<Foto>
					<URLArquivo>{{asset('assets/images/property/thumb').'/'.$property->image}}</URLArquivo>
					<Principal>1</Principal>
				</Foto>
			@foreach($property->photos as $key => $photo)
				<Foto>
					<URLArquivo>{{asset('assets/images/property').'/'.$photo->image}}</URLArquivo>
					<Principal>0</Principal>
				</Foto>
			@endforeach
			</Fotos>
			<Videos>
				<Video>{{$property->video}}</Video>
			</Videos>

			@foreach ($property->attributes as $attribute)
				@switch($attribute->attribute->name)
					@case('Quartos')
						<QTDDormitorios>{{$attribute->value}}</QTDDormitorios>
						@break
					@case('Banheiros')
						<QTDBanheiros>{{$attribute->value}}</QTDBanheiros>
						@break
					@case('Vagas na Garagem')
						<QTDVagas>{{$attribute->value}}</QTDVagas>
						@break
					@case('Ar Condicionado')
						<ArCondicionado>1</ArCondicionado>
						@break
					@case('Sala de Ginástica')
						<SalaGinastica>1</SalaGinastica>
						@break
					@case('Varanda')
						<Varanda>1</Varanda>
						@break
					@case('Área de Serviço')
						<AreaServico>1</AreaServico>
						@break
					@case('Churrasqueira')
						<Churrasqueira>1</Churrasqueira>
						@break
					@case('Quanto de Serviço')
						<QuartoWCEmpregado>1</QuartoWCEmpregado>
						@break
					@case('Piscina')
						<Piscina>1</Piscina>
						@break
					@case('Salão de Festas')
						<SalaoFestas>1</SalaoFestas>
						@break
					@case('Portaria')
						<Porteiro>1</Porteiro>
						@break
					@case('Segurança 24h')
						<Porteiro>1</Porteiro>
						@break
				@endswitch
			@endforeach
			
		</Imovel>
        @endforeach
    </Imoveis>
</Carga>