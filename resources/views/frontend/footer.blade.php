<footer>
    <div class="footer">
        <div class="container">
            <div class="row d-flex align-items-center">

                <div class="col-lg-4 col-xs-12 logoRodape margin-top_half text-left" style="padding-left: 30px !important;">
                    <a href="{{route('home')}}"><img src="{{ asset('assets/images/data/'.$data->logo_footer) }}" alt="" srcset=""></a>
                </div>

                <div class="col-lg-4 col-xs-12  textoRodape icons text-center">
                    <div>
                        @foreach($socialMedia as $sm)
                        <a href="{{$sm->link}}" title="{{$sm->name}}" id="{{$sm->id}}" class="light-blue-text" target="_blank">
                            <i class="{{$sm->icon}} text-xxxl footerIconesMidiasSociais " onmouseover="hover(this)" onmouseout="noHover(this)"></i>
                        </a>
                        @endforeach
                    </div>
                    <div>
                        <strong>{{$data->name}}</strong> <br/>
                        <strong> CRECI {{$data->creci}}</strong><br/>
                        {{$data->address}}, {{$data->number}} - {{$data->district}} <br/>
                        {{$data->cep}} - {{$data->city->name}}-{{$data->state->initials}}
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12 textoRodape phonesRodape margin-top_half icons text-right" style="padding-right: 30px !important;" >
                    <div>
                        {{$data->phone}}
                    </div>
                    <div>
                        @foreach($phones as $phone)
                        <span>{{ $phone->phone }}</span><br/>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-footer text-center">
        <a href="http://www.prestige.com.br/" target="_blank">
            <img src="{{ asset('site/images/prestige.png') }}" class="img-fluid">
        </a>
    </div>
</footer>

<script>
    function hover(x) {

        $(x).css('color', '{{$colors->footerIconesMidiasSociaisHover}}');
    }

    function noHover(x) {
        $(x).css('color', '{{$colors->footerIconesMidiasSociais}}');
    }

    function textHover(x) {

        $(x).css('color', '{{$colors->textoRodapeHover}}');
    }

    function textNoHover(x) {
        $(x).css('color', '{{$colors->textoRodape}}');
    }
</script>