@extends('frontend.master')
@section('title', $data->name)
@section('description', $data->name)

@section('content')
@if(count($banners) != 0)
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach($banners as $key => $banner)
        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : ''}}">
        </li>
        @endforeach
    </ol>
    <div class="carousel-inner banner">
        @foreach ($banners as $key => $banner)
        @if($banner->link != NULL)
        <a class="carousel-item {{ $key == 0 ? 'active' : ''}}" href="{{$banner->link}}" target="{{$banner->newTab}}" title="{{ $banner->title }}">
            <img src="{{ asset('assets/images/banner/'.$banner->image)}}" class="d-block w-100" alt="{{ $banner->title }}">
        </a>
        @else
        <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
            <img src="{{ asset('assets/images/banner/'.$banner->image)}}" class="d-block w-100" alt="{{ $banner->title }}">
        </div>
        @endif
        @endforeach
    </div>
</div>
@endif
@include('frontend.partials.filter')

<div class="container">
    @if(count($venda) > 0)
    <div class="text-center col col-md-12 mt-5" style="font-size: 1.7em;">Destaques para <strong>venda</strong> </div>
    <div class="property-list">
        @foreach ($venda as $item)
        <div class="property-content margin-top col-md-3">
            <a href="{{ route('site.imoveis.show', $item->slug) }}" class="hover-img" title="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">

                @if(!isset($item->image) || ($item->image) == "")
                <img src="{{ asset('assets/images/property/sem.jpg')}}" class="img-fluid img-blog" alt="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">
                @else
                <img src="{{ asset('assets/images/property/thumb/'.$item->image)}}" class="img-fluid img-blog" alt="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">
                @endif
                <div class="property-info">
                    <div class="font-weight-bold">
                        {{ $item->getFrontTitle() }}
                    </div>
                    
                    @if($item->condominium != null)
                    <span>Condomínio {{$item->condominium->name}}</span><br>
                    @endif
                    <span>{{$item->district->city->name}}-{{$item->district->city->state->initials}}</span><br>
                    <p class="external_id">Cod: {{$item->external_id}}</p>
                    <div class="priceItem">
                        Venda: <span class="font-weight-bold">{{ floatFormat($item->price) }}</span>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
    @endif
</div>

@if(count($aluguel) > 0 )
<div class="divider margin-top"></div>
@endif

<div class="container">
    @if(count($aluguel) > 0)
    <div class="text-center col col-md-12 mt-5" style="font-size: 1.7em;">Destaques para <strong>locação</strong> </div>
    <div class="property-list">
        @foreach ($aluguel as $item)
        <div class="property-content margin-top col-md-3">
            <a href="{{ route('site.imoveis.show', $item->slug) }}" class="hover-img" title="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">

                @if(!isset($item->image) || ($item->image) == "")
                <img src="{{ asset('assets/images/property/sem.jpg')}}" class="img-fluid img-blog" alt="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">
                @else
                <img src="{{ asset('assets/images/property/thumb/'.$item->image)}}" class="img-fluid img-blog" alt="{{\Illuminate\Support\Str::limit($item->getTitle(), 80)}}">
                @endif
                <div class="property-info">
                    <div class="font-weight-bold">
                        {{ $item->getFrontTitle() }}
                    </div>
                    
                    @if($item->condominium != null)
                    <span>Condomínio {{$item->condominium->name}}</span><br>
                    @endif
                    <span>{{$item->district->city->name}}-{{$item->district->city->state->initials}}</span><br>
                    <p class="external_id">Cod: {{$item->external_id}}</p>
                    <div class="priceItem">
                        Locação: <span class="font-weight-bold"> {{ floatFormat($item->price) }}</span>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
    @endif
</div>

@endsection
