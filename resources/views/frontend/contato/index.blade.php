@extends('frontend.master')
@section('title', 'Contato - '. $data->name)

@section('content')
<div class="container">
    <div class="conteudo">

        <div class="cabecalho">
            <h1>Contato</h1>
            <p class="linhaFina">Preencha o formulário e envie sua mensagem</p>
        </div>
        @if (session()->has('success'))
        <div class="row justify-content-md-center" style="padding: 25px;">
            <div class="col-md-6 col-xs-12 contatoEnviado" >
                {{ session()->get('success') }}
            </div>
        </div>
        @endif
        <div class="row justify-content-md-center">
            <div class="col-md-6 col-xs-12">
                <form action="{{route('site.contactSave')}}" method="POST" id="contact-form" class="contact-form">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="form-group col-md-12 no-margin">
                            <label for="name" class="text-sm">Nome Completo</label>
                            <input type="text" class="form-control item" id="name" name="name" value="{{ old('name') }}" required>
                            <label id="name-error" class="error" for="name"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 no-margin">
                            <label for="email" class="text-sm">E-mail</label>
                            <input type="email" class="form-control item" id="email" name="email" value="{{ old('email') }}" required>
                            <label id="email-error" class="error" for="email"></label>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone" class="text-sm">Telefone/Celular</label>
                            <input type="phone" class="form-control phone item" id="phone" name="phone" value="{{ old('phone') }}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 no-margin">
                            <label for="subject" class="text-sm">Assunto</label>
                            <input type="text" class="form-control item" id="subject" name="subject" value="{{ old('subject') }}" required>
                            <label id="name-error" class="error" for="subject"></label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12 no-margin">
                            <label for="message" class="text-sm">
                                Digite sua mensagem
                            </label>
                            <textarea class="form-control item" rows="6" id="message" required name="message">{{ (isset($info) ? $info : '')}}{{ old('message') }}</textarea>
                            <label id="message-error" class="error" for="message"></label>
                        </div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="form-group col-md-12 no-margin">
                            @include('frontend.partials.recaptcha')
                        </div>
                    </div>



                    <div class="text-center margin-bottom ml-3" id="loading">
                        <button class="btn   buttonColor
                            text-uppercase medium-button font-weight-bold" id='call-to-action'>
                            Enviar
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('site/js/form.min.js') }}"></script>
@endsection