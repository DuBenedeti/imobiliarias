<!DOCTYPE html>
<!-- Desenvolvido e/ou gerenciado por Eduardo Benedeti -->
<html lang="pt-BR">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1,shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{ asset('assets/images/data/'.$data->favicon) }}" />
    <link rel="canonical" href="{{Request::url()}}" />
    <title>@yield('title', $data->initials)</title>
    <meta name="description" content="@yield('description')">
    <meta name="title" content="@yield('title', $data->name)">
    <meta property="og:locale" content="pt_BR">
    <meta property="og:site_name" content="{{$data->name}}" />
    <meta property="og:title" content="@yield('title')">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:url" content="{{Request::url()}}">
    <meta name="author" content="Eduardo G Benedeti - Prestige" />
    <meta property="og:image" content="@yield('og_image',  asset('assets/images/data/'.$data->logo_d) )">
    @yield('meta')

    @yield('css')
    {{ Html::style( asset('site/css/style.min.css')) }}
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link rel="stylesheet" href="{{ asset('assets/css/frontend.css') }}" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    {{-- JS TEMPLATE --}}
    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.2.0.min.js" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrapv3/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-block-ui/jqueryblockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-mask/jquery.mask.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/boostrap-clockpicker/bootstrap-clockpicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/dropzone.js') }}" type="text/javascript"></script>
    {{-- JS PERSONAL --}}
    <script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/knob/jquery.knob.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/noty/jquery.noty.packaged.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/cropper/cropper.min.js') }}" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1JhAcUiWd45YFiqJv1tQ_EhslrOzhfLM"></script>
    <script src="{{ asset('webarch/js/webarch.js') }}" type="text/javascript"></script>
    <script src="{{ asset('webarch/js/custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/ios-switch/ios7-switch.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/form_elements.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/plugins/cidades-estados.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <link rel="stylesheet" href="{{ asset('assets/font/fontawesome/css/all.css') }}" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','{{ $data->gtm_code }}');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ $data->gtm_code }}"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    @include('frontend.header')
    <div class="overlay"></div>

    <div>
        @yield('content')
    </div>

    <?php

    $exibirPopup = false;

    if (isset($pop[0]->image)) {
        if (!isset($_COOKIE["viewPopup"])) {
            setcookie('viewPopup', 'YES', time() + 900);
            $exibirPopup = true;
        }
    }

    ?>

    {{-- Modal --}}
    @if($exibirPopup)
    @if(isset($pop[0]->image))
    <div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="popupLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <a href="{{$pop[0]->link}}">
                        <img src="{{ asset('assets/images/banner/'.$pop[0]->image)}}" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endif
    
    <div class="whatsappfixoIcon">
        <a href="{{ whatsappLink($data->phone, '+55', 'Olá, entrei em contato pelo site e gostaria de tirar algumas dúvidas') }}" class="whatsapp-fixed" aria-label="Conversar pelo Whatsapp" target="_blank" rel="noopener">
        <img src="{{asset('assets/img/icon-whatsapp.png')}}" alt="Imagem de icone Whatsapp" class=""
        data-src="{{asset('assets/img/icon-whatsapp.png')}}"></a>
    </div>
    
    @include('frontend.footer')

    <script src="{{ asset('site/js/all.min.js') }}"></script>
    @yield('js')

</body>

</html>
<style>
    .header {
        background-color: <?php echo $colors->barraTopo ?> !important;
    }
    .showFilters{
        background-color: <?php echo $colors->barraFiltro ?> !important;
        color: <?php echo $colors->iconMenu ?> !important;
    }
    .propertiesFilter {
        background-color: <?php echo $colors->barraFiltro ?> !important;
    }

    #navbarDesktop {
        background-color: <?php echo $colors->fundoMenu ?> !important;
    }

    #navbarNav{
        background-color: <?php echo $colors->fundoMenu ?> !important;
    }
    .itensMenu li {
        border-color: <?php echo $colors->lineMenu ?> !important;
    }
    .itensMenu li a{
        color: <?php echo $colors->textoMenu ?> !important;
    }
    .foneMenu{
        color: <?php echo $colors->textoMenu ?> !important;
    }

    .itensMenu li a:hover{
        color: <?php echo $colors->hoverMenu ?> !important;
    }

    .redesSociais a{
        color: <?php echo $colors->topIconesMidiasSociais ?> !important;
    }

    .redesSociais a:hover{
        color: <?php echo $colors->topIconesMidiasSociaisHover ?> !important;
    }

    #showNavbarButton i{
        color: <?php echo $colors->iconMenu ?> !important;
    }

    .orange-text {
        color: <?php echo $colors->iconesLinks ?> !important;
    }

    .iconServices {
        background-color: <?php echo $colors->icones ?> !important;
    }

    .iconContent {
        border-color: <?php echo $colors->icones ?> !important;
    }

    .iconContent {
        color: <?php echo $colors->icones ?> !important;
    }

    .buttonColor {
        background-color: <?php echo $colors->botoes ?> !important;
        border-color: <?php echo $colors->botoes ?> !important;
        color: <?php echo $colors->textoBotaoConteudo ?> !important;
    }

    .buttonColor:hover {
        background-color: <?php echo $colors->hoverBotaoConteudo ?> !important;
        border-color: <?php echo $colors->hoverBotaoConteudo ?> !important;
        color: <?php echo $colors->hoverTextoBotaoConteudo ?> !important;
    }

    .linkContent {
        color: <?php echo $colors->links ?> !important;
    }

    .texto {
        color: <?php echo $colors->textoTopo ?> !important;
    }

    .textoRodape {
        color: <?php echo $colors->textoRodape ?> !important;
    }

</style>

<script>
    window.onload = function() {

        $('.footerIconesMidiasSociais').css('color', '{{$colors->footerIconesMidiasSociais}}');
        $('.topIconesMidiasSociais').css('color', '{{$colors->topIconesMidiasSociais}}');

        //CONTEUDO
        $('.aside-menu--header').css('background-color', '{{$colors->barraTopo}}');
        $('.green-text').css('color', '{{$colors->icones}}');
        $('.tumb-tag').css('background-color', '{{$colors->barraTopo}}');

        //Footer
        $('.footer').css('background-color', '{{$colors->barraRodape1}}');
        $('.bottom-footer').css('background-color', '{{$colors->barraRodape2}}');
    }
</script>