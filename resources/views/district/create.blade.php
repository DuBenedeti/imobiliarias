@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('district.index') }}">Bairros</a></li>
                <li><a class="active">Cadastrar</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('district.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Cadastrar novo <span class="semi-bold">bairro</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">


                        {!! Form::open([
                            'route' => 'district.store',
                            'files' => true
                        ]) !!}


                        @include('district.form', ['submitButtonText' => 'Cadastrar', 'states' => $states])


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection