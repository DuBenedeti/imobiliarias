@extends('layouts.app')

@section('content')
<style>
    .text-description {
        padding: 10px;
        margin-bottom: 10px;
    }
</style>

<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>VOCÊ ESTÁ AQUI</p>
            </li>
            <li><a class="active">Imóveis</a></li>
        </ul>
        <div class="page-title">
            <div class="row">
                <div class="col-md-6">
                    <a href="{{route('settings.index')}}">
                        <i class="icon-custom-left"></i>
                    </a>
                    <h3>Imóveis</h3>
                </div>
                <div class="col-md-6 p-t-15">
                    <div class="text-right text-center-xs">
                        <a href="{{ route('properties.create') }}" class="btn btn-success btn-df-xs btn-small no-ls">
                            <span class="fa fa-plus"></span> Cadastrar
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('properties.filters')

        <div class="grid simple">
            <div class="grid-title no-border">
                <div class="pull-left">
                    <h4>
                        Lista de <span class="semi-bold">imóveis</span>
                        @if (isset($trash)) excluídos @endif
                    </h4>
                </div>
                @if (!isset($trash))
                <div class="pull-left m-l-15">
                    <div class="selected-options inline-block" style="visibility:hidden">
                        <a href="#" class="btn btn-small btn-white delete" data-toggle="tooltip" data-original-title="Excluir selecionados">
                            <i class="fa fa-fw fa-trash"></i>
                            {{ csrf_field() }}
                        </a>
                    </div>
                </div>
                @endif
                <div class="clearfix"></div>
            </div>
            <div class="grid-body no-border">
                @if (count($properties) == 0)
                <h5>Nenhum cadastro.</h5>
                @else
                <table class="table table-striped table-hover table-flip-scroll cf">
                    <thead class="cf">
                        <tr>
                            <th width="auto">Cód</th>
                            <th width="auto">Título</th>
                            <th width="auto">Preço</th>
                            <th width="auto">Negócio</th>
                            <th width="auto">Status</th>
                            <th width="90">Opções</th>
                        </tr>
                    </thead>
                    <tbody id="sortable" class="ui-sortable">
                        @foreach ($properties as $property)
                        <tr id="item_{{ $property->id }}">
                            <td>
                                {{$property->external_id}}
                            </td>
                            <td>
                                {{$property->getTitle()}}
                            </td>
                            <td>
                                {{floatFormat($property->price)}}
                            </td>
                            <td>
                                @if ($property->type_business_id == 1)
                                  Compra
                                @elseif ($property->type_business_id == 2)
                                  Aluguel
                                @endif
                            </td>
                            <td>
                            <input type="checkbox" data-id="{{ $property->id }}" data-type="{{$property->mobile}}" class="js-switch"
                                                value="@if ($property->status == 1) 1 @else 0 @endif"
                                                @if ($property->status == 1) checked @endif data-video="{{ $property->video }}"/>
                            </td>
                            
                            <td>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-window-restore"></i> Ações
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{  route('properties.photos', [$property->id]) }}">
                                            <i class="fa fa-image"></i> Fotos
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{  route('properties.edit', [$property->id]) }}">
                                            <i class="fa fa-edit"></i> Editar
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{  route('properties.destroy', [$property->id]) }}">
                                            <i class="fa fa-trash"></i> Excluir
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pages">
                    <div class="pull-left results">
                        <strong>{{ $properties->total()}}</strong> registro(s)
                    </div>

                    <div class="pull-right">
                        {!! $properties->appends(Request::except('page'))->links() !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).on('change', '.js-switch', function() {
        var url = window.location.pathname + "/status";
        var id = $(this).attr('data-id');
        var status = $(this).prop('checked');

        if (status == true)
            status = 1;
        else status = 0;

        $.get(url, {
            id: id,
            status: status
        }, function(code) {
        console.log(status);
            if (code != 200)
                noty({
                    text: "Ocorreu um problema! Tente novamente mais tarde.",
                    type: 'error'
                });
            if (code == 200)
                noty({
                    text: "Status alterado.",
                    type: 'success'
                });
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#sortable').sortable({
            handle: '.handle',
            update: function(event, ui) {
                var url = "/sistema/menu/order";
                var data = $(this).sortable('serialize');

                console.log(data);

                $.post(url, data, function(res) {
                    console.log(res);
                    if (res != 200) {
                        noty({
                            text: "Ocorreu um erro! Tente novamente.",
                            type: 'error'
                        });
                    }
                });
            }
        });
    });
</script>
@endsection