@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('properties.index') }}">Imóvel</a></li>
                <li><a class="active">Alterar: {{$property->getTitle()}}</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('properties.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Alterar <span class="semi-bold">Imóvel</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">

                        <div class="grid-body no-border">


                            {!! Form::model($property, [
                                                    'method' => 'PATCH',
                                                    'route' => ['properties.update', $property->id],
                                                    'files' => true
                                                ]) !!}


                            @include('properties.form-edit', ['submitButtonText' => 'Alterar',
                                                            'var' => $property])


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

