@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="content">

        <!-- BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <p>VOCÊ ESTÁ AQUI</p>
            </li>
            <li><a href="{{ route('properties.index') }}">Imóvel</a></li>
            <li><a class="active">Fotos: {{$property->getTitle()}}</a></li>
        </ul>

        <!-- TITLE-->
        <div class="page-title">
            <a href="{{ route('properties.index') }}">
                <i class="icon-custom-left"></i>
            </a>
            <h3>
                Adicionar <span class="semi-bold">Fotos</span>
            </h3>
        </div>

        <!-- CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Enviar Fotos</h4>
                    </div>

                    <div class="grid-body no-border">
                        <div class="row-fluid">
                            <form action="{{ route('properties.upload', $property->id) }}" method="post" class="dropzone no-margin dz-clickable" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="dz-default dz-message"><span>Arraste os arquivos aqui para enviar</span></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- PHOTOS -->
        @if (count($photos))
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>
                            Fotos do Imóvel
                        </h4>
                    </div>

                    <div class="grid-body no-border">
                        <div class="row" id="sortable">
                            @foreach ($photos as $photo)
                            <div class="col-md-3" id="item_{{ $photo->id }}">
                                <a href="javascript:;" class="photos js-photo-active {{ $photo->featured? 'active' : '' }}" data-id="{{ $photo->id }}">
                                    <img src="/assets/images/property/{{ $photo->image }}" alt="" class="img-responsive">
                                </a>

                                <span class="move handle">
                                    <i class="fas fa-arrows-alt"></i>
                                </span>

                                <form action="{{ route('properties.photos.delete', $photo->id) }}" method="post" class="img-delete">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button type="submit"><i class="fa fa-times"></i></button>
                                </form>

                                <form action="{{ route('properties.photos.caption', $photo->id) }}" method="post">
                                    {!! csrf_field() !!}
                                    <input type="text" name="caption" class="form-control" placeholder="Legenda" value="{{ $photo->subtitle? $photo->subtitle : old('caption') }}">
                                    <div class="row m-t-5">
                                        <div class="col-md-9 p-r-5">
                                            <input type="text" name="credits" class="form-control" placeholder="Créditos" value="{{ $photo->credits? $photo->credits : old('credits') }}">
                                        </div>
                                        <div class="col-md-3 p-l-0">
                                            <button class="btn btn-success btn-block">OK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions m-b-5">
                <div class="pull-right">

                <a class="btn btn-success" href="{{ route('properties.index') }}">Salvar</a>
                </div>
            </div>
        </div>
        @endif

    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#sortable').sortable({
            handle: '.handle',
            update: function(event, ui) {
                var url = "/sistema/properties/photos/order";
                var data = $(this).sortable('serialize');
                $.post(url, data, function(res) {
                    console.log(res);
                    if (res != 200) {
                        noty({
                            text: "Ocorreu um erro! Tente novamente.",
                            type: 'error'
                        });
                    }
                });
            }
        });
    });
</script>
@endsection