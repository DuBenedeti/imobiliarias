<div class="grid-title no-border">

</div>

<div class="grid-body no-border">

  <div class="row">
    <div class="col-md-2 col-xs-12">
      <div class="form-group">Código</label>
        <input class="form-control"type="text" name="external_id" id="external_id" class="form-control" value="{{ old('external_id') ?? $property->external_id ?? '' }}"  disabled/>
      </div>
    </div>

    <div class="col-md-5 col-xs-12">
      <div class="form-group">
        <label class="required" for="type_business_id">Tipo do negócio</label>
        <select name="type_business_id" id="type_business_id" class="form-control">
          @foreach ($typeBusiness as $type)
          <option value="{{ $type->id }}">
            {{ $type->name }}
          </option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-md-5 col-xs-12">
      <div class="form-group">
        <label class="required" for="type_property_id">Tipo do imóvel</label>
        <select name="type_property_id" id="type_property_id" class="form-control" data-url="{{ route('getPropertyAttributes', 1) }}">
          <option value="0">
            Tipo
          </option>
          @foreach ($tipoImovel as $type)
          <option value="{{ $type->id }}">
            {{ $type->name }}
          </option>
          @endforeach
        </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4 col-xs-12" id="building-area-container">
      <div class="form-group">
        <label for="meters">Área Construída <strong>(M²)</strong></label>
        <input type="number" min="0" step="0.01" name="meters" id="meters" class="form-control" />
      </div>
    </div>

    <div class="col-md-4 col-xs-12" id="total-area-container">
      <div class="form-group">
        <label class="required" for="total_area">Área Total <strong>(M²)</strong></label>
        <input type="number" min="0" step="0.01" name="total_area" id="total_area" class="form-control" />
      </div>
    </div>

    <div class="col-md-4 col-xs-12" id="price-container">
      <div class="form-group">
        <label class="required" for="price">Valor <strong>(R$)</strong></label>
        <input type="text" name="price" id="price" class="form-control" data-mask="000.000.000,00" data-mask-reverse="true" />
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-3 col-xs-12">
      <div class="form-group">
        <label for="zipcode">CEP</label>
        <input type="text" name="zipcode" id="zipcode" class="form-control cep-mask js-zipcode" />
      </div>
    </div>

    <div class="col-md-3 col-xs-12">
      {{-- state --}}
      <div class="form-group">
        {!! Form::label('state', 'Estado', ['class' => 'form-label required']) !!}
        {!! Form::select('state', $states, $data->state_id ?? null, ['class' => 'form-control js-state', 'data-default' => $data->state_id ?? ""]) !!}
      </div>
    </div>

    <div class="col-md-3 col-xs-12">
      {{-- city --}}
      <div class="form-group {{ $errors->first('city_id')? 'has-error' : '' }}">
        {!! Form::label('city', 'Cidade', ['class' => 'form-label required']) !!}
        {!! Form::select('city', ['Selecione o UF'], $data->city_id ?? null, ['class' => 'form-control js-city', 'data-default' => $data->city_id ?? ""]) !!}
        <small class="error">{{ $errors->first('city_id') }}</small>
      </div>
    </div>

    <div class="col-md-3 col-xs-12">
      {{-- district --}}
      <div class="form-group {{ $errors->first('district_id')? 'has-error' : '' }}">
        {!! Form::label('district', 'Bairro', ['class' => 'form-label required']) !!}
        <a href="{{route('district.create')}}">Cadastrar Bairro</a>
        {!! Form::select('district', ['Selecione uma cidade'], $condominium->district_id ?? null, ['class' => 'form-control', 'data-default' => $condominium->district_id ?? ""]) !!}
        <small class="error">{{ $errors->first('district_id') }}</small>
      </div>
    </div>
    
  </div>

  <div class="row">
    <div class="col-md-3 col-xs-12">
      {{-- condominium --}}
      <div class="form-group {{ $errors->first('condominium_id')? 'has-error' : '' }}">
        {!! Form::label('condominium', 'Condomínio', ['class' => 'form-label']) !!}
        <a href="{{route('condominium.create')}}">Cadastrar Condomínio</a>
        {!! Form::select('condominium', ['Selecione um condomínio'], $condominium->condominium_id ?? null, ['class' => 'form-control', 'data-default' => $condominium->condominium_id ?? ""]) !!}
        <small class="error">{{ $errors->first('condominium_id') }}</small>
      </div>
    </div>
    <div class="col-md-6 col-xs-12">
      <div class="form-group">
        <label for="place">Endereço</label>
        <input type="text" name="place" id="place" class="form-control js-address" />
      </div>
    </div>

    <div class="col-md-3 col-xs-12">
      <div class="form-group">
        <label for="number">Número</label>
        <input type="text" name="number" id="number" class="form-control" />
      </div>
    </div>

  </div>

  <div id='boolean-attributes' name='boolean-attributes'>
  </div>
  <div id='money-attributes' name='money-attributes' class="row">
  </div>
  <div id='number-attributes' name='number-attributes' class="row">
  </div>
  <div id='text-attributes' name='text-attributes' class="row">
  </div>


  <div class="row">
    <div class="col-xs-12">
      <label for="details">Descrição do imóvel</label>
      <textarea name="details" id="ckeditor" class="form-control" rows="7" style="resize: true;">{{ old('details') ?? $property->details ?? '' }}</textarea>
    </div>
  </div>
  <!-- <div class="row">
    <div class="col-md-12 col-xs-12" style="margin-top: 20px;">
      <div class="form-group">
        <label for="video">Vídeo</label>
        <input type="text" name="video" id="video" class="form-control js-address" />
      </div>
    </div>
  </div> -->
  <div class="row">
    <div class="col-md-6 col-xs-12" id="image">
      <div class="form-group">
        <label for="file" class="required">Foto principal
          <span>
            <small> Você poderá enviar mais fotos posteriormente</small>
          </span>
        </label>

        <div class="clearfix"></div>

        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="max-width:250px; max-height:250px;">
            {{-- Preview --}}
            <img data-src="http://placehold.it/240x240/eee/aaa/?text=Imagem" alt="" width="240" height="240" />
          </div>

          <div class="fileinput-preview fileinput-exists thumbnail" style="width:250px; height:250px;"></div>

          <div>
            <span class="btn btn-default btn-file">
              <span class="fileinput-new">Selecionar imagem</span>
              <span class="fileinput-exists">Trocar</span>
              {{-- INPUT --}}
              <input type="file" name="image" id="image" required/>
            </span>
            <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">
              Remover
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="form-actions m-b-5">
    <div class="pull-right">
      <button class="btn btn-success" type_id="submit">
        <i class="fa fa-check"></i>
        {{ $submitButtonText }}
      </button>
      <a class="btn btn-white" href="{{ route('properties.index') }}">Cancelar</a>
    </div>
  </div>
</div>


@section('js')
@include('helpers.zipcode-fill')
<script src="{{ asset('assets/js/loadCities.js') }}"></script>
<script src="{{ asset('assets/js/loadDistricts.js') }}"></script>
<script src="{{ asset('assets/js/loadCondominiums.js') }}"></script>
<script src="{{ asset('assets/js/loadPropertiesAttributes.js') }}"></script>

<link href="{{ asset('assets/css/summernote.css') }}" rel="stylesheet">
    <script src="{{asset('assets/js/summernote.min.js')  }}"></script>
    <script src="{{ asset('assets/js/summernote-pt-BR.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#ckeditor').summernote({
                lang: 'pt-BR',
                height: 610,
                toolbar: [ 
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['#616161']],
                    ['insert', ['picture','link','table','hr', 'video']],
                    ['fontsize', ['fontsize']],
                    ['font', ['Arial']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['view', ['codeview']],
                ],
                fontSizes: ['12', '14', '16', '18', '20', '22'],
                callbacks: {
                    onImageUpload: function(files, editor, welEditable) {
                        sendFile(files[0],editor,welEditable);
                    },
                    onPaste: function (e) {
                        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                        e.preventDefault();
                        document.execCommand('insertText', false, bufferText);
                    }
                },
            });
        });

        function sendFile(file,editor,welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: "/sistema/dynamic-pages/saveimage",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    $('#ckeditor').summernote('insertImage', url);
                }
            });
        }
        
    </script>

    <script>
        $('.fileinput').on('change.bs.fileinput', function (event) {
            $('.fileinput-preview img').cropper({
                aspectRatio: 1 / 1,
                zoomable: false,
                minCropBoxWidth: 150,
                minCropBoxHeight: 150,
                crop: function (e) {
                    $('#x').val(e.x);
                    $('#y').val(e.y);
                    $('#width').val(e.width);
                    $('#height').val(e.height);
                }
            });
        });
    </script>
@endsection