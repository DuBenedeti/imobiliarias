<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h4>Filtros</h4>
            </div>

            <div class="grid-body no-border">
                <form action="{{ route('properties.index') }}" method="get" id="filter-form">

                    <div class="row">
                        <div class="col-md-3 col-xs-12 m-b-10">
                            <div class="input-append default no-padding col-xs-12">
                                <input type="text" name="title" class="form-control" placeholder="Título">
                            </div>
                        </div>

                        <div class="col-md-1 col-xs-12 m-b-10">
                            <div class="input-append default no-padding col-xs-12">
                                <input type="text" name="external_id" class="form-control" placeholder="Código">
                            </div>
                        </div>

                        <div class="col-md-2 col-xs-12 m-b-10">
                            <select name="type_property_id" id="type_property_id" class="form-control" data-url="{{ route('getPropertyAttributes', 1) }}">
                                <option value="">
                                    Tipo do imóvel
                                </option>
                                @foreach ($tipoImovel as $type)
                                <option value="{{ $type->id }}">
                                    {{ $type->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-3 col-xs-12 m-b-10">
                            <div class="input-append default no-padding col-xs-12">
                                <input type="text" name="start_price" class="form-control money-mask" placeholder="Preço mínimo">
                            </div>
                        </div>

                        <div class="col-md-3 col-xs-12 m-b-10">
                            <div class="input-append default no-padding col-xs-12">
                                <input type="text" name="end_price" class="form-control money-mask" placeholder="Preço máximo">
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="radio p-t-5">
                                <input type="radio" id="all" name="status" value="" checked>
                                <label for="all">Todos</label>

                                <input type="radio" id="active" name="status" value="1">
                                <label for="active">Ativos</label>

                                <input type="radio" id="not-active" name="status" value="0">
                                <label for="not-active">Inativos</label>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="radio p-t-5">
                                <input type="radio" id="business_all" name="business" value="" checked>
                                <label for="business_all">Todos</label>

                                <input type="radio" id="business_active" name="business" value="1">
                                <label for="business_active">Comprar</label>

                                <input type="radio" id="business_not-active" name="business" value="2">
                                <label for="business_not-active">Alugar</label>

                                <input type="radio" id="business_season" name="business" value="3">
                                <label for="business_season">Temporada</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- remove filters button -->
                            @if ($_GET)
                            <a href="{{ route('properties.index') }}" class="btn btn-mini btn-default">
                                <i class="fa fa-times"></i> Limpar filtros
                            </a>
                            @endif

                            <!-- submit filtering -->
                            <button type="submit" class="btn btn-primary btn-small btn-cons pull-right">Filtrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
 $('.money-mask').mask('000.000.000,00', {reverse: true});
</script>