@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('propertyContact.index') }}">Contatos</a></li>
                <li><a class="active">Visualizar: {{ $propertyContact->name }}</a></li>

            </ul>


            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('propertyContact.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Visualizar <span class="semi-bold">Interessados</span>
                </h3>


            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">

                        <div class="grid-body no-border">


                            {!! Form::model($propertyContact, [
                                                    'method' => 'PATCH',
                                                    'route' => ['propertyContact.update', $propertyContact->id],
                                                    'files' => true
                                                ]) !!}


                            @include('propertyContact.form', ['submitButtonText' => 'Alterar',
                                                            'var' => $propertyContact])


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

