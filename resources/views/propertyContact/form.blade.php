<div class="grid-title no-border">
    <h4> Dados do Interessado</h4>
</div>

<div class="grid-body no-border">

    <div class="row">
        <div class="form-group col-md-4 {{ $errors->first('dateVisit')? 'has-errors' : '' }}">
            {!! Form::label('dateVisit', 'Visita', ['class' => 'form-label']) !!}
            {!! Form::text('dateVisit', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('dateVisit') }}</small>
        </div>
        <div class="form-group col-md-8 {{ $errors->first('name')? 'has-errors' : '' }}">
            {!! Form::label('name', 'Nome', ['class' => 'form-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('name') }}</small>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-6 {{ $errors->first('email')? 'has-errors' : '' }}">
            {!! Form::label('email', 'E-mail', ['class' => 'form-label']) !!}
            {!! Form::text('email', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('email') }}</small>
        </div>
        <div class="form-group col-md-6 {{ $errors->first('phone')? 'has-errors' : '' }}">
            {!! Form::label('phone', 'Telefone', ['class' => 'form-label']) !!}
            {!! Form::text('phone', null, ['class' => 'form-control', 'disabled' ]) !!}
            <small class="error">{{ $errors->first('phone') }}</small>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12 {{ $errors->first('message')? 'has-errors' : '' }}">
            {!! Form::label('message', 'Mensagem', ['class' => 'form-label']) !!}
            {!! Form::textarea('message', null, ['class' => 'form-control', 'disabled', 'style' => 'resize: none;']) !!}
            <small class="error">{{ $errors->first('caption') }}</small>
        </div>
    </div>
    

</div>