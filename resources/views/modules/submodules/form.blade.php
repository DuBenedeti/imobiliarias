<style>
    #doc {
    display: none
}
/* Aparência que terá o seletor de arquivo */
.doc {
    background-color: #3498db;
    color: #fff !important;
    cursor: pointer;
    padding: 6px 50px;
}
#arq{
    font: 1.1em sans-serif;
}
.cores{
    border: none !important;
}
.fa-file{
    font-size: 15px
}

    </style>
<div class="grid-title no-border">

</div>

<div class="grid-body no-border">

    <div class="row">
        <div class="form-group col-md-4 {{ $errors->first('name')? 'has-errors' : '' }}">
            {!! Form::label('name', 'Nome', ['class' => 'form-label required']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required' => 'required']) !!}
            <small class="title">{{ $errors->first('name') }}</small>
        </div>

        <div class="form-group col-md-4 {{ $errors->first('url')? 'has-errors' : '' }}">
        {!! Form::label('url', 'url', ['class' => 'form-label required']) !!}
            {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => '/sistema', 'required' => 'required']) !!}
            <small class="title">{{ $errors->first('url') }}</small>
        </div>
        
        <div class="form-group col-md-4 {{ $errors->first('status')? 'has-errors' : '' }}">
            {!! Form::label('status', 'status', ['class' => 'form-label required']) !!}
            <select name="status" class="form-label" value="{{( isset($modules->status) ? $modules->status : '')}}" id="status">
                <option value="Ativo" {{( isset($modules->status) ? ($modules->status == "Ativo" ? 'selected' : '') : '')}}>Ativo</option>
                <option value="Inativo" {{( isset($modules->status) ? ($modules->status == "Inativo" ? 'selected' : '') : '')}}>Desativado</option>
            </select>
        </div>

        <div class="form-group col-md-2 hide {{ $errors->first('position')? 'has-errors' : '' }}">
            {!! Form::label('position', 'Posição', ['class' => 'form-label required']) !!} 
            {!! Form::number('position', 0, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="title">{{ $errors->first('position') }}</small>
        </div>

        <div class="form-group col-md-4 {{ $errors->first('module_id')? 'has-errors' : '' }}">
            {!! Form::label('module_id', 'module', ['class' => 'form-label hide']) !!}
            {!! Form::text('module_id', $id, ['class' => 'form-control hide']) !!}
            <small class="title">{{ $errors->first('module_id') }}</small>
        </div>
        
    </div>

    <div class="form-actions m-b-5">
        <div class="pull-right">
            <button class="btn btn-success" type_id="submit">
                <i class="fa fa-check"></i>
                {{ $submitButtonText }}
            </button>
            <a class="btn btn-white" href="{{ route('modules.index') }}">Cancelar</a>
        </div>
    </div>
</div>