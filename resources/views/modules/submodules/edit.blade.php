@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('modules.index') }}">Módulos</a></li>
                <li><a class="active">Alterar Submódulo: {{$modules->name}}</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('modules.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Alterar <span class="semi-bold">Submódulo</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">

                        <div class="grid-body no-border">

                            {!! Form::model($modules, [
                                                    'method' => 'PATCH',
                                                    'route' => ['modules.updateSub', $modules->id],
                                                    'files' => true
                                                ]) !!}

                            @include('modules.submodules.form', ['submitButtonText' => 'Alterar',
                                                                'var' => $modules])

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

