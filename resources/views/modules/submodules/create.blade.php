@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('modules.index') }}">Módulos</a></li>
                <li><a class="active">Submódulo - Cadastrar</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('modules.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Cadastrar novo <span class="semi-bold">registro</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">


                        {!! Form::open([
                            'route' => 'modules.storeSub',
                            'files' => true
                        ]) !!}


                        @include('modules.submodules.form', ['submitButtonText' => 'Cadastrar'])


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection