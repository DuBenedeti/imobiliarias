@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="content">
        <div class="title">
            <h2><i class="fa fa-cog"></i>Configurações</h2>
            <br>
        </div>

        <div class="col-md-4 col-sm-6 m-b-10">
            <a href="{{ route('data.edit', 1) }}">
                <div class="tiles white">
                    <div class="tiles-body" style="height: 140px; ">
                        <h2>Dados da empresa</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-6 m-b-10">
            <a href="{{ route('ajustes.index') }}">
                <div class="tiles white" style="height: 140px;">
                    <div class="tiles-body alinhar">
                        <h2>Ajustes</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-6 m-b-10">
            <a href="{{ route('colors.edit', 1) }}">
                <div class="tiles white" style="height: 140px;">
                    <div class="tiles-body alinhar">
                        <h2>Cores</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4 col-sm-6 m-b-10">
            <a href="{{ route('socialMedia.index') }}">
                <div class="tiles white" style="height: 140px;">
                    <div class="tiles-body alinhar">
                        <h2>Mídias Sociais</h2>
                    </div>
                </div>
            </a>
        </div>
        <!-- <div class="col-md-4 col-sm-6 m-b-10">
            <a href="{{ route('linksTopo.index') }}">
                <div class="tiles white" style="height: 140px;">
                    <div class="tiles-body alinhar">
                        <h2>Links do Topo</h2>
                    </div>
                </div>
            </a>
        </div> -->
        <div class="col-md-4 col-sm-6 m-b-10">
            <a href="{{ route('phones.index') }}">
                <div class="tiles white" style="height: 140px;">
                    <div class="tiles-body alinhar">
                        <h2>Telefones</h2>
                    </div>
                </div>
            </a>
        </div>

    </div>
</div>
@endsection