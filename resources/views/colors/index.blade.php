@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="content">

        <!-- BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <p>VOCÊ ESTÁ AQUI</p>
            </li>
            <li>Cores</li>
            <li><a class="active">Alterar</a></li>
        </ul>

        <!-- TITLE-->
        <div class="page-title">
            <a href="{{ route('settings.index') }}">
                <i class="icon-custom-left"></i>
            </a>
            <h3>
                Alterar <span class="semi-bold">Cores</span>
            </h3>
        </div>

        <!-- CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-body no-border">


                        {!! Form::model($colors, [
                        'method' => 'PATCH',
                        'route' => ['colors.update', $colors->id],
                        'files' => true
                        ]) !!}


                        @include('colors.form', ['submitButtonText' => 'Alterar',
                        'var' => $colors])


                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection