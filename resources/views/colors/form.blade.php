<div class="grid-body no-border">
    <div class="page-title">
        <h2>Conteúdo</h2>
    </div>

    <div class="row">
        <br>


        <div class="form-group col-md-2 {{ $errors->first('botoes')? 'has-errors' : '' }}">
            {!! Form::label('botoes', 'Botões', ['class' => 'form-label required']) !!}
            {!! Form::text('botoes', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('botoes', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('botoes') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('hoverBotaoConteudo')? 'has-errors' : '' }}">
            {!! Form::label('hoverBotaoConteudo', 'Botão hover', ['class' => 'form-label required']) !!}
            {!! Form::text('hoverBotaoConteudo', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('hoverBotaoConteudo', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('hoverBotaoConteudo') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('textoBotaoConteudo')? 'has-errors' : '' }}">
            {!! Form::label('textoBotaoConteudo', 'Texto botão', ['class' => 'form-label required']) !!}
            {!! Form::text('textoBotaoConteudo', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('textoBotaoConteudo', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('textoBotaoConteudo') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('hoverTextoBotaoConteudo')? 'has-errors' : '' }}">
            {!! Form::label('hoverTextoBotaoConteudo', 'Texto botão hover', ['class' => 'form-label required']) !!}
            {!! Form::text('hoverTextoBotaoConteudo', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('hoverTextoBotaoConteudo', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('hoverTextoBotaoConteudo') }}</small>
        </div>


    </div>

    <div class="page-title">
        <h2>Topo</h2>
    </div>
    
    <div class="row">
        <br>


        <div class="form-group col-md-2 {{ $errors->first('barraTopo')? 'has-errors' : '' }}">
            {!! Form::label('barraTopo', 'Barra', ['class' => 'form-label required']) !!}
            {!! Form::text('barraTopo', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('barraTopo', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('barraTopo') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('barraFiltro')? 'has-errors' : '' }}">
            {!! Form::label('barraFiltro', 'Barra de busca', ['class' => 'form-label required']) !!}
            {!! Form::text('barraFiltro', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('barraFiltro', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('barraFiltro') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('iconMenu')? 'has-errors' : '' }}">
            {!! Form::label('iconMenu', 'Botão de menu', ['class' => 'form-label required']) !!}
            {!! Form::text('iconMenu', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('iconMenu', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('iconMenu') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('topIconesMidiasSociais')? 'has-errors' : '' }}">
            {!! Form::label('topIconesMidiasSociais', 'Ícones Mídias Sociais', ['class' => 'form-label required']) !!}
            {!! Form::text('topIconesMidiasSociais', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('topIconesMidiasSociais', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('topIconesMidiasSociais') }}</small>
        </div>
        <div class="form-group col-md-3 {{ $errors->first('topIconesMidiasSociaisHover')? 'has-errors' : '' }}">
            {!! Form::label('topIconesMidiasSociaisHover', 'Ícones Mídias Sociais Hover', ['class' => 'form-label required']) !!}
            {!! Form::text('topIconesMidiasSociaisHover', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('topIconesMidiasSociaisHover', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('topIconesMidiasSociaisHover') }}</small>
        </div>


    </div>
    <div class="page-title">
        <h2>Menu</h2>
    </div>
    
    <div class="row">
        <br>


        <div class="form-group col-md-2 {{ $errors->first('fundoMenu')? 'has-errors' : '' }}">
            {!! Form::label('fundoMenu', 'Fundo', ['class' => 'form-label required']) !!}
            {!! Form::text('fundoMenu', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('fundoMenu', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('fundoMenu') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('textoMenu')? 'has-errors' : '' }}">
            {!! Form::label('textoMenu', 'Texto', ['class' => 'form-label required']) !!}
            {!! Form::text('textoMenu', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('textoMenu', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('textoMenu') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('hoverMenu')? 'has-errors' : '' }}">
            {!! Form::label('hoverMenu', 'Texto hover', ['class' => 'form-label required']) !!}
            {!! Form::text('hoverMenu', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('hoverMenu', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('hoverMenu') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('lineMenu')? 'has-errors' : '' }}">
            {!! Form::label('lineMenu', 'Traço', ['class' => 'form-label required']) !!}
            {!! Form::text('lineMenu', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('lineMenu', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('lineMenu') }}</small>
        </div>
    </div>
    <div class="page-title">
        <h2>Rodapé</h2>
    </div>
    
    <div class="row">
        <br>


        <div class="form-group col-md-2 {{ $errors->first('barraRodape1')? 'has-errors' : '' }}">
            {!! Form::label('barraRodape1', 'Barra 1', ['class' => 'form-label required']) !!}
            {!! Form::text('barraRodape1', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('barraRodape1', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('barraRodape1') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('barraRodape2')? 'has-errors' : '' }}">
            {!! Form::label('barraRodape2', 'Barra 2', ['class' => 'form-label required']) !!}
            {!! Form::text('barraRodape2', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('barraRodape2', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('barraRodape2') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('textoRodape')? 'has-errors' : '' }}">
            {!! Form::label('textoRodape', 'Texto', ['class' => 'form-label required']) !!}
            {!! Form::text('textoRodape', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('textoRodape', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('textoRodape') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('textoRodapeHover')? 'has-errors' : '' }}">
            {!! Form::label('textoRodapeHover', 'Texto Hover', ['class' => 'form-label required']) !!}
            {!! Form::text('textoRodapeHover', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('textoRodapeHover', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('textoRodapeHover') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('footerIconesMidiasSociais')? 'has-errors' : '' }}">
            {!! Form::label('footerIconesMidiasSociais', 'Ícones Mídias Sociais', ['class' => 'form-label required']) !!}
            {!! Form::text('footerIconesMidiasSociais', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('footerIconesMidiasSociais', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('footerIconesMidiasSociais') }}</small>
        </div>
        <div class="form-group col-md-3 {{ $errors->first('footerIconesMidiasSociaisHover')? 'has-errors' : '' }}">
            {!! Form::label('footerIconesMidiasSociaisHover', 'Ícones Mídias Sociais Hover', ['class' => 'form-label required']) !!}
            {!! Form::text('footerIconesMidiasSociaisHover', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! Form::color('footerIconesMidiasSociaisHover', null, ['class' => 'form-control', 'disabled']) !!}
            <small class="error">{{ $errors->first('footerIconesMidiasSociaisHover') }}</small>
        </div>


    </div>

    <div class="form-actions m-b-5">
        <div class="pull-right">
            <button class="btn btn-success" type_id="submit">
                <i class="fa fa-check"></i>
                {{ $submitButtonText }}
            </button>
            <a class="btn btn-white" href="{{ route('data.index') }}">Cancelar</a>
        </div>
    </div>
</div>

@section('js')
<link href="{{ asset('assets/css/summernote.css') }}" rel="stylesheet">
<script src="{{asset('assets/js/summernote.min.js')  }}"></script>
<script src="{{ asset('assets/js/summernote-pt-BR.js') }}"></script>
<script>
    window.onload = function() {
        $('#ckeditor').summernote({
            lang: 'pt-BR',
            height: 250,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['text', ['#616161']],
                ['insert', ['picture', 'link', 'table', 'hr', 'video']],
                ['fontsize', ['fontsize']],
                ['font', ['Arial']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
            ],
            fontSizes: ['14', '16', '18'],
            callbacks: {
                onImageUpload: function(files, editor, welEditable) {
                    sendFile(files[0], editor, welEditable);
                },
                onPaste: function(e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            }
        });
    };

    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "POST",
            url: "/sistema/press/noticias/saveimage",
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                $('#ckeditor').summernote('insertImage', url);
            }
        });
    }

    $('#cnpj').mask("99.999.999/9999-99");
    $('#phone').mask("(99) 99999-9999");
</script>
@endsection