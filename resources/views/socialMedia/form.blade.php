<div class="grid-body no-border">
    <div class="row ">
        <br>
        <div class="form-group col-md-2 {{ $errors->first('icon')? 'has-errors' : '' }}">
            {!! Form::label('icon', 'Ícone', ['class' => 'form-label required']) !!}
            {!! Form::text('icon', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('icon') }}</small>
        </div>
        <div class="form-group col-md-2 {{ $errors->first('name')? 'has-errors' : '' }}">
            {!! Form::label('name', 'Nome', ['class' => 'form-label required']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('name') }}</small>
        </div>
        <div class="form-group col-md-6 {{ $errors->first('link')? 'has-errors' : '' }}">
            {!! Form::label('link', 'Link', ['class' => 'form-label required']) !!}
            {!! Form::text('link', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('link') }}</small>

        </div>
        <div class="form-group col-md-2 " style="margin-top: 2%">

            @if($submitButtonText == 'Cadastrar')
            <button class="btn btn-success" type_id="submit">
                <i class="fa fa-check"></i>
            </button>
            @endif
            @if($submitButtonText != 'Cadastrar')
            <a class="btn btn-danger" href="{{ route('socialMedia.destroy', $var->id) }}">
                <i class="fas fa-trash"> </i> </a>
            @endif
        </div>
    </div>
</div>