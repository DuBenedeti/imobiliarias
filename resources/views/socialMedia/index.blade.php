@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="content">

        <!-- BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <p>VOCÊ ESTÁ AQUI</p>
            </li>
            <li>Mídias Sociais</li>
            <li><a class="active">Alterar</a></li>
        </ul>

        <!-- TITLE-->
        <div class="page-title">
            <a href="{{ route('settings.index') }}">
                <i class="icon-custom-left"></i>
            </a>
            <h3>
                Alterar <span class="semi-bold">Mídias Sociais</span>
            </h3>
        </div>

        <!-- CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-body no-border">

                        @foreach($socialMedia as $sm)

                        {!! Form::model($sm, [
                        'method' => 'PATCH',
                        'route' => ['socialMedia.update', $sm->id],
                        'files' => true
                        ]) !!}

                        @include('socialMedia.form', ['submitButtonText' => 'Salvar','var' => $sm])

                        {!! Form::close() !!}
                        @endforeach

                        {!! Form::open([
                             'route' => 'socialMedia.store',
                             'files' => true
                           ]) !!}


                        @include('socialMedia.form', ['submitButtonText' => 'Cadastrar'])


                        {!! Form::close() !!}

                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection