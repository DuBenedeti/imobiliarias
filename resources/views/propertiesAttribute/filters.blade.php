<div class="row">
  <div class="col-md-12">
    <div class="grid simple">

      <div class="grid-title no-border">
        <h4>Filtros</h4>
      </div>

      <div class="grid-body no-border">
        <form method="GET" class="filter-form">

          <div class="row">
            <div class="col-md-6">
              <div class="input-append default no-padding col-xs-12">
                <input type="text" id="name" name="name" class="form-control" placeholder="Nome" />
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <select class="form-control" name="type" id="type">
                  <option value="" disabled selected hidden>Tipo do imóvel</option>
                  @foreach ($tipoImovel as $type)
                  <option value="{{ $type->id }}">{{ $type->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12">
              @if ($_GET)
              <a href="{{ route('propertiesAttribute.index') }}" class="btn btn-mini btn-default">
                <i class="fa fa-times"></i> Limpar filtros
              </a>
              @endif

              {{-- submit filtering --}}
              <button type="submit" class="btn btn-primary btn-small btn-cons pull-right">Filtrar</button>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
