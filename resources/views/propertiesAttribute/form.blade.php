<style>
  #doc {
    display: none
  }

  .doc {
    background-color: #3498db;
    color: #fff !important;
    cursor: pointer;
    padding: 6px 50px;
  }

  #arq {
    font: 1.1em sans-serif;
  }

  .cores {
    border: none !important;
  }

  .fa-file {
    font-size: 15px
  }
</style>
<div class="grid-title no-border">

</div>

<div class="grid-body no-border">

  <div class="row">
    <div class="form-group col-md-4 {{ $errors->first('name')? 'has-errors' : '' }}">
      {!! Form::label('name', 'Nome', ['class' => 'form-label required']) !!}
      {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required' => 'required']) !!}
      <small class="title">{{ $errors->first('name') }}</small>
    </div>

    <div class="form-group col-md-4 {{ $errors->first('type_property_id')? 'has-error' : '' }}">
      {!! Form::label('type_property', 'Tipo de imóvel', ['class' => 'form-label required']) !!}
      <select name="type_property_id" class="form-control">
      <option value="null">Selecione</option>
        @foreach ($tipoImovel as $type)
        <option value="{{ $type->id }}">{{ $type->name }}</option>
        @endforeach
      </select> 
      <small class="error">{{ $errors->first('type_property_id') }}</small>
    </div>

    <div class="form-group col-md-4 {{ $errors->first('type_field')? 'has-error' : '' }}">
      {!! Form::label('type_field', 'Formato', ['class' => 'form-label required']) !!}
      <select name="type_field" class="form-control">
        <option value="text" selected>Texto</option>
        <option value="boolean">Checkbox</option>
        <option value="money">Dinheiro</option>
        <option value="quantity">Quantidade</option>
      </select> 
      <small class="error">{{ $errors->first('type_field') }}</small>
    </div>

  </div>

  <div class="form-actions m-b-5">
    <div class="pull-right">
      <button class="btn btn-success" type_id="submit">
        <i class="fa fa-check"></i>
        {{ $submitButtonText }}
      </button>
      <a class="btn btn-white" href="{{ route('menu.index') }}">Cancelar</a>
    </div>
  </div>
</div>