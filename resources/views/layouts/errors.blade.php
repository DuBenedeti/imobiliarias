<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title')</title>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link href="//fonts.googleapis.com/css?family=Bungee+Inline|Lato" rel="stylesheet">

    <style>
      html, body {
        height: 100%;
      }

      body {
        margin: 0;
        padding: 0;
        width: 100%;
        color: #616161;
        display: table;
        font-family: 'Lato', sans-serif;
        font-size: 18px;
      }

      .container {
        text-align: center;
        display: table-cell;
        vertical-align: middle;
      }

      .content {
        text-align: center;
        display: inline-block;
      }

      .code {
        font-family: 'Bungee Inline', cursive;
        font-size: 123px;
      }

      .title {
        font-size: 32px;
        margin-bottom: 40px;
      }

      .button {
        display: inline-block;
        background-color: #CFD8DC;
        padding: 5px 8px;
        border-radius: 3px;
        color: #455A64;
        text-decoration: none;
      }

      .button:hover {
        background-color: #B0BEC5;
        color: #37474F;
      }

      .blue {
        color: #00669b;
      }

      .gray {
        color: #6d6e71;
      }

      .green {
        color: #8dc63f;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="content">
        <div class="code">
          @yield('code')
        </div>

        <div class="title">@yield('title')</div>

        <p class="description">
          @yield('description')
        </p>

        <a href="@yield('button_url')" class="button">@yield('button_text')</a>
      </div>
    </div>
  </body>
</html>
