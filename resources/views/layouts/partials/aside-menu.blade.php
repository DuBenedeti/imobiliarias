<div class="aside-menu ">
    <div class="aside-menu--header ">
        {{$data->initials}}
    </div>
    <ul class="aside-menu--list">
        @foreach($menusSigla as $subMen)
        <li class="item-menu">
            <a href="{{ $subMen->url }}">
            {{$subMen->name}}
            </a>
        </li>
        @endforeach
        <li class="item-menu">
            <a href="{{ route('site.contact') }}">
                Contato
            </a>
        </li>
    </ul>
</div>