{{-- USER NAME --}}
<div class="user-details dvSuporte">
    <div class="username">
        <button class="btn btn-lg btn-dark" data-toggle="modal" data-target="#modalSuport"><i class="fa fa-life-ring" aria-hidden="true"></i> Suporte</button>

    </div>
</div>
<a href="#" class="dropdown-toggle" id="user-options" data-placement="bottom" data-toggle="dropdown">
    <div class="user-details">
        <div class="username">
            {{-- first name --}}
            {{ head(explode(" ", Auth::user()->name)) }}

            {{-- last name --}}
            @if (count(explode(" ", Auth::user()->name)) > 1)
                <span class="bold">
          {{ last(explode(" ", Auth::user()->name)) }}
        </span>
            @endif
        </div>
    </div>
    <div class="iconset top-down-arrow"></div>
</a>

{{-- NAVIGATION --}}
<ul class="dropdown-menu user-options" role="menu" aria-labelledby="user-options">
    {{--<li>--}}
        {{--<a href="{{ route('profile.edit') }}">Editar Perfil</a>--}}
    {{--</li>--}}
    {{--<li>--}}
        {{--<a href="{{ route('profile.password.edit') }}">Alterar Senha</a>--}}
    {{--</li>--}}

    <li>
        <div style="display: none;">{{ $id_user = last(explode(" ", Auth::user()->id)) }}</div>
        <a href="{{ route('users.edit', $id_user) }}">
            <i class="fa fa-edit"></i>
            Editar Informações
        </a>
    </li>

    <li>
        <a href="{{ route('users.alt_pass', $id_user) }}">
            <i class="fa fa-unlock-alt"></i>
             Alterar Senha
        </a>
    </li>

    <li class="divider"></li>

    <li>
        <a href="{{ url('/sistema/logout') }}">
            <i class="fa fa-power-off"></i>
            Logout
        </a>
    </li>
</ul>

<ul class="dropdown-menu user-options" role="menu" aria-labelledby="user-options">
    {{--<li>--}}
    {{--<a href="{{ route('profile.edit') }}">Editar Perfil</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="{{ route('profile.password.edit') }}">Alterar Senha</a>--}}
    {{--</li>--}}

    <li>
        <div style="display: none;">{{ $id_user = last(explode(" ", Auth::user()->id)) }}</div>
        <a href="{{ route('users.edit', $id_user) }}">
            <i class="fa fa-edit"></i>
            Editar Informações
        </a>
    </li>

    <li>
        <a href="{{ route('users.alt_pass', $id_user) }}">
            <i class="fa fa-unlock-alt"></i>
            Alterar Senha
        </a>
    </li>

    <li class="divider"></li>

    <li>
        <a href="{{ url('/sistema/logout') }}">
            <i class="fa fa-power-off"></i>
            Logout
        </a>
    </li>
</ul>





