<div class="page-sidebar" id="main-menu">
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">

        

        {{-- SIDEBAR MENU --}}
        <!-- <p class="menu-title">NAVEGAÇÃO</p> -->
        <ul>

            <li>
                <a href="{{ url('/sistema/') }}">
                    <i class="fa fa-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li class="divider"></li>
            @foreach ($nav as $module)
                {{-- module --}}

                @if((\Illuminate\Support\Facades\Auth::user()->hasModule($module->slug) > 0)
                or (\Illuminate\Support\Facades\Auth::user()->hasRole('Admin')))
                    
                    @if ((\Illuminate\Support\Facades\Auth::user()->hasRole('Admin')) && ($module->id == 1))

                    <li @if ((isset($currentModule)) and ($module->url == $currentModule->url)) class="active" @endif>
                        {{-- label --}}
                        <a href="{{ url('/sistema'.$module->url) }}">
                            <i class="fas fa-{{ $module->icon }}"></i>
                            <span class="title">{{ $module->name }}</span>

                            @if (count($module->submodules) > 0)
                                <span class="arrow"></span>
                            @endif
                        </a>

                        {{-- submodules --}}
                        @if (count($module->submodules) > 0)
                            <ul class="sub-menu">
                                @foreach ($module->submodules as $submodule)
                                        <li>
                                            <a href="{{ url($module->url.$submodule->url) }}">{{ $submodule->label }}</a>
                                        </li>
                                @endforeach
                            </ul>
                        @endif

                    </li>
                    @elseif ($module->id != 1)
                        <li @if ((isset($currentModule)) and ($module->url == $currentModule->url)) class="active" @endif>
                            {{-- label --}}
                            <a href="{{ url('/sistema'.$module->url) }}">
                                <i class="fas fa-{{ $module->icon }}"></i>
                                <span class="title">{{ $module->name }}</span>

                                @if (count($module->submodules) > 0)
                                    <span class="arrow"></span>
                                @endif
                            </a>

                            {{-- submodules --}}
                            @if (count($module->submodules) > 0)
                                <ul class="sub-menu">
                                    @foreach ($module->submodules as $submodule)
                                            <li>
                                                <a href="{{ url('/sistema'.$module->url.$submodule->url) }}">{{ $submodule->name }}</a>
                                            </li>
                                    @endforeach
                                </ul>
                            @endif

                        </li>
                    @endif

                @endif
            @endforeach
            <br>

            {{--<li class="divider"></li>--}}
            {{--<li>--}}
                {{--<a href="{{ url('/sistema/logout') }}">--}}
                    {{--<i class="fa fa-power-off"></i>--}}
                    {{--<span class="title">Logout</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            {{-- Configs for Mobile --}}
            @if ((!Auth::user()->hasRole('Usuário')) and (count($navConfig) > 0))
                {{--<li class="visible-xs divider"></li>--}}
                {{--<li class="visible-xs">--}}
                {{--<a href="#">--}}
                {{--<i class="fa fa-cog"></i>--}}
                {{--<span class="title">Configurações</span>--}}
                {{--</a>--}}
                {{--</li>--}}
            @endif
        </ul>
    </div>
</div>
