<!DOCTYPE html>
<html>

<head>
  <title>Painel de Controle</title>

  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

  <meta name="robots" content="noindex, nofollow">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="base-path" content="{{ url('/') }}">

  <link rel="shortcut icon" href="{{ asset('assets/images/favicon/prestige.ico') }}" type="image/x-icon">
  {{-- CSS TEMPLATE --}}
  <link href="{{ asset('assets/plugins/pace/pace-theme-flash.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
  <link href="{{ asset('assets/plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/bootstrapv3/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/animate.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/datepicker.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/boostrap-clockpicker/bootstrap-clockpicker.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/dropzone/css/dropzone.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('webarch/css/webarch.css') }}" rel="stylesheet" type="text/css" />
  {{-- CSS PERSONAL --}}
  <link href="{{ asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/cropper/cropper.min.css') }}" rel="stylesheet">


  <link rel="stylesheet" href="{{ asset('assets/plugins/dropzone/css/dropzone.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/plugins/ios-switch/ios7-switch.css')}}">
  <link href="{{ asset('assets/plugins/cropper/cropper.min.css') }}" rel="stylesheet">

  <link href="{{ asset('assets/css/app.custom.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <style>
    .cog i {
      font-size: 18px;
      color: #4c5264;
    }

    .fa {
      display: inline-block;
      font: normal normal normal 14px/1 FontAwesome;
      font-size: inherit;
      text-rendering: auto;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }

    .menu-esquerdo>ul>li>a {
      color: #8b91a0 !important;
      display: block !important;
      position: relative !important;
      margin: 0 !important;
      border: 0px !important;
      padding: 11.5px 31px 11.5px 31px !important;
      text-decoration: none !important;
      font-size: 14px !important;
      font-weight: normal !important;
      list-style: none !important;
    }

    #menu_esquerdo {
      color: #8b91a0 !important;
      display: block !important;
      position: relative !important;
      margin: 0 !important;
      border: 0px !important;
      padding: 11.5px 31px 11.5px 31px !important;
      text-decoration: none !important;
      font-size: 14px !important;
      font-weight: normal !important;
      list-style: none !important;
    }

    .ItemOferta {
      display: block;
      margin-bottom: 20px;
      line-height: 1.42857143;
      background-color: #fff;
      border: 1px solid #ddd;
      border-radius: 4px;
      -webkit-transition: border .2s ease-in-out;
      -o-transition: border .2s ease-in-out;
      transition: border .2s ease-in-out;
      text-align: center;
      margin-left: 5px;
      margin-right: 5px;

      height: 312px;
      max-width: 222px;
    }


    .lblTituloOferta {
      text-align: center;
      font-weight: bold;
      color: #333;
    }

    .delItem {
      color: #9f041b;

    }

    .delItem:hover {
      color: #1b1e25;
    }

    .dvSuporte {
      margin-right: 15px;
    }

    .modal-body {
      background: #FFFFFF;
    }

    .modal-footer {
      background: #FFFFFF;
    }

    .popover {
      background: #1b1e24;
      color: #FFFFFF;
    }

    .popover.right>.arrow:after {
      bottom: -10px;
      left: 1px;
      content: " ";
      border-right-color: #1b1e24;
      border-left-width: 0;
    }

    .popover-content {
      background: #1b1e24;
      color: #FFFFFF;
    }

    .radio_status {
      margin-top: 10px;
    }

    .btnAddProduct {
      margin-top: 7px;
    }
  </style>

  {{-- BEGIN PAGE CSS --}}
  @yield('css')
</head>

<body class="">
  {{-- HEADER --}}
  <div class="header navbar navbar-inverse">
    <div class="navbar-inner">
      <div class="header-seperation">

        {{-- MOBILE HEADER --}}
        <ul class="nav pull-left notifcation-center visible-xs visible-sm">
          <li class="dropdown">
            <a href="#main-menu" data-webarch="toggle-left-side">
              <div class="iconset top-menu-toggle-white"></div>
            </a>
          </li>
        </ul>

        {{-- LOGO --}}
        <a href="{{ url('/sistema') }}">
          <img src="{{ asset('assets/images/w-imob-white.png') }}" style="height: 75px;
                    transform: translate(10%);margin-top: -5px;" class="" alt="W-imob" />
        </a>

        {{-- NAV BUTTONS --}}
        <!-- <ul class="nav pull-right notifcation-center">
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="{{ url('/sistema') }}"  class="dropdown-toggle active" data-toggle="">
                        <div class="iconset top-home"></div>
                    </a>
                </li>
                <li class="dropdown visible-xs visible-sm">
                    <a href="#" data-webarch="toggle-right-side">
                        <div class="iconset top-chat-white "></div>
                    </a>
                </li>
            </ul> -->
      </div>

      {{-- CONTENT HEADER --}}
      <div class="header-quick-nav">
        <div class="pull-left">
          <ul class="nav quick-section">
            <li class="quicklinks">
              <a href="#" class="" id="layout-condensed-toggle">
                <div class="iconset top-menu-toggle-dark"></div>
              </a>
            </li>
          </ul>

          {{-- HEADER TITLE --}}
          <h4 class="quick-section">Painel de Controle | W-Imobi</h4>


        </div>

        {{-- HEADER RIGHT SIDE SECTION --}}
        <div class="pull-right">

          <div class="chat-toggler">

            {{-- SETTINGS CENTER --}}
            @include('layouts.partials.settings')

            {{-- PROFILE PICTURE --}}
            <div class="profile-pic">
              @if (Auth::user()->image)
              <img src="{{ asset('assets/images/profile/'.Auth::user()->image) }}" alt="" width="35" height="35" />
              @else
              <img src="{{ asset('assets/images/profile/default.png') }}" alt="" width="35" height="35">
              @endif
            </div>
            <div class="cog">
              @if (Auth::user()->hasRole('Admin'))
              <a href="{{ route('settings.index') }}">
                <i class="fa fa-cog"></i>
              </a>
              @endif
            </div>

          </div>
        </div>{{-- END HEADER RIGHT SIDE SECTION --}}
      </div>{{-- END CONTENT HEADER --}}
    </div>{{-- END TOP NAVIGATION BAR --}}
  </div>{{-- END HEADER --}}

  <div class="modal fade" id="modalSuport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><span class="semi-bold">SUPORTE</span> PRESTIGE</h5>

        </div>
        <div class="modal-body">
          <div class="col-md-12 m-t-15 m-b-15 p-t-10 p-b-10">
            <div class="row">
              <div class="col-md-6">
                <h5>
                  <i class="fa fa-phone"></i>&nbsp;
                  <a href="tel:04436228831">(44) 3622-8831</a>
                </h5>
              </div>
              <div class="col-md-6">
                <h5>
                  <i class="fa fa-phone"></i>&nbsp;
                  <a href="tel:04430561163">(44) 3056-1163</a>
                </h5>
              </div>

              <div class="col-md-6">
                <h5>
                  <i class="fa fa-envelope"></i>&nbsp;
                  <a href="mailto:suporte@prestige.com.br">
                    suporte@prestige.com.br
                  </a>
                </h5>

              </div>
              <div class="col-md-6">
                <h5>
                  <i class="fab fa-whatsapp"></i>&nbsp;
                  <a target="_blank" href="https://web.whatsapp.com/send?phone=5544998819952&text=Ol%C3%A1%2C%20estou%20navegando%20no%20painel%20da%20ACITO%20e%20gostaria%20de%20conversar%20sobre" title="whatsapp" class="light-blue-text">
                    (44) 99881-9952
                  </a>
                </h5>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <p class="bold">
              Horário de atendimento:
            </p>
            <p>
              De Segunda à Sexta:
              <br>
              08:00 às 12:00 - 14:00 às 18:00
            </p>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>
  <div class="page-container row-fluid">
    {{-- MENU --}}
    @include('layouts.partials.navigation')

    {{-- SCROLL UP --}}
    <a href="#" class="scrollup">Scroll</a>

    {{-- CONTENT --}}
    @yield('content')

  </div>


  {{-- JS TEMPLATE --}}
  <script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
  <script src="https://code.jquery.com/jquery-3.2.0.min.js" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/bootstrapv3/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/jquery-block-ui/jqueryblockui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/jquery-mask/jquery.mask.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/boostrap-clockpicker/bootstrap-clockpicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/dropzone.js') }}" type="text/javascript"></script>
  {{-- JS PERSONAL --}}
  <script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/knob/jquery.knob.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/noty/jquery.noty.packaged.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/cropper/cropper.min.js') }}" type="text/javascript"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1JhAcUiWd45YFiqJv1tQ_EhslrOzhfLM"></script>
  <script src="{{ asset('webarch/js/webarch.js') }}" type="text/javascript"></script>
  <script src="{{ asset('webarch/js/custom.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/plugins/ios-switch/ios7-switch.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/form_elements.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/js/plugins/cidades-estados.js') }}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>



  {{-- NOTIFICATIONS --}}
  @if (session()->has('success'))
  <script>
    noty({
      text: "{!! session()->get('success') !!}",
      type: 'success'
    });
  </script>
  @endif
  @if (session()->has('error'))
  <script>
    noty({
      text: "{!! session()->get('error') !!}",
      type: 'error'
    });
  </script>
  @endif

  {{-- INLINE JAVASCRIPTS --}}
  <script>
    $(document).ready(function() {
      $('select').select2();
    })
  </script>

  <script type="text/javascript">
    $('.fileinput').on('change.bs.fileinput', function(event) {
      $('.fileinput-preview img').cropper({
        aspectRatio: 4 / 3,
        zoomable: false,
        minCropBoxWidth: 500,
        minCropBoxHeight: 500,
        cropBoxResizable: true,
        crop: function(e) {
          $('#x').val(e.x);
          $('#y').val(e.y);
          $('#width').val(e.width);
          $('#height').val(e.height);
        }
      });
    });
  </script>

  <script type="text/javascript">
    function MascaraMoeda(objTextBox, e) {
      var SeparadorMilesimo = '.';
      var SeparadorDecimal = ',';

      var sep = 0;
      var key = '';
      var i = j = 0;
      var len = len2 = 0;
      var strCheck = '0123456789';
      var aux = aux2 = '';
      var whichCode = (window.Event) ? e.which : e.keyCode;
      if (whichCode == 13 || whichCode == 8) return true;
      key = String.fromCharCode(whichCode); // Valor para o código da Chave
      if (strCheck.indexOf(key) == -1) return false; // Chave inválida
      len = objTextBox.value.length;
      for (i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
      aux = '';
      for (; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i)) != -1) aux += objTextBox.value.charAt(i);
      aux += key;
      len = aux.length;
      if (len == 0) objTextBox.value = '';
      if (len == 1) objTextBox.value = '0' + SeparadorDecimal + '0' + aux;
      if (len == 2) objTextBox.value = '0' + SeparadorDecimal + aux;
      if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
          if (j == 3) {
            aux2 += SeparadorMilesimo;
            j = 0;
          }
          aux2 += aux.charAt(i);
          j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
          objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
      }
      return false;
    }
  </script>
  <script>
    function showCities(city=null, state=null) {
      $.getJSON("{{asset('assets/js/json/cities.json')}}", function(data) {
        
        var items = [];
        var options = '<option value="">escolha um estado</option>';
        
        $.each(data, function(key, val) {
          if(val.nome == state){
            options += '<option value="' + val.nome + '" selected>' + val.nome + '</option>';
          }else{
            options += '<option value="' + val.nome + '">' + val.nome + '</option>';
          }
        });
        $("#state").html(options);

        $("#state").change(function() {

          var options_cidades = '';
          var str = "";

          $("#state option:selected").each(function() {
            str += $(this).text();
          });

          $.each(data, function(key, val) {
            if (val.nome == str) {
              $.each(val.cidades, function(key_city, val_city) {
                if(val_city == city){
                  options_cidades += '<option value="' + val_city + '" selected>' + val_city + '</option>';
                }else{
                  options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                }
              });
            }
          });

          $("#city").html(options_cidades);

        }).change();

      });

    };
  </script>

  @yield('js')
</body>

</html>