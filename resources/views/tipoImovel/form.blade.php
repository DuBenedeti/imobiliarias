<style>
    #doc {
    display: none
}
.doc {
    background-color: #3498db;
    color: #fff !important;
    cursor: pointer;
    padding: 6px 50px;
}
#arq{
    font: 1.1em sans-serif;
}
.cores{
    border: none !important;
}
.fa-file{
    font-size: 15px
}

    </style>
<div class="grid-title no-border">

</div>

<div class="grid-body no-border">

    <div class="row">
        <div class="form-group col-md-4 {{ $errors->first('name')? 'has-errors' : '' }}">
            {!! Form::label('name', 'Nome', ['class' => 'form-label required']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required' => 'required']) !!}
            <small class="title">{{ $errors->first('name') }}</small>
        </div>

        <div class="form-group col-md-4 {{ $errors->first('slug')? 'has-errors' : '' }}">
            {!! Form::label('slug', 'Slug', ['class' => 'form-label required']) !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug', 'required' => 'required']) !!}
            <small class="title">{{ $errors->first('slug') }}</small>
        </div>

        <div class="form-group col-md-4 {{ $errors->first('plural_name')? 'has-errors' : '' }}">
            {!! Form::label('plural_name', 'Plural', ['class' => 'form-label required']) !!}
            {!! Form::text('plural_name', null, ['class' => 'form-control', 'placeholder' => 'Plural', 'required' => 'required']) !!}
            <small class="title">{{ $errors->first('plural_name') }}</small>
        </div>

        
    </div>

    <div class="form-actions m-b-5">
        <div class="pull-right">
            <button class="btn btn-success" type_id="submit">
                <i class="fa fa-check"></i>
                {{ $submitButtonText }}
            </button>
            <a class="btn btn-white" href="{{ route('menu.index') }}">Cancelar</a>
        </div>
    </div>
</div>