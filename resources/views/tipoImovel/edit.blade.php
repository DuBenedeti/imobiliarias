@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('tipoImovel.index') }}">Tipo de imóvel</a></li>
                <li><a class="active">Alterar: {{$tipoImovel->name}}</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('tipoImovel.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Alterar <span class="semi-bold">Tipo de imóvel</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">

                        <div class="grid-body no-border">


                            {!! Form::model($tipoImovel, [
                                                    'method' => 'PATCH',
                                                    'route' => ['tipoImovel.update', $tipoImovel->id],
                                                    'files' => true
                                                ]) !!}


                            @include('tipoImovel.form', ['submitButtonText' => 'Alterar',
                                                            'var' => $tipoImovel])


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

