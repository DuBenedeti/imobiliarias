<div class="grid-body no-border">
    <div class="row ">
        <br>
        <div class="form-group col-md-2 {{ $errors->first('phone')? 'has-errors' : '' }}">
            {!! Form::label('phone', 'Número', ['class' => 'form-label required']) !!}
            {!! Form::text('phone', null, ['class' => 'form-control phone', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('phone') }}</small>
        </div>
        <div class="form-group col-md-3 {{ $errors->first('description')? 'has-errors' : '' }}">
            {!! Form::label('description', 'Descrição', ['class' => 'form-label required']) !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('description') }}</small>
        </div>
        <div class="form-group col-md-2 " style="margin-top: 2%">

            @if($submitButtonText == 'Cadastrar')
            <button class="btn btn-success" type_id="submit">
                <i class="fa fa-check"></i>
            </button>
            @endif
            @if($submitButtonText != 'Cadastrar')
            <a class="btn btn-danger" href="{{ route('phones.destroy', $var->id) }}">
                <i class="fas fa-trash"> </i> </a>
            @endif
        </div>
    </div>
</div>


@section('js')
<script>
    var SPMaskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.phone').mask(SPMaskBehavior, spOptions);
</script>
@endsection