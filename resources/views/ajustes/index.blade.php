@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="content">

        <!-- BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <p>VOCÊ ESTÁ AQUI</p>
            </li>
            <li>Configuração do site</li>
            <li><a class="active">Alterar</a></li>
        </ul>

        <!-- TITLE-->
        <div class="page-title">
            <a href="{{ route('settings.index') }}">
                <i class="icon-custom-left"></i>
            </a>
            <h3>
                Alterar <span class="semi-bold">Configuração do site</span>
            </h3>
        </div>

        <!-- CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-body no-border">

                        {!! Form::model($ajustes, [
                        'method' => 'PATCH',
                        'route' => ['ajustes.update', $ajustes->id],
                        'files' => true
                        ]) !!}


                        @include('ajustes.form', ['submitButtonText' => 'Alterar',
                        'var' => $ajustes])


                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection