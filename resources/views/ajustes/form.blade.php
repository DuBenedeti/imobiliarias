<div class="grid-body no-border" style="padding: 13px !important;">
  <div class="row">
    <div class="form-group col-md-2">
      <a class="btn btn-success" href="{{ route('properties.delete-images') }}">Excluir imagem dos imóveis excluídos</a>
    </div>
  </div>
  <h4>Máximo de imóveis na Home</h4>

  <div class="row">

    <div class="form-group col-md-2 {{ $errors->first('maxSale')? 'has-errors' : '' }}">
      {!! Form::label('maxSale', 'Venda', ['class' => 'form-label required']) !!}
      {!! Form::number('maxSale', null, ['class' => 'form-control', 'required' => 'required', 'min' => '0']) !!}
      <small class="error">{{ $errors->first('maxSale') }}</small>
    </div>
    <div class="form-group col-md-2 {{ $errors->first('maxRent')? 'has-errors' : '' }}">
      {!! Form::label('maxRent', 'Locação', ['class' => 'form-label required']) !!}
      {!! Form::number('maxRent', null, ['class' => 'form-control', 'required' => 'required', 'min' => '0']) !!}
      <small class="error">{{ $errors->first('maxRent') }}</small>
    </div>
  </div>

  <div class="form-actions m-b-5">
    <div class="pull-right">
      <button class="btn btn-success" type_id="submit">
        <i class="fa fa-check"></i>
        {{ $submitButtonText }}
      </button>
      <a class="btn btn-white" href="{{ route('ajustes.index') }}">Cancelar</a>
    </div>
  </div>
</div>