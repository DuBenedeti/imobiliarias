@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="content">

        <!-- BREADCRUMB -->
        <ul class="breadcrumb">
            <li>
                <p>VOCÊ ESTÁ AQUI</p>
            </li>
            <li>Dados da Empresa</li>
            <li><a class="active">Alterar</a></li>
        </ul>

        <!-- TITLE-->
        <div class="page-title">
            <a href="{{ route('settings.index') }}">
                <i class="icon-custom-left"></i>
            </a>
            <h3>
                Alterar <span class="semi-bold">Dados da Empresa</span>
            </h3>
        </div>

        <!-- CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="grid simple">
                    <div class="grid-body no-border">


                        {!! Form::model($data, [
                        'method' => 'PATCH',
                        'route' => ['data.update', $data->id],
                        'files' => true
                        ]) !!}


                        @include('data.form', ['submitButtonText' => 'Alterar',
                        'var' => $data,  'states' => $states])


                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection