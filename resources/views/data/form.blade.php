<div class="grid-body no-border">

  <div class="row">
    <br>
    <div class="form-group col-md-3 {{ $errors->first('type')? 'has-errors' : '' }}">
      {!! Form::label('type', 'Tipo', ['class' => 'form-label required']) !!}
      <div style="display: flex;">
        <input class="form-check-input" type="radio" name="type" id="inlineRadio1" value="PF" {{ $data->type=="PF" ? "checked" : "" }}>
        <label class="form-check-label" for="inlineRadio1">Pessoa física</label>
        <input class="form-check-input" type="radio" name="type" id="inlineRadio2" value="PJ" style="margin-left: 15px;" {{ $data->type=="PJ" ? "checked" : "" }}>
        <label class="form-check-label" for="inlineRadio2">Pessoa jurídica</label>
      </div>
      <small class="error">{{ $errors->first('type') }}</small>
    </div>
    <div class="form-group col-md-2 {{ $errors->first('gtm_code')? 'has-errors' : '' }}">
      {!! Form::label('gtm_code', 'Tag Manager', ['class' => 'form-label']) !!}
      {!! Form::text('gtm_code', null, ['class' => 'form-control']) !!}
      <small class="error">{{ $errors->first('gtm_code') }}</small>
    </div>
  </div>

  <div class="row">

    <div class="form-group col-md-5 {{ $errors->first('name')? 'has-errors' : '' }}">
      {!! Form::label('name', 'Nome fantasia', ['class' => 'form-label required']) !!}
      {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('name') }}</small>
    </div>
    <div class="form-group col-md-5 {{ $errors->first('corporateName')? 'has-errors' : '' }}">
      {!! Form::label('corporateName', 'Razão social', ['class' => 'form-label required']) !!}
      {!! Form::text('corporateName', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('corporateName') }}</small>
    </div>
    <div class="form-group col-md-2 {{ $errors->first('creci')? 'has-errors' : '' }}">
      {!! Form::label('creci', 'CRECI', ['class' => 'form-label required']) !!}
      {!! Form::text('creci', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('creci') }}</small>
    </div>

  </div>

  <div class="row">
    <div class="form-group col-md-3 {{ $errors->first('cnpj')? 'has-errors' : '' }}">
      {!! Form::label('cnpj', 'CNPJ', ['class' => 'form-label required']) !!}
      {!! Form::text('cnpj', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('cnpj') }}</small>
    </div>

    <div class="form-group col-md-3 {{ $errors->first('cep')? 'has-errors' : '' }}">
      {!! Form::label('cep', 'CEP', ['class' => 'form-label required']) !!}
      {!! Form::text('cep', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('cep') }}</small>
    </div>
    <div class="form-group col-md-4 {{ $errors->first('address')? 'has-errors' : '' }}">
      {!! Form::label('address', 'Endereço', ['class' => 'form-label required']) !!}
      {!! Form::text('address', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'cep']) !!}
      <small class="error">{{ $errors->first('address') }}</small>
    </div>
    <div class="form-group col-md-2 {{ $errors->first('number')? 'has-errors' : '' }}">
      {!! Form::label('number', 'Número', ['class' => 'form-label required']) !!}
      {!! Form::text('number', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('number') }}</small>
    </div>
  </div>

  <div class="row">
    <div class="form-group col-md-2 {{ $errors->first('phone')? 'has-errors' : '' }}">
      {!! Form::label('phone', 'Whatsapp Principal', ['class' => 'form-label required']) !!}
      {!! Form::text('phone', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('phone') }}</small>
    </div>

    <div class="form-group col-md-2 {{ $errors->first('whats_rent')? 'has-errors' : '' }}">
      {!! Form::label('whats_rent', 'Whatsapp Locação', ['class' => 'form-label']) !!}
      {!! Form::text('whats_rent', null, ['class' => 'form-control']) !!}
      <small class="error">{{ $errors->first('whats_rent') }}</small>
    </div>

    <div class="form-group col-md-2 {{ $errors->first('whats_sale')? 'has-errors' : '' }}">
      {!! Form::label('whats_sale', 'Whatsapp Venda', ['class' => 'form-label']) !!}
      {!! Form::text('whats_sale', null, ['class' => 'form-control']) !!}
      <small class="error">{{ $errors->first('whats_sale') }}</small>
    </div>
  </div>

  <div class="row">
    {{-- state --}}
    <div class="col-md-3">
      <div class="form-group">
        {!! Form::label('state', 'UF', ['class' => 'form-label required']) !!}
        {!! Form::select('state', $states, $data->state_id ?? null, ['class' => 'form-control', 'data-default' => $data->state_id ?? ""]) !!}
      </div>
    </div>

    {{-- city --}}
    <div class="col-md-3">
      <div class="form-group {{ $errors->first('city_id')? 'has-error' : '' }}">
        {!! Form::label('city', 'Cidade', ['class' => 'form-label required']) !!}
        {!! Form::select('city', ['Selecione o UF'], null, ['class' => 'form-control', 'data-default' => $data->city_id ?? ""]) !!}
        <small class="error">{{ $errors->first('city_id') }}</small>
      </div>
    </div>
    <div class="form-group col-md-3 {{ $errors->first('district')? 'has-errors' : '' }}">
      {!! Form::label('district', 'Bairro', ['class' => 'form-label required']) !!}
      {!! Form::text('district', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('district') }}</small>
    </div>
    <div class="form-group col-md-3 {{ $errors->first('mail')? 'has-errors' : '' }}">
      {!! Form::label('mail', 'Email', ['class' => 'form-label required']) !!}
      {!! Form::text('mail', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="error">{{ $errors->first('mail') }}</small>
    </div>
  </div>

  <div class="row">
    <div class="form-group col-md-3">
      <label class="focustomrm-label required" for="logo_d">Logotipo Topo</label>
      <div class="clearfix"></div>
      <div class="fileinput_img fileinput-new" data-provides="fileinput">
        <div class="fileinput-new thumbnail" style="width:250px; height:auto;">
          @if (isset($data->logo_d))
          <img src="{{ asset('assets/images/data/'.$data->logo_d) }}" alt="avatar">
          @endif
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 250px; height: auto; line-height: 250px;"></div>
        <div>
          <span class="btn btn-default btn-file">
            <span class="fileinput-new">Selecionar imagem</span>
            <span class="fileinput-exists">Trocar</span>
            <input type="hidden"><input type="file" name="logo_d" id="logo_d" accept=".png, .jpg, .jpeg">
          </span>
          <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
        </div>
      </div>
      <small class="error">{{ $errors->first('logo_d') }}</small>
    </div>

    <div class="form-group col-md-3">
      <label class="focustomrm-label required" for="logo_footer">Logotipo Rodapé</label>
      <div class="clearfix"></div>
      <div class="fileinput_img fileinput-new" data-provides="fileinput">
        <div class="fileinput-new thumbnail" style="width:250px; height:auto;">
          @if (isset($data->logo_footer))
          <img src="{{ asset('assets/images/data/'.$data->logo_footer) }}" alt="avatar">
          @endif
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 250px; height: auto; line-height: 250px;"></div>
        <div>
          <span class="btn btn-default btn-file">
            <span class="fileinput-new">Selecionar imagem</span>
            <span class="fileinput-exists">Trocar</span>
            <input type="hidden"><input type="file" name="logo_footer" id="logo_footer" accept=".png, .jpg, .jpeg">
          </span>
          <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
        </div>
      </div>
      <small class="error">{{ $errors->first('logo_footer') }}</small>
    </div>
    
    <div class="form-group col-md-3">
      <label class="focustomrm-label required" for="favicon">Favicon</label>
      <div class="clearfix"></div>
      <div class="fileinput_img fileinput-new" data-provides="fileinput">
        <div class="fileinput-new thumbnail" style="width:250px; height:auto;">
          @if (isset($data->favicon))
          <img src="{{ asset('assets/images/data/'.$data->favicon) }}" alt="avatar">
          @endif
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 250px; height: auto; line-height: 250px;"></div>
        <div>
          <span class="btn btn-default btn-file">
            <span class="fileinput-new">Selecionar imagem</span>
            <span class="fileinput-exists">Trocar</span>
            <input type="hidden"><input type="file" name="favicon" id="favicon" accept=".png, .jpg, .jpeg">
          </span>
          <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
        </div>
      </div>
      <small class="error">{{ $errors->first('favicon') }}</small>
    </div>
    
  </div>

  <div class="form-actions m-b-5">
    <div class="pull-right">
      <button class="btn btn-success" type_id="submit">
        <i class="fa fa-check"></i>
        {{ $submitButtonText }}
      </button>
      <a class="btn btn-white" href="{{ route('data.index') }}">Cancelar</a>
    </div>
  </div>
</div>

@section('js')
<script src="{{ asset('assets/js/loadCities.js') }}"></script>
<script src="{{ asset('assets/js/loadDistricts.js') }}"></script>

<script>
  // window.onload = function() {
  //   showCities("{{$data->city}}", "{{$data->state}}");
  // };

  $('#cnpj').mask("99.999.999/9999-99");
  $('#phone').mask("(99) 99999-9999");
  $('#whats_rent').mask("(99) 99999-9999");
  $('#whats_sale').mask("(99) 99999-9999");
  $('#cep').mask("99999-000");
  
</script>
@endsection