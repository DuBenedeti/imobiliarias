@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('condominium.index') }}">Condomínios</a></li>
                <li><a class="active">Cadastrar</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('condominium.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Cadastrar novo <span class="semi-bold">condomínio</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">


                        {!! Form::open([
                            'route' => 'condominium.store',
                            'files' => true
                        ]) !!}


                        @include('condominium.form', ['submitButtonText' => 'Cadastrar', 'states' => $states, 'district' => $district])


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection