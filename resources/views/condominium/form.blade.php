<style>
  #doc {
    display: none
  }

  .doc {
    background-color: #3498db;
    color: #fff !important;
    cursor: pointer;
    padding: 6px 50px;
  }

  #arq {
    font: 1.1em sans-serif;
  }

  .cores {
    border: none !important;
  }

  .fa-file {
    font-size: 15px
  }
</style>
<div class="grid-title no-border">

</div>

<div class="grid-body no-border">

  <div class="row">
    <div class="form-group col-md-3 {{ $errors->first('name')? 'has-errors' : '' }}">
      {!! Form::label('name', 'Nome', ['class' => 'form-label required']) !!}
      {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required' => 'required']) !!}
      <small class="title">{{ $errors->first('name') }}</small>
    </div>

    <div class="col-md-2">
      {{-- state --}}
      <div class="form-group">
        {!! Form::label('state', 'UF', ['class' => 'form-label required']) !!}
        {!! Form::select('state', $states, $condominium->district->city->state->id ?? null, ['class' => 'form-control', 'data-default' => $condominium->district->city->state->id ?? ""]) !!}
      </div>
    </div>

    <div class="col-md-4">
      {{-- city --}}
      <div class="form-group {{ $errors->first('city_id')? 'has-error' : '' }}">
        {!! Form::label('city', 'Cidade', ['class' => 'form-label required']) !!}
        {!! Form::select('city', ['Selecione o UF'], $condominium->district->city_id ?? null, ['class' => 'form-control', 'data-default' => $condominium->district->city_id ?? ""]) !!}
        <small class="error">{{ $errors->first('city_id') }}</small>
      </div>
    </div>

    <div class="col-md-3">
      {{-- district --}}
      <div class="form-group {{ $errors->first('district_id')? 'has-error' : '' }}">
        {!! Form::label('district', 'Bairro', ['class' => 'form-label required']) !!}
        {!! Form::select('district', ['Selecione uma cidade'], $condominium->district_id ?? null, ['class' => 'form-control', 'data-default' => $condominium->district_id ?? ""]) !!}
        <small class="error">{{ $errors->first('district_id') }}</small>
      </div>
    </div>


  </div>

  <div class="form-actions m-b-5">
    <div class="pull-right">
      <button class="btn btn-success" type_id="submit">
        <i class="fa fa-check"></i>
        {{ $submitButtonText }}
      </button>
      <a class="btn btn-white" href="{{ route('district.index') }}">Cancelar</a>
    </div>
  </div>
</div>

@section('js')
<script src="{{ asset('assets/js/loadCities.js') }}"></script>
<script src="{{ asset('assets/js/loadDistricts.js') }}"></script>
<script>
  $(document).ready(function() {
    $('.cep').mask('00000-000');
  });
</script>
@endsection