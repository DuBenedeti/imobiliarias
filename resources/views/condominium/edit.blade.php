@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="content">

            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a href="{{ route('condominium.index') }}">Condomínios</a></li>
                <li><a class="active">Alterar: {{$condominium->name}}</a></li>
            </ul>

            <!-- TITLE-->
            <div class="page-title">
                <a href="{{ route('condominium.index') }}">
                    <i class="icon-custom-left"></i>
                </a>
                <h3>
                    Alterar <span class="semi-bold">condomínio</span>
                </h3>
            </div>

            <!-- CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">

                        <div class="grid-body no-border">


                            {!! Form::model($condominium, [
                                                    'method' => 'PATCH',
                                                    'route' => ['condominium.update', $condominium->id],
                                                    'files' => true
                                                ]) !!}


                            @include('condominium.form', ['submitButtonText' => 'Alterar',
                                                            'var' => $condominium])


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

