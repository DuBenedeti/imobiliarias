@extends('layouts.app')

@section('content')
<style>
    .text-description {
        padding: 10px;
        margin-bottom: 10px;
    }
</style>

<div class="page-content">
    <div class="content">
        <ul class="breadcrumb">
            <li>
                <p>VOCÊ ESTÁ AQUI</p>
            </li>
            <li><a class="active">Condomínios</a></li>
        </ul>
        <div class="page-title">
            <div class="row">
                <div class="col-md-6">
                    <a href="{{route('dashboard.index')}}">
                        <i class="icon-custom-left"></i>
                    </a>
                    <h3>Condomínios</h3>
                </div>
                <div class="col-md-6 p-t-15">
                    <div class="text-right text-center-xs">
                        <a href="{{ route('condominium.create') }}" class="btn btn-success btn-df-xs btn-small no-ls">
                            <span class="fa fa-plus"></span> Cadastrar
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid simple">
            <div class="grid-title no-border">
                <div class="pull-left">
                    <h4>
                        Lista de <span class="semi-bold">Condomínios</span>
                        @if (isset($trash)) excluídos @endif
                    </h4>
                </div>
                @if (!isset($trash))
                <div class="pull-left m-l-15">
                    <div class="selected-options inline-block" style="visibility:hidden">
                        <a href="#" class="btn btn-small btn-white delete" data-toggle="tooltip" data-original-title="Excluir selecionados">
                            <i class="fa fa-fw fa-trash"></i>
                            {{ csrf_field() }}
                        </a>
                    </div>
                </div>
                @endif
                <div class="clearfix"></div>
            </div>
            <div class="grid-body no-border">
                @if (count($condominium) == 0)
                <h5>Nenhum cadastro.</h5>
                @else
                <table class="table table-striped table-hover table-flip-scroll cf">
                    <thead class="cf">
                        <tr>
                            <th width="auto">Nome</th>
                            <th width="auto">Bairro</th>
                            <th width="auto">Cidade</th>
                            <th width="auto">Estado</th>
                            <th width="90">Opções</th>
                        </tr>
                    </thead>
                    <tbody id="sortable" class="ui-sortable">
                        @foreach ($condominium as $cond)
                        <tr id="item_{{ $cond->id }}">
                            <td>
                                {{$cond->name}}
                            </td>
                            <td>
                              {{$cond->district->name}}
                            </td>
                            <td>
                                {{$cond->district->city->name}}
                            </td>
                            <td>
                            {{$cond->district->city->state->name}}
                            </td>
                            <td>
                                <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-window-restore"></i> Ações
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{  route('condominium.edit', [$cond->id]) }}">
                                            <i class="fa fa-edit"></i> Editar
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{  route('condominium.destroy', [$cond->id]) }}">
                                            <i class="fa fa-trash"></i> Excluir
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pages">
                    <div class="pull-left results">
                        <strong>{{ $condominium->total()}}</strong> registro(s)
                    </div>

                    <div class="pull-right">
                        {!! $condominium->appends(Request::except('page'))->links() !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).on('change', '.js-switch', function() {
        var url = window.location.pathname + "/status";
        var id = $(this).attr('data-id');
        var status = $(this).prop('checked');

        if (status == true)
            status = 'Ativo';
        else status = 'Inativo';

        $.get(url, {
            id: id,
            status: status
        }, function(code) {
        console.log(status);
            if (code != 200)
                noty({
                    text: "Ocorreu um problema! Tente novamente mais tarde.",
                    type: 'error'
                });
            if (code == 200)
                location.replace(window.location);
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#sortable').sortable({
            handle: '.handle',
            update: function(event, ui) {
                var url = "/sistema/menu/order";
                var data = $(this).sortable('serialize');

                console.log(data);

                $.post(url, data, function(res) {
                    console.log(res);
                    if (res != 200) {
                        noty({
                            text: "Ocorreu um erro! Tente novamente.",
                            type: 'error'
                        });
                    }
                });
            }
        });
    });
</script>
@endsection