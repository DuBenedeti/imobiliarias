<div class="grid-body no-border">

    <div class="row">
        <div class="form-group col-md-12 {{ $errors->first('title')? 'has-errors' : '' }}">
            {!! Form::label('title', 'Título', ['class' => 'form-label required']) !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título do Banner', 'required' => 'required']) !!}
            <small class="error">{{ $errors->first('title') }}</small>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12 {{ $errors->first('link')? 'has-errors' : '' }}">
            {!! Form::label('link', 'Link ', ['class' => 'form-label']) !!} <span>( ex.: http://link.com.br )</span>
            {!! Form::text('link', null, ['class' => 'form-control', 'placeholder' => 'Link do Banner']) !!}
            <small class="error">{{ $errors->first('link') }}</small>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-12 {{ $errors->first('newTab')? 'has-errors' : '' }}">
            {!! Form::checkbox('newTab', null, ['class' => 'form-control']) !!}
            {!! Form::label('newTab', 'Abrir em nova aba', ['class' => 'form-label check-label']) !!}
            <small class="error">{{ $errors->first('newTab') }}</small>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('mobile', 'Tipo do banner', ['class' => 'form-label required']) !!}
                <br />

                <div class="radio radio-primary">
                    <input id="ckDesktop" onclick="tocaLegenda(0);" checked type="radio" name="mobile" value="Não">
                    <label for="ckDesktop">Banner Topo</label>

                    <input class="hide" id="ckMobile" onclick="tocaLegenda(0);" type="radio" name="mobile" value="Sim">
                    <label class="hide" for="ckMobile">Mobile</label>

                    <input id="ckPop" onclick="tocaLegenda(1);" type="radio" name="mobile" value="Pop">
                    <label for="ckPop">Popup</label>
                </div>

                <small class="error">{{ $errors->first('mobile') }}</small>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="focustomrm-label required" for="user_avatar">Imagem</label>
        <span class="help">Tamanho: 1920 x 500 pixels no Desktop</span>
        <div class="clearfix"></div>
        <div class="fileinput_img fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="width:128px; height:auto;">
                @if (isset($banner->image))
                <img src="{{ asset('assets/images/banner/'.$banner->image) }}" alt="avatar">
                @endif
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="width: 128px; height: auto; line-height: 128px;"></div>
            <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">Selecionar imagem</span>
                    <span class="fileinput-exists">Trocar</span>
                    <input type="hidden"><input type="file" name="imagem" id="image" accept=".jpg, .jpeg">
                </span>
                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
            </div>
        </div>
        <small class="error">{{ $errors->first('image') }}</small>
    </div>
    {{-- coordinates --}}
    <input type="hidden" name="width" id="width" value="">
    <input type="hidden" name="height" id="height" value="">
    <input type="hidden" name="x" id="x" value="">
    <input type="hidden" name="y" id="y" value="">

    <div class="form-actions m-b-5">
        <div class="pull-right">
            <button class="btn btn-success" type_id="submit">
                <i class="fa fa-check"></i>
                {{ $submitButtonText }}
            </button>
            <a class="btn btn-white" href="{{ route('banners.index') }}">Cancelar</a>
        </div>
    </div>
</div>

<script>
    function tocaLegenda(type) {
        if (type != 0) {
            $('.help').html('Tamanho: 1000 x 700 pixels para Popup.');
        } else {
            $('.help').html('Tamanho: 1920 x 500 pixels no Desktop ');
        }
    }
</script>