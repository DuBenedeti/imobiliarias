@extends('layouts.app')

@section('content')
    <style>
        .text-description{
            padding: 10px;
            margin-bottom: 10px;
        }
    </style>

    <div class="page-content">
        <div class="content">
            <!-- BREADCRUMB -->
            <ul class="breadcrumb">
                <li><p>VOCÊ ESTÁ AQUI</p></li>
                <li><a class="active">Banners</a></li>
            </ul>

            <!-- TITLE -->
            <div class="page-title">
                <div class="row">
                    <div class="col-md-6">
                        <a href="./">
                            <i class="icon-custom-left"></i>
                        </a>
                        <h3>Banners</h3>
                    </div>

                    <div class="col-md-6 p-t-15">
                        <div class="text-right text-center-xs">

                            <a href="{{ route('banners.create') }}"
                                class="btn btn-success btn-df-xs btn-small no-ls">
                                <span class="fa fa-plus"></span> Cadastrar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <!-- LISTING camp -->
            <div class="grid simple">
                <div class="grid-title no-border">
                    <div class="pull-left">
                        <h4>
                            Lista de <span class="semi-bold">Banners</span>
                            @if (isset($trash)) excluídos @endif
                        </h4>
                    </div>
                    @if (!isset($trash))
                        <div class="pull-left m-l-15">
                            <div class="selected-options inline-block" style="visibility:hidden">
                                <a href="#" class="btn btn-small btn-white delete" data-toggle="tooltip"
                                    data-original-title="Excluir selecionados">
                                    <i class="fa fa-fw fa-trash"></i>
                                    {{ csrf_field() }}
                                </a>
                            </div>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                </div>

                <div class="grid-body no-border">
                    <!-- if there is no results -->
                    @if (count($banners) == 0)
                        <h5>Nenhum Banner encontrado.</h5>
                    @else
                    <!-- the table -->
                        <table class="table table-striped table-hover table-flip-scroll cf">
                            <thead class="cf">
                            <tr>
                                <th width="15"></th>
                                <th width="185">Data</th>
                                <th width="90">Imagem</th>
                                <th width="420">Título</th>
                                <th width="120">Tipo</th>
                                <th>Status</th>
                                <th width="96">Opções</th>
                            </tr>
                            </thead>
                            <tbody id="sortable" class="ui-sortable">
                            @foreach ($banners as $banner)
                                <tr id="item_{{ $banner->id }}">
                                    <td class="handle ui-sortable-handle">
                                        <i class="fas fa-arrows-alt" 
                                            data-toggle="tooltip" data-original-title=""></i>
                                    </td>

                                    <td>
                                        <i class="fa fa-calendar"></i> {{ date('d/m/Y', strtotime($banner->created_at)) }}
                                        <br>
                                        <i class="fa fa-clock-o"></i> {{ date('H:i:s', strtotime($banner->created_at)) }}
                                    </td>
                                    <td>
                                        @if($banner->image != NULL)
                                            <div class="user-profile">
                                                <img src="{{ asset('assets/images/banner/thumb/'.$banner->thumb) }}"
                                                    name="{{ $banner->image }}"
                                                    title="{{ $banner->image }}"
                                                    data-src="{{ asset('assets/images/banner/thumb/'.$banner->thumb) }}"
                                                    data-src-retina="{{ asset('assets/images/banner/thumb/'.$banner->thumb ) }}"
                                                    style="display: inline-block; max-width: 48px; width: 48px; height: 48px;
                                                min-height: 48px; object-position: center; object-fit: cover;"/>
                                            </div>
                                        @else
                                            <div class="user-profile">
                                                <img src="{{ asset('assets/images/banner/video.png') }}"
                                                    name="Vídeo"
                                                    title="Vídeo"
                                                    data-src="{{ asset('assets/images/banner/video.png') }}"
                                                    data-src-retina="{{ asset('assets/images/banner/video.png') }}"
                                                    style="display: inline-block; max-width: 48px; width: 48px; height: auto;
                                                min-height: 48px; object-position: center; object-fit: cover;"/>
                                            </div>
                                        @endif
                                    </td>
                                    <td>{{ $banner->title }}</td>
                                    <td>
                                        @if($banner->mobile == 'Sim')
                                            <i class="fa fa-mobile fa-4" style="font-size: 40px" aria-hidden="true"></i><span class='text-description'>Mobile</span>
                                        @elseif($banner->mobile == 'Não')
                                            <i class="fa fa-desktop fa-4" style="font-size: 22px" aria-hidden="true"></i><span class='text-description'>Banner</span>
                                        @else
                                            <i class="far fa-window-maximize fa-4" style="font-size: 20px" aria-hidden="true"></i><span class='text-description'>Popup</span>
                                        @endif
                                    </td>
                                    <td>
                                    <input type="checkbox" data-id="{{ $banner->id }}" data-type="{{$banner->mobile}}" class="js-switch"
                                                value="@if ($banner->status == 'Ativo') Ativo @else Inativo @endif"
                                                @if ($banner->status == 'Ativo') checked @endif data-video="{{ $banner->video }}"/>
                                    </td>
                                    <td>
                                        <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-window-restore"></i> Ações
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{  route('banners.edit', [$banner->id]) }}">
                                                    <i class="fa fa-edit"></i> Editar
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{  route('banners.destroy', [$banner->id]) }}">
                                                    <i class="fa fa-trash"></i> Excluir
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- paginator -->
                        {{--@include('helpers.paginator', ['var' => $hotels])--}}
                    @endif
                </div> <!-- /.grid-body -->
            </div> <!-- /.grid -->
            <!-- LISTING END -->
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).on('change', '.js-switch', function () {
            var url = window.location.pathname + "/status";
            var id = $(this).attr('data-id');
            var status = $(this).prop('checked');
            var video = $(this).attr('data-video');
            var tipo = $(this).attr('data-type');

            if (status == true) status = 'Ativo';
            else status = 'Inativo';
            $.get(url, {id: id, status: status, video: video, tipo: tipo}, function (code) {
                if (code != 200)
                    noty({
                        text: "Ocorreu um problema! Tente novamente mais tarde.",
                        type: 'error'
                    });
                if (code == 200)
                    location.replace(window.location);
            });
        });
    </script>

    <script>
    $(document).ready(function() {
        $('#sortable').sortable({
        handle: '.handle',
        update: function(event, ui) {
            var url = "/sistema/banners/order";
            var data = $(this).sortable('serialize');

            console.log(data);

            $.post(url, data, function(res) {
            console.log(res);
            if (res != 200) {
                noty({text: "Ocorreu um erro! Tente novamente.", type: 'error'});
            }
            });
        }
        });
    });
    </script>

@endsection