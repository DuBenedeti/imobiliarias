@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="content">

        <div class="col-md-3 col-sm-6 m-b-10">
            <div class="tiles white">
                <div class="tiles-body">
                    <div class="help" style="color:#0c8d51"><i class="fas fa-home"></i> Imóveis ativos </div>
                    <div class="row-fluid">
                        <div class="heading">
                            <div class="tiles-title">Total</div>
                            <span class="animate-number" data-value="{{$properties}}" data-animation-duration="500">
                                0
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 m-b-10">
            <div class="tiles white">
                <div class="tiles-body">
                    <div class="help" style="color:#0c8d51"><i class="fas fa-image"></i> Banners ativos </div>
                    <div class="row-fluid">
                        <div class="heading">
                            <div class="tiles-title">Total</div>
                            <span class="animate-number" data-value="{{$banners}}" data-animation-duration="500">
                                0
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 m-b-10">
            <div class="tiles white">
                <div class="tiles-body">
                    <div class="help" style="color:#0c8d51"><i class="fas fa-user"></i> Usuários ativos </div>
                    <div class="row-fluid">
                        <div class="heading">
                            <div class="tiles-title">Total</div>
                            <span class="animate-number" data-value="{{$users}}" data-animation-duration="500">
                                0
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 m-b-10">
            <div class="tiles white">
                <div class="tiles-body">
                    <div class="help" style="color:#0c8d51"><i class="fas fa-envelope"></i> Contatos recebidos </div>
                    <div class="row-fluid">
                        <div class="heading">
                            <div class="tiles-title">Total</div>
                            <span class="animate-number" data-value="{{$contacts}}" data-animation-duration="500">
                                0
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 m-b-12">
            <div class="tiles white grid simple">
                <div class="grid-body no-border">
                    <!-- if there is no results -->
                    @if (count($propertyContact) == 0)
                    <h5>Nenhuma Mensagem encontrada.</h5>
                    @else

                    <!-- the table -->
                    <table class="table table-striped table-hover table-flip-scroll cf">
                        <thead class="cf">
                            <tr>

                                <th>Data</th>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th width="96">Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($propertyContact as $msg)
                            <tr>
                                <td>
                                    <i class="fa fa-calendar"></i> {{ date('d/m/Y', strtotime($msg->created_at)) }}
                                    <br>
                                    <i class="fa fa-clock-o"></i> {{ date('H:i:s', strtotime($msg->created_at)) }}
                                </td>
                                <td>{{ $msg->name }}</td>
                                <td>{{ $msg->email }}</td>

                                <td>
                                    <button class="btn btn-small dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-window-restore"></i> Ações
                                        <span class="caret"></span></button>

                                    <ul class="dropdown-menu">

                                        <li>
                                            <a href="{{  route('propertyContact.edit', [$msg->id]) }}">
                                                <i class="fa fa-eye"></i> Visualizar
                                            </a>
                                        </li>



                                    </ul>

                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <!-- paginator -->
                    {{--@include('helpers.paginator', ['var' => $hotels])--}}

                    @endif

                </div>
            </div>
        </div>

    </div>
</div>
@endsection