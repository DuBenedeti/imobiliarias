<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Contact extends Model
{

    protected $table = 'contact';
    use LogsActivity;
    protected $fillable = [
        'name',
        'email',
        'phone',
        'subject',
        'message'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Mensagem de Contato";
    }

    public function scopeKeywords($query, $keywords = null)
    {
        if (!is_null($keywords)) {
            $keywords = str_replace(' ', '%', $keywords);

            return $query->where('name', 'like', "%$keywords%")
                ->orWhere('subject', 'like', "%$keywords%");
        }
    }
}
