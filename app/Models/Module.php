<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function submodules()
    {
        return $this->hasMany('App\Models\SubModule')
            ->where('status', '=', 'Ativo')
            ->orderBy('position', 'asc');
    }
}
