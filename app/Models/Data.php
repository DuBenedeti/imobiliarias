<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Data extends Model
{
    protected $table = 'data';

    use LogsActivity;
    protected $fillable = [
        'type',
        'name',
        'corporateName',
        'cnpj',
        'phone',
        'whats_rent',
        'whats_sale',
        'address',
        'city_id',
        'state_id',
        'mail',
        'logo_footer',
        'favicon',
        'logo_d',
        'creci',
        'workwithus',
        'gtm_code',
        'cep',
        'number',
        'district'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Dados da Empresa";
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }
}
