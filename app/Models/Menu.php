<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

use Carbon\Carbon;
use function foo\func;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Menu extends Model
{
    protected $table = 'menu';

    use LogsActivity;
    protected $fillable = [
        'name',
        'url',
        'position',
        'subMenu',
        'slug',
        'status'
    ];
    public function getLogNameToUse(string $eventName = ''): string
    {
        return "menu";
    }
}
