<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class State extends Model
{
    use LogsActivity;

    protected $table = 'state';

    protected $fillable = [
        'name',
        'initials'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "State";
    }

    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }
}