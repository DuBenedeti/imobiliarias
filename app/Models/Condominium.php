<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Condominium extends Model
{
    use LogsActivity;

    protected $table = 'condominium';

    protected $fillable = [
        'name',
        'district_id'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "City";
    }
    
    public function district()
    {
        return $this->belongsTo('App\Models\District');
    }

}