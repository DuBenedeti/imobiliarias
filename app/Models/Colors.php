<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

use Carbon\Carbon;
use function foo\func;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Colors extends Model
{
    protected $table = 'colors';

    use LogsActivity;
    protected $fillable = [
        'botoes',
        'icones',
        'chapeu',
        'links',
        'barraTopo',
        'textoTopo',
        'textoTopoHover',
        'topIconesMidiasSociais',
        'topIconesMidiasSociaisHover',
        'iconesLinks',
        'barraRodape1',
        'barraRodape2',
        'textoRodape',
        'textoRodapeHover',
        'footerIconesMidiasSociais',
        'footerIconesMidiasSociaisHover',
        'updated_at',
        'barraFiltro',
        'iconMenu',
        'fundoMenu',
        'textoMenu',
        'hoverMenu',
        'lineMenu',
        'hoverBotaoConteudo',
        'textoBotaoConteudo',
        'hoverTextoBotaoConteudo'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Cores";
    }
}
