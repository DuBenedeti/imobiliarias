<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

use Carbon\Carbon;
use function foo\func;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class MenuSub extends Model
{
    protected $table = 'sub_menu';

    use LogsActivity;
    protected $fillable = [
        'name',
        'url',
        'position',
        'slug',
        'menu_id',
        'status'
    ];
    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Submenu";
    }
}
