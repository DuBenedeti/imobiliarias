<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

use Carbon\Carbon;
use function foo\func;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class LinksTopo extends Model
{
    protected $table = 'links_topo';

    use LogsActivity;
    protected $fillable = [
        'icon',
        'name',
        'link',
        'created_at',
        'updated_at'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Links do Topo";
    }
}
