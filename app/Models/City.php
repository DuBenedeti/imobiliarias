<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class City extends Model
{
    use LogsActivity;

    protected $table = 'city';

    protected $fillable = [
        'name',
        'state_id'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "City";
    }
    
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }
  
    public function districts()
    {
        return $this->hasMany('App\Models\District');
    }
}