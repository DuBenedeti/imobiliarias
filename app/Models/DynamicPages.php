<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DynamicPages extends Model
{

    protected $table = 'dynamic_pages';
    use LogsActivity;
    protected $fillable = [
        'title',
        'slug',
        'status',
        'text', 
        'description'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Páginas dinâmicas";
    }

}
