<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TipoImovel extends Model
{
    use LogsActivity;
    protected $table = 'type_property';
    protected $fillable = [
        'name',
        'plural_name',
        'slug'

    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Tipo de imóvel";
    }

    public function properties()
    {
        return $this->hasMany('App\Models\Property', 'type_property_id')->status(1);
    }
    public function propertiesWithTrashed()
    {
        return $this->hasMany('App\Models\Property', 'type_property_id')->withTrashed();
    }
}

