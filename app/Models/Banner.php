<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Banner extends Model
{
    use LogsActivity;
    protected $fillable = [
        'title',
        'link',
        'image',
        'thumb',
        'caption',
        'page',
        'mobile',
        'status',
        'range'

    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Banner";
    }
}

