<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Phones extends Model
{
    protected $table = 'phones';

    use LogsActivity;
    protected $fillable = [
        'icon',
        'phone',
        'is_whats',
        'description'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Telefones";
    }
}
