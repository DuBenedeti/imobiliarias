<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Modules extends Model
{
    protected $table = 'modules';

    protected $fillable = [
        'name',
        'url',
        'position',
        'icon',
        'slug',
        'status'
    ];
    public function getLogNameToUse(string $eventName = ''): string
    {
        return "modules";
    }
}
