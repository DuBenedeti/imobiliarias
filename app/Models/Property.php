<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Property extends Model
{
    use LogsActivity;
    use SoftDeletes;
    protected $table = 'properties';
    protected $fillable = [
        'external_id',
        'place',
        'number',
        'district_id',
        'zipcode',
        'price',
        'meters',
        'total_area',
        'slug',
        'video',
        'details',
        'image',
        'type_business_id',
        'type_property_id',
        'condominium_id',
        'status'

    ];
    
    public function getThumbPath()
    {
        return "assets/images/property/thumb/$this->image";
    }

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Imóvel";
    }

    public function scopeTypeBusiness($query, $typeBusiness = null)
    {
        if (!is_null($typeBusiness)) {
            return $query->where('type_business_id', $typeBusiness);
        }
    }

    public function scopeTypeProperty($query, $typeProperty = null)
    {
        if (!is_null($typeProperty) and $typeProperty != 0) {
            return $query->where('type_property_id', $typeProperty);
        }
    }

    public function scopeDistrict($query, $district = null)
    {
        if (!is_null($district)) {
            return $query->where('district_id', $district);
        }
    }

    public function scopeExternalId($query, $external_id = null)
    {
        if (!is_null($external_id)) {
            $query->where("external_id",'LIKE', "%$external_id%");
        }
    }

    public function scopeStatus($query, $status = null)
    {
        if (!is_null($status)) {
            if ($status == 0) {
                $query->where('status',  0);
            } else {
                $query->where('status',  1);
            }
        }
    }

    public function scopeOrder($query, $order = null)
    {
        if (!is_null($order)) {
            switch ($order) {
                case 1:
                    $query->orderBy('created_at', 'ASC');
                    break;
                    
                case 2:
                    $query->orderBy('created_at', 'DESC');
                    break;
                    
                case 3:
                    $query->orderBy('price', 'ASC');
                    break;
                    
                case 4:
                    $query->orderBy('price', 'DESC');
                    break;
                    
                default:
                    $query->orderBy('created_at', 'DESC');
                    break;
            }
        }
    }

    public function scopePrice($query, $priceMin = null, $priceMax = null)
    {
        if ($priceMax == 0) {
            $priceMax = null;
        }
        if ($priceMin == 0) {
            $priceMin = null;
        }
        
        if ((!is_null($priceMin)) && (!is_null($priceMax))) {
            $query->where('price', '>', getFloatFromInput($priceMin));
            $query->where('price', '<', getFloatFromInput($priceMax));
        } else if (!is_null($priceMin)) {
            $query->where('price', '>', getFloatFromInput($priceMin));
        } else if (!is_null($priceMax)) {
            $query->where('price', '<', getFloatFromInput($priceMax));
        }
    }

    public function typeProperty()
    {
        return $this->belongsTo('App\Models\TipoImovel');
    }

    public function condominium()
    {
        return $this->belongsTo('App\Models\Condominium');
    }

    public function typeBusiness()
    {
        return $this->belongsTo('App\Models\TypeBusiness');
    }

    public function district()
    {
        return $this->belongsTo('App\Models\District');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\PropertyPhotos', 'property_id')->orderBy('order', 'ASC');
    }

    public function attributes()
    {
        return $this->hasMany('App\Models\AttributesProperty', 'property_id');
    }

    public function getTitle()
    {
        $typePropertySlug = $this->typeProperty->name;
        if($this->meters != 0 and !empty($this->meters)){
            $area = $this->meters;
        }else{
            $area = $this->total_area;
        }
        $city = $this->district->city->name;
        $state = $this->district->city->state->initials;
        $id = $this->id;

        return $typePropertySlug . ' ' . $area . 'm ' . $city . '-' . $state . ' ' . $id;
    }

    public function getFrontTitle()
    {
        $typePropertySlug = $this->typeProperty->name;
        if($this->meters != 0 and !empty($this->meters)){
            $area = $this->meters;
        }else{
            $area = $this->total_area;
        }
        $district = $this->district->name;
        $id = $this->id;
        $business = $this->typeBusiness->name;
        if ($business === "Compra") {
            return $typePropertySlug . ' à venda - ' . $area . 'm² - ' . $district;
        } else if ($business === "Temporada") {
            return $typePropertySlug . ' para temporada - ' . $area . 'm² - ' . $district;
        } else {
            return $typePropertySlug . ' para alugar - ' . $area . 'm² - ' . $district;
        }
    }

    public function getFrontAddress()
    {
        $address = $this->place;
        $number = $this->number ?? 'S/N';
        $district = $this->district->name;
        if($this->condominium != null){
            $district .= ' - Condomínio ' . $this->condominium->name;
        }
        $city = $this->district->city->name;
        $state = $this->district->city->state->initials;
        $id = $this->id;
        return $address.' - nº '. $number.' - '. $district.' - '. $city.' - '. $state;
    }

    public function getCoverImage()
    {
        $image = $this->image;

        if (!empty($image)) {
            return asset('/assets/images/property/thumb/' . $image);
        }

        return null;
    }
}
