<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AttributesProperty extends Model
{
    use LogsActivity;
    protected $table = 'properties_has_attributes';
    protected $fillable = [
        'value',
        'attribute_id',
        'property_id'

    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Atributo pertencente ao imóvel";
    }

    public function attribute()
    {
        return $this->belongsTo('App\Models\PropertyAttribute');
    }

    public function property()
    {
        return $this->belongsTo('App\Models\Property');
    }

}

