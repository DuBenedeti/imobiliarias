<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PropertyContact extends Model
{
    use LogsActivity;
    protected $table = 'property_contact';
    protected $fillable = [
        'name',
        'message',
        'email',
        'phone',
        'property_id',
        'dateVisit'

    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Contato de imóvel";
    }

    public function properties()
    {
        return $this->hasMany('App\Models\Property', 'type_business_id')->status(1);
    }
}

