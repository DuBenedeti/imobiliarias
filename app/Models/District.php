<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class District extends Model
{
    use LogsActivity;

    protected $table = 'district';

    protected $fillable = [
        'name',
        'cep',
        'city_id'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "City";
    }
    
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function condominiums()
    {
        return $this->hasMany('App\Models\Condominium');
    }

}