<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PropertyPhotos extends Model
{
    use LogsActivity;
    protected $table = 'property_photos';
    protected $fillable = [
        'property_id',
        'image',
        'thumb',
        'subtitle',
        'credits',
        'featured',
        'order'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Fotos dos Imóveis";
    }

    public function getImagePath()
    {
        return "assets/images/property/$this->image";
    }
    
    public function getThumbPath()
    {
        return "assets/images/property/thumb/$this->thumb";
    }
}
