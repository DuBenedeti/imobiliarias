<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TrabalheConosco extends Model
{

    protected $table = 'work_with_us';
    use LogsActivity;
    protected $fillable = [
        'name',
        'email',
        'phone',
        'file',
        'file_type'
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Mensagem de Trablahe Conosco";
    }

    public function scopeKeywords($query, $keywords = null)
    {
        if (!is_null($keywords)) {
            $keywords = str_replace(' ', '%', $keywords);

            return $query->where('name', 'like', "%$keywords%");
        }
    }
}
