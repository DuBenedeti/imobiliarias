<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TypeBusiness extends Model
{
    use LogsActivity;
    protected $table = 'type_business';
    protected $fillable = [
        'name'

    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Tipo de negócio";
    }

    public function properties()
    {
        return $this->hasMany('App\Models\Property', 'type_business_id')->status(1);
    }
}

