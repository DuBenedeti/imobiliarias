<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

use Carbon\Carbon;
use function foo\func;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class ModulesSub extends Model
{
    
    protected $table = 'sub_modules';

    use LogsActivity;
    protected $fillable = [
        'name',
        'slug',
        'url',
        'position',
        'module_id',
        'status'
    ];
    public function getLogNameToUse(string $eventName = ''): string
    {
        return "modulesSub";
    }
}
