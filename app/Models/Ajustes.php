<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Ajustes extends Model
{
    protected $table = 'ajustes';

    use LogsActivity;
    protected $fillable = [
        'maxRent',
        'maxSale'
    ];

}
