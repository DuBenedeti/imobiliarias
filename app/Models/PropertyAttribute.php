<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PropertyAttribute extends Model
{
    use LogsActivity;
    protected $table = 'properties_attributes';
    protected $fillable = [
        'name',
        'type_field',
        'type_property_id'

    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Atributo do imóvel";
    }

    public function typeProperty()
    {
        return $this->belongsTo('App\Models\TipoImovel');
    }

    public function scopeName(\Illuminate\Database\Eloquent\Builder $query, ?string $value = null): void
    {
        if (isset($value))
            $query->where('name', 'ilike', "%{$value}%");
    }

    public function scopeType(\Illuminate\Database\Eloquent\Builder $query, ?int $value = null): void
    {
        if (isset($value))
            $query->where('type_property_id', '=', $value);
    }
}

