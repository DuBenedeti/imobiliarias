<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    

    protected $fillable = [
        'name', 'email', 'password', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function hasModule($slug)
    {
        $permission = Permission::select('id')->where('name','like',"%$slug%")->pluck('id');

        return DB::table('model_has_permissions')->where('model_id', Auth::user()->id)
            ->whereIn('permission_id', $permission)->count();
    }

    // ESCOPES ====================================================

    public function scopeAllExceptFirst($query)
    {
        return $query->where('id', '<>', 1);
    }

    public function scopeName($query, $name = null)
    {
        if (!is_null($name)) {
            $name = str_replace(' ', '%', $name);

            return $query->where('name', 'like', "%$name%");
        }
    }

    public function scopeEmail($query, $email = null)
    {
        if (!is_null($email)) {
            $email = str_replace(' ', '%', $email);

            return $query->where('email', 'like', "%$email%");
        }
    }

    public function scopeStatus($query, $status = null)
    {
        if (!is_null($status)) {
            return $query->where('status', $status);
        }
    }


    public function getLogNameToUse(string $eventName = ''): string
    {
        return "Usuários";
    }
}
