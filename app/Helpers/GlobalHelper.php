<?php



if (!function_exists('floatFormat')) {
  /**
   * @param mixed $float
   * @param string $location
   *
   * Availables locations:
   *  'default'
   *
   */
  function floatFormat($float, string $location = '')
  {
    $location = trim(strtolower($location));
    $formatedNumber = 0;

    if($float == 0 ){
      return "Sob consulta";
    }

    switch ($location) {
      case 'default':
        $formatedNumber = floatval($float);
        $formatedNumber = number_format($formatedNumber, 2, '.', ',');
        break;

      default:
        $formatedNumber = floatval($float);
        $formatedNumber = number_format($formatedNumber, 2, ',', '.');
        break;
    }

    return 'R$'.$formatedNumber;
  }
}

if (!function_exists('floatFormatInput')) {
  
  function floatFormatInput($float, string $location = '')
  {
    $location = trim(strtolower($location));
    $formatedNumber = 0;

    switch ($location) {
      case 'default':
        $formatedNumber = floatval($float);
        $formatedNumber = number_format($formatedNumber, 2, '.', ',');
        break;

      default:
        $formatedNumber = floatval($float);
        $formatedNumber = number_format($formatedNumber, 2, ',', '.');
        break;
    }

    return $formatedNumber;
  }
}

if (!function_exists('getFloatFromInput')) {
  /**
   * Ex: Transform 1.234,56 in 1234.56
   */
  function getFloatFromInput(string $number): float
  {
    $number = str_replace('.', '', $number);
    $number = str_replace(',', '.', $number);

    return (float) $number;
  }
}

if (!function_exists('truncateString')) {
  function truncateString(string $string, int $length, bool $showEllipsis = true): string
  {
    $truncatedString = $string;

    if (strlen($string) > $length) {
      $truncatedString = mb_substr($string, 0, $length);
      $truncatedString .= $showEllipsis ? '...' : '';
    }

    return $truncatedString;
  }
}

if (!function_exists('toBRL')) {
  /**
   * Format float to BRL
   *
   * @param float $float
   * @return string $currency
   */
  function toBRL(float $float): string
  {
    return "R$ " . number_format($float, 2, ',', '.');
  }
}

if (!function_exists('toBRL')) {
  /**
   * Format float to BRL
   *
   * @param float $float
   * @return string $currency
   */
  function toBRL(float $float): string
  {
    return "R$" . number_format($float, 2, ',', '.');
  }
}

if (!function_exists('whatsappLink')) {
  function whatsappLink(string $number, ?string $countryCode = "55", ?string $message = null): string
  {
    $whatsappURL = 'https://wa.me/';

    if (!$number)
      return $whatsappURL;

    $query = http_build_query([
      "text" => urldecode($message) ?? ''
    ]);

    return $whatsappURL . $countryCode . preg_replace('/\D/', '', $number) . "?{$query}";
  }

  if (!function_exists('whatsProperty')) {
    function whatsProperty($data, $property,  ?string $countryCode = "55", ?string $message = null): string
    {
      $whatsappURL = 'https://wa.me/';
  
      $business = $property->typeBusiness->name;
      if ($business === "Compra") {
          $number = $data->whats_sale;
      } else {
        $number = $data->whats_rent;
      }

      if (!$number){
        $number = $data->phone;
      }
      
      if (!$number){
        $number = $data->phone;
        return $whatsappURL;
      }
  
      $query = http_build_query([
        "text" => urldecode($message) ?? ''
      ]);
  
      return $whatsappURL . $countryCode . preg_replace('/\D/', '', $number) . "?{$query}";
    }
  }
}
