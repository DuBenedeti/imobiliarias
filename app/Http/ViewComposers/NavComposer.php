<?php

namespace App\Http\ViewComposers;

use App\Models\Module;
use App\Models\Banner;
use App\Models\Cores;
use App\Models\Advice;
use App\Models\Ajustes;
use App\Models\Colors;
use App\Models\SocialMedia;
use App\Models\Data;
use App\Models\District;
use App\Models\DynamicPages;
use App\Models\LinksTopo;
use App\Models\Phones;
use App\Models\Property;
use App\Models\TipoImovel;
use App\Models\TypeBusiness;
use Auth;
use Illuminate\Contracts\View\View;

class NavComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (Auth::check()) {
            /**
             * Select the active modules to the navigation bar.
             */
            if (Auth::user()->hasRole('Admin'))
                $modules = Module::with('submodules')
                    ->where('status', '=', 'Ativo')
                    ->orderBy('position', 'asc')
                    ->get();
            else
                $modules = Module::with('submodules')
                    ->where('status', '=', 'Ativo')
                    ->orderBy('position', 'asc')
                    ->get();

            $view->with('nav', $modules);


            /**
             * Select the active modules for config.
             */
            $modules = null;
            if (!Auth::user()->hasRole('Usuário')) {
                $modules = Module::where('status', '=', 'Ativo')
                    ->orderBy('position', 'asc')
                    ->get();
            }




            $view->with('navConfig', $modules);
        } else {
            // $hotelMenu = Hotel::where('status', 'Ativo')->get();

            //  $view->with('hotelMenu', $hotelMenu);
        }
        $pop = Banner::where('status', 'Ativo')
            ->where('deleted_at', null)
            ->limit('1')->get();
        $view->with('pop', $pop);

        $socialMedia = SocialMedia::get();
        $view->with('socialMedia', $socialMedia);

        $linksTopo = LinksTopo::get();
        $view->with('linksTopo', $linksTopo);

        $data = Data::first();
        $view->with('data', $data);
        
        
        $colors = Colors::first();
        $view->with('colors', $colors);

        $phones = Phones::get();
        $view->with('phones', $phones);

        $tipoImovel = TipoImovel::orderBy('name', 'ASC')->get();
        $view->with('tipoImovel', $tipoImovel);

        $typeBusiness = TypeBusiness::get();
        $view->with('typeBusiness', $typeBusiness);
        
        $districts = District::orderBy('name', 'ASC')->get();
        $view->with('districts', $districts);

        $dynamicPages = DynamicPages::where('status', 1)->orderBy('id', 'DESC')->get();
        $view->with('dynamicPages', $dynamicPages);
    }
}
