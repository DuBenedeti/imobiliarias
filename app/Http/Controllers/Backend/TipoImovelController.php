<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\TipoImovel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class TipoImovelController extends Controller
{
    
    public function index()
    {
        $tipoImovel = TipoImovel::orderBy('name', 'ASC')->paginate('20');
        $typeProperty = $tipoImovel;
        // dd($tipoImovel->appends(1)->links());
        return view('tipoImovel.index', compact('tipoImovel', 'typeProperty'));
    }

    public function create()
    {   
        return view('tipoImovel.create');
    }

    public function store(Request $request)
    {        
      $request['slug'] = $this->generateSlug($request['name']);

        TipoImovel::create($request->all());

        

        session()->flash('success', 'Cadastro realizado com sucesso.');

        return redirect()->route('tipoImovel.index');
    }

    public function edit($id)
    {   
        $tipoImovel = TipoImovel::findOrFail($id);
        return view('tipoImovel.edit', ['tipoImovel' => $tipoImovel]);
    }

    public function update(Request $request, $id)
    {
        $tipoImovel = TipoImovel::findOrFail($id);

        $tipoImovel->update($request->all());

        session()->flash('success', 'Cadastro editado com sucesso.');
        return redirect()->route('tipoImovel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipoImovel = TipoImovel::findOrFail($id);
        try{
            $tipoImovel->delete();

            session()->flash('success', "Tipo de imóvel excluído com sucesso.");
            return redirect()->back();

        } catch (\Throwable $th) {
            session()->flash('error', "Erro ao excluir tipo de imóvel. Código: ". $th->errorInfo[0]);
            return redirect()->back();
        }
    }

    protected function generateSlug($slug) {

        $slug = Str::slug($slug, '-');

        $slugs = TipoImovel::where('slug', 'like', "$slug%")->get();
        if ( $slugs->count() === 0 ) {
            return $slug;
        }

        $lastSlug = $slugs->sortBy('id')->last()->slug;
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));

        return $slug . '-' . ($lastSlugNumber + 1);
    }
}