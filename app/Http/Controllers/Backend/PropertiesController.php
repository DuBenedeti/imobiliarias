<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\AttributesProperty;
use App\Models\Property;
use App\Models\TipoImovel;
use App\Models\State;
use App\Models\District;
use App\Models\Condominium;
use App\Models\PropertyAttribute;
use App\Models\PropertyPhotos;
use App\Models\TypeBusiness;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Response;
use Image;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Storage;

class PropertiesController extends Controller
{

  public function index(Request $request)
  {
    // dd($request->end_price);
    $properties = Property::select('*')
    ->price($request->start_price, $request->end_price)
    ->typeProperty($request->type_property_id)
    ->typeBusiness($request->business)
    ->status($request->status)
    ->externalId($request->external_id)
    ->orderBy('created_at', 'DESC')
    ->get();
    if ($request->title) {
        $propertiesTitles = [];
        foreach ($properties as $property) {
          if (strpos(strtolower($property->getTitle()), $request->title) !== false) {
            array_push($propertiesTitles, $property);
          }
        }
        $properties = $propertiesTitles;
      }
    $properties = $this->paginate($properties);
    
    $realEstateTypes = TipoImovel::orderBy('name', 'ASC')->get();
    return view('properties.index', compact('properties', 'realEstateTypes'));
  }

  public function create()
  {
    $tipoImovel = TipoImovel::orderBy('name', 'ASC')->get();
    $typeBusiness = TypeBusiness::get();
    $states = State::select('id', 'initials')->pluck('initials', 'id');
    $district = District::select('id', 'name')->pluck('name', 'id');
    $condominium = Condominium::select('id', 'name')->pluck('name', 'id');
    return view('properties.create', compact('tipoImovel', 'typeBusiness', 'states', 'district'));
  }

  public function store(Request $request)
  {
    $data = $request->all();
    if($data['district'] == 0){
      session()->flash('error', 'Selecione um Bairro');
      return redirect()->back();
    }

    $data['district_id'] = $data['district'];
    $data['condominium_id'] = $data['condominium'];
    $data['status'] = 1;
    $data['price'] = str_replace(".", "", $data['price']);
    $data['price'] = str_replace(",", ".", $data['price']);

    $data['external_id'] = $this->generateExternalId($data);
    if($data['condominium_id'] == '0'){
      $data['condominium_id'] = null;
    }
    // try {
      $property = Property::create($data);
  
      $slug = self::generateSlug($property);
      $property->slug = $slug;
      $property->save();
      
      if($request->image){
        $image = self::saveImage($request, $property);
        $property->image = $image;
      }
      $property->save();
      
      self::saveAttributes($data, $property->id);
  
      session()->flash('success', 'Cadastro realizado com sucesso.');
      return redirect()->route('properties.index');
    // } catch (\Throwable $th) {

    //   session()->flash('error', 'Erro para cadastrar o imóvel. Código: '.$th->errorInfo[0]);
    //   return redirect()->back();
    // }


  }

  public function edit($id)
  {
    $property = Property::findOrFail($id);

    $tipoImovel = TipoImovel::orderBy('name', 'ASC')->get();
    $typeBusiness = TypeBusiness::get();
    $states = State::select('id', 'initials')->pluck('initials', 'id');
    return view('properties.edit', compact('property', 'tipoImovel', 'typeBusiness', 'states'));
  }

  public function update(Request $request, $id)
  {
    $data = $request->all();
    $property = Property::findOrFail($id);
    $attributes = AttributesProperty::where('property_id', $id)->get();
    foreach ($attributes as $attribute) {
      $attribute->delete();
    }

    self::saveAttributes($data, $id);

    $data['district_id'] = $data['district'];
    $data['condominium_id'] = $data['condominium'];
    $data['price'] = str_replace(".", "", $data['price']);
    $data['price'] = str_replace(",", ".", $data['price']);
    if($data['condominium_id'] == '0'){
      $data['condominium_id'] = null;
    }
    try{
      $property->update($data);

      $slug = self::generateSlug($property);
      $property->slug = $slug;
      if($request->image){
        $image = self::saveImage($request, $property);
        $property->image = $image;
      }
      $property->save();

      session()->flash('success', 'Cadastro editado com sucesso.');
      return redirect()->route('properties.index');
    } catch (\Throwable $th) {

      session()->flash('error', 'Erro para cadastrar o imóvel. Código: '.$th->errorInfo[0]);
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $properties = Property::findOrFail($id);
    $attributes = AttributesProperty::where('property_id', $properties->id)->get();
    try{
      foreach ($attributes as $attribute) {
        $attribute->delete();
      }
      $properties->delete();
      // Comando para deletar imagens ao deletar imovel
      // $this->deletePropertyImages($properties);

      session()->flash('success', "Imóvel excluído com sucesso.");
      return redirect()->back();
    } catch (\Throwable $th) {
      session()->flash('error', "Erro ao excluir imóvel. Código: ". $th->errorInfo[0]);
      return redirect()->back();
    }
  }

  public function deletePropertyTrashImages(){
    $properties = Property::onlyTrashed()->get();
    foreach ($properties as $property){
      $this->deletePropertyImages($property);
    }
    session()->flash('success', "Imagens excluídas com sucesso.");
    return redirect()->back();
  }

  public function deletePropertyImages($property){
    $images = $property->photos;
    if(file_exists(public_path($property->getThumbPath()))) {
      unlink(public_path($property->getThumbPath()));
    }
    foreach ($images as $image) {
      $image->delete();
      if (file_exists(public_path($image->getThumbPath()))){
        unlink(public_path($image->getThumbPath()));
      }
      if (file_exists(public_path($image->getImagePath()))){
        unlink(public_path($image->getImagePath()));
      }
    }
  }

  protected function generateSlug($property)
  {
    $typePropertySlug = $property->typeProperty->slug;
    $area = $property->meters ?? $property->total_area;
    $city = strtolower($property->district->city->name);
    $state = strtolower($property->district->city->state->initials);
    $id = $property->id;

    return $typePropertySlug . '-' . $area . 'm-' . $city . '-' . $state . '-' . $id;
  }

  public function status(Request $request)
  {
    $id = (int) $request->id;
    $status = $request->status;
    $tipo = $request->tipo;

    $code = 418; //I'm a teapot!

    if ($tipo == "Pop") {
      Property::where('status', 1)->where('mobile', 'Pop')->update(['status' => 0]);
    }

    if ($id and (($status == 1) || ($status == 0))) {
      $module = Property::findOrFail($id);
      $module->status = $status;
      if ($module->save()) $code = 200;
    }

    return $code;
  }

  public function saveAttributes($data, $id)
  {
    $attributes = $data['attributes'] ?? null;
    if ($attributes) {
      foreach ($attributes as $key => $attribute) {
        if ($attribute != null) {
          $attr['value'] = $attribute;
          $attr['property_id'] = $id;
          $attr['attribute_id'] = $key;
          AttributesProperty::create($attr);
        }
      }
    }
    $money = $data['money'] ?? null;
    if ($money) {
      foreach ($money as $key => $attribute) {
        if ($attribute != null) {
          $attr['value'] = $attribute;
          $attr['property_id'] = $id;
          $attr['attribute_id'] = $key;
          AttributesProperty::create($attr);
        }
      }
    }
    $quantity = $data['quantity'] ?? null;
    if ($quantity) {
      foreach ($quantity as $key => $attribute) {
        if ($attribute != null) {
          $attr['value'] = $attribute;
          $attr['property_id'] = $id;
          $attr['attribute_id'] = $key;
          AttributesProperty::create($attr);
        }
      }
    }
    $text = $data['text'] ?? null;
    if ($text) {
      foreach ($text as $key => $attribute) {
        if ($attribute != null) {
          $attr['value'] = $attribute;
          $attr['property_id'] = $id;
          $attr['attribute_id'] = $key;
          AttributesProperty::create($attr);
        }
      }
    }
  }

  public function photos($id)
  {
    $property = Property::findOrFail($id);
    $photos = PropertyPhotos::where('property_id', $id)->orderBy('order', 'ASC')->get();

    return view('properties.photos', ['photos' => $photos, 'property' => $property]);
  }

  public function upload($id, Request $request)
  {
    $property = Property::findOrFail($id);

    $photo = $request->file('file');

    $imagename = Str::slug($property->slug . '-' . uniqid());
    $imagename .= '.' . $request->file('file')->getClientOriginalExtension();
    $image_name_thumb = "thumb-" . $imagename;

    list($largura, $altura) = getimagesize($photo);

    $destinationPath = public_path('/assets/images/property/thumb');

    if ($largura < $altura) {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else if ($largura == $altura) {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 560, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else if (($largura > 1000) && ($altura > 700)) {

      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    }

    $thumb_img->save($destinationPath . '/' . $image_name_thumb, 100);
    $destinationPath = public_path('/assets/images/property');
    $photo->move($destinationPath, $imagename);

    $request['property_id'] = $property->id;
    $request['image'] = $imagename;
    $request['subtitle'] = $property->title;
    $request['thumb'] = $image_name_thumb;
    PropertyPhotos::create($request->all());

    session()->flash('success', 'Fotos enviadas com sucesso.');
  }
  public function delPhoto($id) {
    $photo = PropertyPhotos::findOrFail($id);

    $photo->delete();

    session()->flash('success', 'Foto excluída com sucesso.');

    return redirect()->back();
}

public function caption($id, Request $request) {
    $photo = PropertyPhotos::findOrFail($id);

    $photo->subtitle = $request->caption;
    $photo->credits  = $request->credits;
    $photo->save();

    session()->flash('success', "Informações adicionadas com sucesso.");

    return redirect()->back();
}

public function order(Request $request) {
    $code = 418; //I'm a teapot!

    foreach ( $request->item as $order => $id ) {
        $photo        = PropertyPhotos::findOrFail($id);
        $photo->order = $order;
        if ( $photo->save() ) {
            $code = 200;
        }
    }

    return $code;
}
  public function getTypesProperty($typePropertyId)
  {

    $types = PropertyAttribute::where('type_property_id', $typePropertyId)->get();
    return Response::json($types);
  }

  public function getAttributesProperty($propertyId)
  {
    $types = AttributesProperty::where('property_id', $propertyId)->get();
    return Response::json($types);
  }

  public function saveImage($request, $property)
  {
    $photo = $request->file('image');

    $imagename = Str::slug($property->slug . '-' . uniqid());
    $imagename .= '.' . $photo->getClientOriginalExtension();
    $image_name_thumb = "thumb-" . $imagename;

    list($largura, $altura) = getimagesize($photo);

    $destinationPath = public_path('/assets/images/property/thumb');


    if ($largura < $altura) {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else if ($largura == $altura) {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 560, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else if (($largura > 1000) && ($altura > 700)) {

      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    }

    $thumb_img->save($destinationPath . '/' . $image_name_thumb, 100);
    $destinationPath = public_path('/assets/images/property');
    $photo->move($destinationPath, $imagename);
    return $image_name_thumb;
  }
  public static function paginate($items, $perPage = "20", $page = null, $options = [])
  {
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $items = $items instanceof Collection ? $items : Collection::make($items);

    $options = ['path' => url()->current()];
    return new LengthAwarePaginator($items->forPage($page, $perPage), count($items), $perPage, $page, $options);
  }

  public static function generateExternalId($request){
    $typeProperty = TipoImovel::findOrFail($request['type_property_id']);
    $initials = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$typeProperty->name);

    $initials = strtoupper(substr($initials, 0, 2)).$typeProperty->id;
    $number = count($typeProperty->propertiesWithTrashed)+1;
    $number = str_pad($number , 4 , '0' , STR_PAD_LEFT);
    $code = $initials.$number;
    return $code;

  }
}
