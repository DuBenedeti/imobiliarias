<?php

namespace App\Http\Controllers\Backend;

use App\Models\CategoryBag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\LinksTopo;

class LinksTopoController extends Controller
{
    public function index()
    {
        $linksTopo = LinksTopo::get();
        return view('linksTopo.index', compact('linksTopo'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        linksTopo::create($request->all());

        session()->flash('success', 'Link cadastrado com sucesso.');

        return redirect()->route('linksTopo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Ver o usuario para ser atualizado ou falhar se não existir
        $linksTopo = LinksTopo::findOrFail($id);


        // Salva
        $linksTopo->update($request->all());

        // Retorna com mensagem
        session()->flash('success', 'Link editado com sucesso.');
        return redirect()->route('linksTopo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Pega o Banner e deleta
        $linksTopo = LinksTopo::findOrFail($id);
        $linksTopo->delete();
        // Retorna e exibe mensagem
        session()->flash('success', "Link excluído com sucesso.");
        return redirect()->back();
    }


    public function status()
    {
        //

    }
}
