<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TrabalheConosco;

class TrabalheConoscoController extends Controller
{
    public function index(Request $request)
    {
        
        $trabalheConosco = TrabalheConosco::keywords($request->keyword)->orderBy('created_at', 'DESC')->get();
        return view('trabalheConosco.index', ['trabalheConosco' => $trabalheConosco]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   ///
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //... 
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
        $trabalheConosco = TrabalheConosco::findOrFail($id);

        return view('trabalheConosco.edit', ['trabalheConosco' => $trabalheConosco,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status()
    {//
    }

    protected function generateSlug($slug) {

        $slug = str_slug($slug);

        $slugs = TrabalheConosco::where('slug', 'like', "$slug%")->get();
        if ( $slugs->count() === 0 ) {
            return $slug;
        }

        $lastSlug       = $slugs->sortBy('id')->last()->slug;
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));

        return $slug . '-' . ($lastSlugNumber + 1);
    }
    public function download($id)
    {
        $download = TrabalheConosco::where('id', $id)->first();
        $filePath = public_path("assets/curriculos/$download->file");
        $headers = ['Content-Type: application/.$file->type'];
        $fileName = $download->file;
        return response()->download($filePath, $fileName, $headers);
    }
}
