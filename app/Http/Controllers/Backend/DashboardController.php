<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Contact;
use App\Models\PropertyContact;
use App\Models\Property;
use App\User;

class DashboardController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    function index()
    {
        $properties = Property::where('status', 1)->count();
        $banners = Banner::where('status', 'Ativo')->count();
        $users = User::where('status', 'Ativo')->where('email', '!=', 'sistema@prestige.com.br')->count();
        $contacts = Contact::count();
        $propertyContact = PropertyContact::orderBy('dateVisit', 'DESC')->limit(10)->get();

        return view('dashboard.index', compact('properties', 'users', 'contacts', 'banners', 'propertyContact'));
    }
}
