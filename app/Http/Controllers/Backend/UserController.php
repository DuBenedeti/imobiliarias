<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;


use App\Models\Module;
use Illuminate\Support\Facades\Redirect;
use Validator;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:users');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->hasRole('Admin')){
            $users = User::allExceptFirst()
            ->name($request->name)
            ->email($request->email)
            ->status($request->status)->paginate(config('helpers.qtdPerPag'));
            
            return view('user.index', compact('users'));
        }else{
            session()->flash('success', 'Cadastro editado com sucesso.');
            return redirect()->route('dashboard.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'name' => 'required|max:60',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ];

        $msg = [
            'name.required' => 'O Campo nome é obrigatório',
            'name.max' => 'o Campo Nome tem o limite de :max caracteres',
            'email.required' => 'O Campo email é obrigatório',
            'password.required' => 'O Campo Senha é obrigatório',
        ];

        $validator = Validator::make($request->all(), $rule, $msg);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        $request['password'] = bcrypt($request->password);

        // Recebendo a imagem, alterando nome, salvando no banco e movendo para pasta profile
        if (isset($request->user_avatar)) {
            $file = $request->file('user_avatar')->getClientOriginalName();
            $image_name = time() . "-" . $file;

            $request->file('user_avatar')->move('assets/images/profile', $image_name);
            $request['image'] = $image_name;
        }

        // Salva
        $user = User::create($request->all());
        $user->assignRole('Usuário');

        // Retorna com mensagem
        session()->flash('success', 'Cadastro realizado com sucesso.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit', compact('user'));
    }

    public function alt_pass($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit_pass', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Ver o usuario para ser atualizado ou falhar se não existir
        $user = User::findOrFail($id);

        // Se possui senha encrypta
        if (isset($request->password)) {
            $request['password'] = bcrypt($request->password);
        } else {
            $request['password'] = $user->password;
        }

        if (isset($request->user_avatar))
        {
            // Recebendo a imagem, alterando nome, salvando no banco e movendo para pasta profile
            $file = $request->file('user_avatar')->getClientOriginalName();
            $image_name = time()."-".$file;

            $request->file('user_avatar')->move('assets/images/profile', $image_name);
            $request['image'] = $image_name;
        }


        // Salva
        $user->update($request->all());

        // Retorna com mensagem
        session()->flash('success', 'Cadastro editado com sucesso.');
        return redirect()->route('users.index');

    }

    public function update_pass(Request $request, $id)
    {
        $rule = [

            'password' => 'required|confirmed',
        ];

        $msg = [
            'password.required' => 'O Campo Senha é obrigatório',
        ];

        $validator = Validator::make($request->all(), $rule, $msg);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }

        // Ver o usuario para ser atualizado ou falhar se não existir
        $user = User::findOrFail($id);

        // Se possui senha encrypta
        if (isset($request->password)) {
            $request['password'] = bcrypt($request->password);
        } else {
            $request['password'] = $user->password;
        }

        if (isset($request->user_avatar))
        {
            // Recebendo a imagem, alterando nome, salvando no banco e movendo para pasta profile
            $file = $request->file('user_avatar')->getClientOriginalName();
            $image_name = time()."-".$file;

            $request->file('user_avatar')->move('assets/images/profile', $image_name);
            $request['image'] = $image_name;
        }

        // Salva
        $user->update($request->all());

        // Retorna com mensagem
        session()->flash('success', 'Cadastro editado com sucesso.');
        return redirect()->route('users.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Verifica se usuário é o prestige
        if ($id == 1) {
            abort(403);
        }

        // Pega o usuário e deleta
        $user = User::findOrFail($id);
        $user->delete();


        // Retorna e exibe mensagem
        session()->flash('success', "Usuário excluído com sucesso.");
        return redirect()->back();
    }


    public function resetPassword($id)
    {
        $new_password = str_random(8);

        $user = User::findOrFail($id);
        $user->password = bcrypt($new_password);
        $user->save();

        Mail::send('emails.users.password', ['new_password' => $new_password, 'user' => $user], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Alteração de Senha');
        });

        session()->flash('success', 'Senha alterada e enviada com sucesso.');
        return redirect()->back();
    }

    /**
     * @param $cod
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editPermission($cod)
    {
        // Verifica se usuário é o prestige
        if ($cod == 1) {
            abort(403);
        }

        // Pegar o usuário que vai ser editado as permissões
        $user = User::findOrFail($cod);

        //retornar a view
        return view('user.partial.permission', compact('user'));
    }

    /**
     * @param Request $request
     * @param $cod
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePermission(Request $request, $cod)
    {
        // Pegar o usuário que terá as permissões atualizadas.
        $user = User::findOrFail($cod);

        // Preparar para deletar todas as permissões atuais deste usuário
        foreach ($user->permissions as $perm) {
            // Deletar todas as permissões
            $user->revokePermissionTo($perm->name);
        }

        // Adicionar as novas permissões para este usuário.
        if ($request->permissions)
            $user->givePermissionTo($request->permissions);

        // Adicionar mensagem e retornar
        session()->flash('success', 'Permissões atualizadas com sucesso.');
        return redirect()->back();

    }
/**
     * @param UploadedFile|null $request
     * @return JsonResponse
     */
    protected function validAndUploadImg(UploadedFile $request = null): JsonResponse
    {
        // Verifica se existe
        if (!$request) {
            return response()->json(['msg' => 'Imagem não uploaded', 'type' => 'not found', 'localImg' => null]);
        }

        // Verifica se possui imagem
        if ($request->isValid()) {

            // Efetuar o upload da imagem
            $helper = new HelperController();

            // Chamar metodo
            $ret = $helper->moveImg($this->moveTo, $request);

            // Retorna o JSON
            return response()->json($ret->getData());
        }
    }

    public function status(Request $request)
    {
        $id = (int) $request->id;
        $status = $request->status;

        $code = 418; //I'm a teapot!

        if ( $id and (($status == 'Ativo') || ($status == 'Inativo')) ) {
            $module = User::findOrFail($id);
            $module->status = $status;
            if ($module->save()) $code = 200;
        }

        return $code;

    }

}