<?php

namespace App\Http\Controllers\Backend;

use App\Models\Banner;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Validator;
use Image;
use Intervention\Image\ImageManagerStatic;

class BannerController extends Controller
{
    public function index()
    {
        $banners = Banner::orderBy('range', 'ASC')->get();
        $user = User::all();

        return view('banner.index', ['banners' => $banners, 'users' => $user]);
    }

    public function create()
    {
        return view('banner.create');
    }

    public function store(Request $request)
    {

        if ($request->mobile == 'Sim') {

            if ($request->file('imagem') == '') {

                $rule = [
                    'imagem' => 'required',
                ];

                $msg = [

                    'imagem.required' => 'Preencha este campo.',
                ];
                $validator = Validator::make($request->all(), $rule, $msg);

                if ($validator->fails()) {
                    session()->flash('error', 'Erro ao cadastrar! O Campo Imagem é Obrigatório');

                    return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

                }

            }

            list($largura, $altura) = getimagesize($request->file('imagem'));

            if ($largura > $altura) {

                $largura_max = 1920;
                $largura_max2 = 1920;
                $altura_max = 700;
                $altura_max2 = 1400;

            }
            if ($largura < $altura) {

                $rule2 = [
                    'imagem' => 'required',

                ];

                $msg2 = [
                    'imagem.required' => 'A Largura da imagem não pode ser menor que a altura. Verifique!',

                ];

                $validator2 = \Illuminate\Support\Facades\Validator::make($request->all(), $rule2, $msg2);

                session()->flash('error', 'A Largura da imagem não pode ser menor que a altura. Verifique!');
                return redirect()->back()->withErrors($validator2->errors())->withInput($request->all());

            }
            if ($largura == $altura) {

                $largura_max = 1920;
                $largura_max2 = 1920;
                $altura_max = 700;
                $altura_max2 = 1400;

            }

            $path = 'assets/images/banner/thumb';
            $path2 = 'assets/images/banner';

            $file = $request->file('imagem')->getClientOriginalName();
            $image_name = time() . "-" . $file;

            $image_name_thumb = "thumb" . $image_name;

            $img = imagecreatefromjpeg($request->file('imagem'));
            $img2 = imagecreatefromjpeg($request->file('imagem'));

            $original_x = imagesx($img); //largura
            $original_x2 = imagesx($img); //largura
            $original_y = imagesy($img); //altura
            $original_y2 = imagesy($img); //altura

            $diretorio = $path . "/" . $image_name_thumb;
            $diretorio2 = $path2 . "/" . $image_name;

            // verifica se a largura ou altura da imagem é maior que o valor
            // máximo permitido

            if (($original_x > $largura_max) || ($original_y > $altura_max)) {
                // verifica o que é maior na imagem, largura ou altura?
                if ($original_x > $original_y) {
                    $altura_max = ($largura_max * $original_y) / $original_x;
                } else {
                    $largura_max = ($altura_max * $original_x) / $original_y;
                }
                $nova = imagecreatetruecolor($largura_max, $altura_max);
                imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura_max, $altura_max, $original_x, $original_y);
                imagejpeg($nova, $diretorio);
                imagedestroy($nova);
                imagedestroy($img);
                // se for menor, nenhuma alteração é feita
            } else {
                imagejpeg($img, $diretorio);
                imagedestroy($img);
            }

            if (($original_x2 > $largura_max2) || ($original_y2 > $altura_max2)) {
                // verifica o que é maior na imagem, largura ou altura?
                if ($original_x2 > $original_y2) {
                    $altura_max2 = ($largura_max2 * $original_y2) / $original_x2;
                } else {
                    $largura_max2 = ($altura_max2 * $original_x2) / $original_y2;
                }
                $nova2 = imagecreatetruecolor($largura_max2, $altura_max2);
                imagecopyresampled($nova2, $img2, 0, 0, 0, 0, $largura_max2, $altura_max2, $original_x2, $original_y2);
                imagejpeg($nova2, $diretorio2);
                imagedestroy($nova2);
                imagedestroy($img2);
                // se for menor, nenhuma alteração é feita
            } else {
                imagejpeg($img2, $diretorio2);
                imagedestroy($img2);
            }

            $image_name_thumb = "thumb" . $image_name;

            /**
             *
             * Fim do Bloco de tratamento da imagem
             *
             */

            $request['image'] = $image_name;

            $request['thumb'] = $image_name_thumb;

                $request['mobile'] = 'Sim';

        }

        if ($request->mobile == 'Não') {

            if ($request->file('imagem') == '') {

                $rule = [
                    'imagem' => 'required',
                ];

                $msg = [

                    'imagem.required' => 'Preencha este campo.',
                ];
                $validator = Validator::make($request->all(), $rule, $msg);

                if ($validator->fails()) {
                    session()->flash('error', 'Erro ao cadastrar! O Campo Imagem é Obrigatório');

                    return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

                }

            }

            list($largura, $altura) = getimagesize($request->file('imagem'));

            if(($largura > '1920') || ($altura > '500') || ($largura < '1920') || ($altura < '500')){
                    
                $rule2 = [
                    'imagem' => 'required',
                ];
                $msg2 = [
                    'imagem.required' => 'A Largura da imagem não pode ser menor que a altura. Verifique!',
                ];

                $validator2 = \Illuminate\Support\Facades\Validator::make($request->all(), $rule2, $msg2);
                session()->flash('error', 'O Tamanho da imagem diferente de 1920x500. Verifique!');
                return redirect()->back()->withErrors($validator2->errors())->withInput($request->all());

            }
                $largura_max = 1920;
                $largura_max2 = 1920;
                $altura_max = 500;
                $altura_max2 = 500;

            $path = 'assets/images/banner/thumb';
            $path2 = 'assets/images/banner';

            $file = $request->file('imagem')->getClientOriginalName();
            $image_name = time() . "-" . $file;

            $image_name_thumb = "thumb" . $image_name;

            $img = imagecreatefromjpeg($request->file('imagem'));
            $img2 = imagecreatefromjpeg($request->file('imagem'));

            $original_x = imagesx($img);
            $original_x2 = imagesx($img);
            $original_y = imagesy($img);
            $original_y2 = imagesy($img);

            $diretorio = $path . "/" . $image_name_thumb;
            $diretorio2 = $path2 . "/" . $image_name;

            if (($original_x > $largura_max) || ($original_y > $altura_max)) {
                // verifica o que é maior na imagem, largura ou altura?
                if ($original_x > $original_y) {
                    $altura_max = ($largura_max * $original_y) / $original_x;
                } else {
                    $largura_max = ($altura_max * $original_x) / $original_y;
                }
                $nova = imagecreatetruecolor($largura_max, $altura_max);
                imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura_max, $altura_max, $original_x, $original_y);
                imagejpeg($nova, $diretorio);
                imagedestroy($nova);
                imagedestroy($img);
            } else {
                imagejpeg($img, $diretorio);
                imagedestroy($img);
            }

            if (($original_x2 > $largura_max2) || ($original_y2 > $altura_max2)) {
                // verifica o que é maior na imagem, largura ou altura?
                if ($original_x2 > $original_y2) {
                    $altura_max2 = ($largura_max2 * $original_y2) / $original_x2;
                } else {
                    $largura_max2 = ($altura_max2 * $original_x2) / $original_y2;
                }
                $nova2 = imagecreatetruecolor($largura_max2, $altura_max2);
                imagecopyresampled($nova2, $img2, 0, 0, 0, 0, $largura_max2, $altura_max2, $original_x2, $original_y2);
                imagejpeg($nova2, $diretorio2);
                imagedestroy($nova2);
                imagedestroy($img2);
            } else {
                imagejpeg($img2, $diretorio2);
                imagedestroy($img2);
            }

            $image_name_thumb = "thumb" . $image_name;

            $request['image'] = $image_name;
            $request['thumb'] = $image_name_thumb;
            $request['mobile'] = 'Não';
        }

        if ($request->mobile == 'Pop') {

            if ($request->file('imagem') == '') {
                $rule = ['imagem' => 'required', ];
                $msg = ['imagem.required' => 'Preencha este campo.',];
                $validator = Validator::make($request->all(), $rule, $msg);

                if ($validator->fails()) {
                    session()->flash('error', 'Erro ao cadastrar! O Campo Imagem é Obrigatório');
                    return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
                }
            }

            list($largura, $altura) = getimagesize($request->file('imagem'));

            if(($largura > '1000') || ($altura > '700') || ($largura < '1000') || ($altura < '700')){
                    
                $rule2 = [
                    'imagem' => 'required',
                ];
                $msg2 = [
                    'imagem.required' => 'A Largura da imagem não pode ser menor que a altura. Verifique!',
                ];

                $validator2 = \Illuminate\Support\Facades\Validator::make($request->all(), $rule2, $msg2);
                session()->flash('error', 'O Tamanho da imagem diferente de 1000x700. Verifique!');
                return redirect()->back()->withErrors($validator2->errors())->withInput($request->all());

            } else {

            if ($largura > $altura) {

                $largura_max = 1000;
                $largura_max2 = 1000;
                $altura_max = 700;
                $altura_max2 = 700;

            }
            
            if ($largura < $altura) {

                $rule2 = [
                    'imagem' => 'required',

                ];

                $msg2 = [
                    'imagem.required' => 'A Largura da imagem não pode ser menor que a altura. Verifique!',

                ];

                $validator2 = \Illuminate\Support\Facades\Validator::make($request->all(), $rule2, $msg2);

                session()->flash('error', 'A Largura da imagem não pode ser menor que a altura. Verifique!');
                return redirect()->back()->withErrors($validator2->errors())->withInput($request->all());

            }

            if ($largura == $altura) {

                $largura_max = 1000;
                $largura_max2 = 1000;
                $altura_max = 700;
                $altura_max2 = 700;
            }

            $path = 'assets/images/banner/thumb';
            $path2 = 'assets/images/banner';

            $file = $request->file('imagem')->getClientOriginalName();
            $image_name = time() . "-" . $file;

            $image_name_thumb = "thumb" . $image_name;

            $img = imagecreatefromjpeg($request->file('imagem'));
            $img2 = imagecreatefromjpeg($request->file('imagem'));

            $original_x = imagesx($img); //largura
            $original_x2 = imagesx($img); //largura
            $original_y = imagesy($img); //altura
            $original_y2 = imagesy($img); //altura

            $diretorio = $path . "/" . $image_name_thumb;
            $diretorio2 = $path2 . "/" . $image_name;

            if (($original_x > $largura_max) || ($original_y > $altura_max)) {
                // verifica o que é maior na imagem, largura ou altura?
                if ($original_x > $original_y) {
                    $altura_max = ($largura_max * $original_y) / $original_x;
                } else {
                    $largura_max = ($altura_max * $original_x) / $original_y;
                }
                $nova = imagecreatetruecolor($largura_max, $altura_max);
                imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura_max, $altura_max, $original_x, $original_y);
                imagejpeg($nova, $diretorio);
                imagedestroy($nova);
                imagedestroy($img);
                // se for menor, nenhuma alteração é feita
            } else {
                imagejpeg($img, $diretorio);
                imagedestroy($img);
            }

            if (($original_x2 > $largura_max2) || ($original_y2 > $altura_max2)) {
                // verifica o que é maior na imagem, largura ou altura?
                if ($original_x2 > $original_y2) {
                    $altura_max2 = ($largura_max2 * $original_y2) / $original_x2;
                } else {
                    $largura_max2 = ($altura_max2 * $original_x2) / $original_y2;
                }
                $nova2 = imagecreatetruecolor($largura_max2, $altura_max2);
                imagecopyresampled($nova2, $img2, 0, 0, 0, 0, $largura_max2, $altura_max2, $original_x2, $original_y2);
                imagejpeg($nova2, $diretorio2);
                imagedestroy($nova2);
                imagedestroy($img2);
                // se for menor, nenhuma alteração é feita
            } else {
                imagejpeg($img2, $diretorio2);
                imagedestroy($img2);
            }

            $image_name_thumb = "thumb" . $image_name;

            $request['image'] = $image_name;
            $request['thumb'] = $image_name_thumb;
            $request['mobile'] = 'Pop';
            }
        }

        if ($request->mobile == '') {

            session()->flash('error', 'Erro ao cadastrar! O Campo Tipo do Banner é Obrigatório.');

            $rule = [
                'mobile' => 'required',
            ];

            $msg = [

                'mobile.required' => 'Preencha este campo.',
            ];

            $validator = Validator::make($request->all(), $rule, $msg);
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        Banner::create($request->all());

        session()->flash('success', 'Cadastro realizado com sucesso.');

        return redirect()->route('banners.index');
    }

    public function show($id)  {  }

    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return view('banner.edit', ['banner' => $banner]);
    }
    public function update(Request $request, $id)
    {
        $path = 'assets/images/banner/thumb';
        $banner = Banner::findOrFail($id);

        if (isset($request->imagem)) {
            if ($request->mobile == 'Sim') {

                if ($request->file('imagem') == '') {

                    $rule = [
                        'imagem' => 'required',
                    ];

                    $msg = [

                        'imagem.required' => 'Preencha este campo.',
                    ];
                    $validator = Validator::make($request->all(), $rule, $msg);

                    if ($validator->fails()) {
                        session()->flash('error', 'Erro ao cadastrar! O Campo Imagem é Obrigatório');

                        return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

                    }
                }

                list($largura, $altura) = getimagesize($request->file('imagem'));

                if ($largura > $altura) {

                    $largura_max = 1920;
                    $largura_max2 = 1920;
                    $altura_max = 700;
                    $altura_max2 = 1400;

                }
                if ($largura < $altura) {

                    $rule2 = [
                        'imagem' => 'required',

                    ];

                    $msg2 = [
                        'imagem.required' => 'A Largura da imagem não pode ser menor que a altura. Verifique!',

                    ];

                    $validator2 = \Illuminate\Support\Facades\Validator::make($request->all(), $rule2, $msg2);

                    session()->flash('error', 'A Largura da imagem não pode ser menor que a altura. Verifique!');
                    return redirect()->back()->withErrors($validator2->errors())->withInput($request->all());

                }
                if ($largura == $altura) {

                    $largura_max = 1920;
                    $largura_max2 = 1920;
                    $altura_max = 700;
                    $altura_max2 = 1400;

                }

                $path = 'assets/images/banner/thumb';
                $path2 = 'assets/images/banner';

                $file = $request->file('imagem')->getClientOriginalName();
                $image_name = time() . "-" . $file;

                $image_name_thumb = "thumb" . $image_name;

                $img = imagecreatefromjpeg($request->file('imagem'));
                $img2 = imagecreatefromjpeg($request->file('imagem'));

                $original_x = imagesx($img); //largura
                $original_x2 = imagesx($img); //largura
                $original_y = imagesy($img); //altura
                $original_y2 = imagesy($img); //altura

                $diretorio = $path . "/" . $image_name_thumb;
                $diretorio2 = $path2 . "/" . $image_name;

                if (($original_x > $largura_max) || ($original_y > $altura_max)) {
                    // verifica o que é maior na imagem, largura ou altura?
                    if ($original_x > $original_y) {
                        $altura_max = ($largura_max * $original_y) / $original_x;
                    } else {
                        $largura_max = ($altura_max * $original_x) / $original_y;
                    }
                    $nova = imagecreatetruecolor($largura_max, $altura_max);
                    imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura_max, $altura_max, $original_x, $original_y);
                    imagejpeg($nova, $diretorio);
                    imagedestroy($nova);
                    imagedestroy($img);
                } else {
                    imagejpeg($img, $diretorio);
                    imagedestroy($img);
                }

                if (($original_x2 > $largura_max2) || ($original_y2 > $altura_max2)) {
                    // verifica o que é maior na imagem, largura ou altura?
                    if ($original_x2 > $original_y2) {
                        $altura_max2 = ($largura_max2 * $original_y2) / $original_x2;
                    } else {
                        $largura_max2 = ($altura_max2 * $original_x2) / $original_y2;
                    }
                    $nova2 = imagecreatetruecolor($largura_max2, $altura_max2);
                    imagecopyresampled($nova2, $img2, 0, 0, 0, 0, $largura_max2, $altura_max2, $original_x2, $original_y2);
                    imagejpeg($nova2, $diretorio2);
                    imagedestroy($nova2);
                    imagedestroy($img2);
                } else {
                    imagejpeg($img2, $diretorio2);
                    imagedestroy($img2);
                }

                $image_name_thumb = "thumb" . $image_name;

                $request['image'] = $image_name;

                $request['thumb'] = $image_name_thumb;

                $request['mobile'] = 'Sim';

            }

            if ($request->mobile == 'Não') {

                if ($request->file('imagem') == '') {

                    $rule = [
                        'imagem' => 'required',
                    ];

                    $msg = [

                        'imagem.required' => 'Preencha este campo.',
                    ];
                    $validator = Validator::make($request->all(), $rule, $msg);

                    if ($validator->fails()) {
                        session()->flash('error', 'Erro ao cadastrar! O Campo Imagem é Obrigatório');

                        return redirect()->back()->withErrors($validator->errors())->withInput($request->all());

                    }

                }

                list($largura, $altura) = getimagesize($request->file('imagem'));

                if(($largura > '1920') || ($altura > '500') || ($largura < '1920') || ($altura < '500')){
                    
                    $rule2 = [
                        'imagem' => 'required',
                    ];
                    $msg2 = [
                        'imagem.required' => 'A Largura da imagem não pode ser menor que a altura. Verifique!',
                    ];
    
                    $validator2 = \Illuminate\Support\Facades\Validator::make($request->all(), $rule2, $msg2);
                    session()->flash('error', 'O Tamanho da imagem diferente de 1920x500. Verifique!');
                    return redirect()->back()->withErrors($validator2->errors())->withInput($request->all());
    
                }
                    $largura_max = 1920;
                    $largura_max2 = 1920;
                    $altura_max = 500;
                    $altura_max2 = 500;

                $path = 'assets/images/banner/thumb';
                $path2 = 'assets/images/banner';

                $file = $request->file('imagem')->getClientOriginalName();
                $image_name = time() . "-" . $file;

                $image_name_thumb = "thumb" . $image_name;

                $img = imagecreatefromjpeg($request->file('imagem'));
                $img2 = imagecreatefromjpeg($request->file('imagem'));

                $original_x = imagesx($img); //largura
                $original_x2 = imagesx($img); //largura
                $original_y = imagesy($img); //altura
                $original_y2 = imagesy($img); //altura

                $diretorio = $path . "/" . $image_name_thumb;
                $diretorio2 = $path2 . "/" . $image_name;

                if (($original_x > $largura_max) || ($original_y > $altura_max)) {
                    // verifica o que é maior na imagem, largura ou altura?
                    if ($original_x > $original_y) {
                        $altura_max = ($largura_max * $original_y) / $original_x;
                    } else {
                        $largura_max = ($altura_max * $original_x) / $original_y;
                    }
                    $nova = imagecreatetruecolor($largura_max, $altura_max);
                    imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura_max, $altura_max, $original_x, $original_y);
                    imagejpeg($nova, $diretorio);
                    imagedestroy($nova);
                    imagedestroy($img);
                    // se for menor, nenhuma alteração é feita
                } else {
                    imagejpeg($img, $diretorio);
                    imagedestroy($img);
                }

                if (($original_x2 > $largura_max2) || ($original_y2 > $altura_max2)) {
                    // verifica o que é maior na imagem, largura ou altura?
                    if ($original_x2 > $original_y2) {
                        $altura_max2 = ($largura_max2 * $original_y2) / $original_x2;
                    } else {
                        $largura_max2 = ($altura_max2 * $original_x2) / $original_y2;
                    }
                    $nova2 = imagecreatetruecolor($largura_max2, $altura_max2);
                    imagecopyresampled($nova2, $img2, 0, 0, 0, 0, $largura_max2, $altura_max2, $original_x2, $original_y2);
                    imagejpeg($nova2, $diretorio2);
                    imagedestroy($nova2);
                    imagedestroy($img2);
                    // se for menor, nenhuma alteração é feita
                } else {
                    imagejpeg($img2, $diretorio2);
                    imagedestroy($img2);
                }

                $image_name_thumb = "thumb" . $image_name;

                /**
                 *
                 * Fim do Bloco de tratamento da imagem
                 *
                 */

                $request['image'] = $image_name;

                $request['thumb'] = $image_name_thumb;

                $request['mobile'] = 'Não';

            }

            if ($request->mobile == 'Pop') {

                if ($request->file('imagem') == '') {
    
                    $rule = [
                        'imagem' => 'required',
                    ];
    
                    $msg = [
    
                        'imagem.required' => 'Preencha este campo.',
                    ];
                    $validator = Validator::make($request->all(), $rule, $msg);
    
                    if ($validator->fails()) {
                        session()->flash('error', 'Erro ao cadastrar! O Campo Imagem é Obrigatório');
    
                        return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
    
                    }
    
                }
    
                list($largura, $altura) = getimagesize($request->file('imagem'));
    
                if ($largura > $altura) {
    
                    $largura_max = 1000;
                    $largura_max2 = 1000;
                    $altura_max = 700;
                    $altura_max2 = 700;
    
                }

                if(($largura > '1000') || ($altura > '700') || ($largura < '1000') || ($altura < '700')){
                    
                    $rule2 = [
                        'imagem' => 'required',
                    ];
                    $msg2 = [
                        'imagem.required' => 'A Largura da imagem não pode ser menor que a altura. Verifique!',
                    ];

                    $validator2 = \Illuminate\Support\Facades\Validator::make($request->all(), $rule2, $msg2);
                    session()->flash('error', 'O Tamanho da imagem diferente de 1000x700. Verifique!');
                    return redirect()->back()->withErrors($validator2->errors())->withInput($request->all());

                } else {

                if ($largura < $altura) {
    
                    $rule2 = [
                        'imagem' => 'required',
    
                    ];
    
                    $msg2 = [
                        'imagem.required' => 'A Largura da imagem não pode ser menor que a altura. Verifique!',
    
                    ];
    
                    $validator2 = \Illuminate\Support\Facades\Validator::make($request->all(), $rule2, $msg2);
    
                    session()->flash('error', 'A Largura da imagem não pode ser menor que a altura. Verifique!');
                    return redirect()->back()->withErrors($validator2->errors())->withInput($request->all());
    
                }
    
                if ($largura == $altura) {
                    $largura_max = 1000;
                    $largura_max2 = 1000;
                    $altura_max = 700;
                    $altura_max2 = 700;
    
                }
    
                $path = 'assets/images/banner/thumb';
                $path2 = 'assets/images/banner';
    
                $file = $request->file('imagem')->getClientOriginalName();
                $image_name = time() . "-" . $file;
    
                $image_name_thumb = "thumb" . $image_name;
    
                $img = imagecreatefromjpeg($request->file('imagem'));
                $img2 = imagecreatefromjpeg($request->file('imagem'));
    
                $original_x = imagesx($img); //largura
                $original_x2 = imagesx($img); //largura
                $original_y = imagesy($img); //altura
                $original_y2 = imagesy($img); //altura
    
                $diretorio = $path . "/" . $image_name_thumb;
                $diretorio2 = $path2 . "/" . $image_name;
    
                // verifica se a largura ou altura da imagem é maior que o valor
                // máximo permitido
    
                if (($original_x > $largura_max) || ($original_y > $altura_max)) {
                    // verifica o que é maior na imagem, largura ou altura?
                    if ($original_x > $original_y) {
                        $altura_max = ($largura_max * $original_y) / $original_x;
                    } else {
                        $largura_max = ($altura_max * $original_x) / $original_y;
                    }
                    $nova = imagecreatetruecolor($largura_max, $altura_max);
                    imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura_max, $altura_max, $original_x, $original_y);
                    imagejpeg($nova, $diretorio);
                    imagedestroy($nova);
                    imagedestroy($img);
                    // se for menor, nenhuma alteração é feita
                } else {
                    imagejpeg($img, $diretorio);
                    imagedestroy($img);
                }
    
                if (($original_x2 > $largura_max2) || ($original_y2 > $altura_max2)) {
                    // verifica o que é maior na imagem, largura ou altura?
                    if ($original_x2 > $original_y2) {
                        $altura_max2 = ($largura_max2 * $original_y2) / $original_x2;
                    } else {
                        $largura_max2 = ($altura_max2 * $original_x2) / $original_y2;
                    }
                    $nova2 = imagecreatetruecolor($largura_max2, $altura_max2);
                    imagecopyresampled($nova2, $img2, 0, 0, 0, 0, $largura_max2, $altura_max2, $original_x2, $original_y2);
                    imagejpeg($nova2, $diretorio2);
                    imagedestroy($nova2);
                    imagedestroy($img2);
                    // se for menor, nenhuma alteração é feita
                } else {
                    imagejpeg($img2, $diretorio2);
                    imagedestroy($img2);
                }
    
                $image_name_thumb = "thumb" . $image_name;
    
                /**
                 *
                 * Fim do Bloco de tratamento da imagem
                 *
                 */
    
                $request['image'] = $image_name;
                $request['thumb'] = $image_name_thumb;
                $request['mobile'] = 'Pop';
                }
    
            }

            if ($request->mobile == '') {

                session()->flash('error', 'Erro ao cadastrar! O Campo Tipo do Banner é Obrigatório.');

                $rule = [
                    'mobile' => 'required',
                ];

                $msg = [

                    'mobile.required' => 'Preencha este campo.',
                ];

                $validator = Validator::make($request->all(), $rule, $msg);
                return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
            }
        }


        /**
         *
         * Fim do Bloco de tratamento da imagem
         *
         */

        // Salva
        $banner->update($request->all());

        // Retorna com mensagem
        session()->flash('success', 'Cadastro editado com sucesso.');
        return redirect()->route('banners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Pega o Banner e deleta
        $banner = Banner::findOrFail($id);
        $banner->delete();
        // Retorna e exibe mensagem
        session()->flash('success', "Banner excluído com sucesso.");
        return redirect()->back();
    }

    public function status(Request $request)
    {
        $id = (int) $request->id;
        $status = $request->status;
        $tipo = $request->tipo;

        $code = 418; //I'm a teapot!

        if($tipo == "Pop") {
            Banner::where('status', 'Ativo')->where('mobile', 'Pop')->update(['status' => 'Inativo']);
        }

        if ( $id and (($status == 'Ativo') || ($status == 'Inativo')) ) {
            $module = Banner::findOrFail($id);
            $module->status = $status;
            if ($module->save()) $code = 200;
        }

        return $code;
    }

    public function order(Request $request) {
        $code = 468; //I'm a teapot!

        foreach ( $request->item as $order => $id ) {
            $photo = Banner::findOrFail($id);
            $photo->range = $order;
            if ( $photo->save() ) {
                $code = 200;
            }
        }

        return $code;
    }
}
