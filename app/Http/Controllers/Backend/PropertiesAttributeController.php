<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PropertyAttribute;
use App\Models\TipoImovel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class PropertiesAttributeController extends Controller
{
    
    public function index()
    {
        $propertiesAttribute = PropertyAttribute::select()
        ->orderBy('name', 'ASC')
        ->name(request()->name ?? null)
        ->type(request()->type ?? null)
        ->paginate('15');
        return view('propertiesAttribute.index', ['propertiesAttribute' => $propertiesAttribute]);
    }

    public function create()
    {   
      $tipoImovel = TipoImovel::orderBy('name', 'ASC')->get();
        return view('propertiesAttribute.create', compact('tipoImovel'));
    }

    public function store(Request $request)
    {        
        PropertyAttribute::create($request->all());

        session()->flash('success', 'Cadastro realizado com sucesso.');

        return redirect()->route('propertiesAttribute.index');
    }

    public function edit($id)
    {   
        $propertiesAttribute = PropertyAttribute::findOrFail($id);
        $tipoImovel = TipoImovel::orderBy('name', 'ASC')->get();

        return view('propertiesAttribute.edit', compact('propertiesAttribute', 'tipoImovel'));
    }

    public function update(Request $request, $id)
    {
        $propertiesAttribute = PropertyAttribute::findOrFail($id);

        $propertiesAttribute->update($request->all());

        session()->flash('success', 'Cadastro editado com sucesso.');
        return redirect()->route('propertiesAttribute.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $propertiesAttribute = PropertyAttribute::findOrFail($id);
        try{
            $propertiesAttribute->delete();
            session()->flash('success', "Atributo do imóvel excluído com sucesso.");
            return redirect()->back();
        } catch (\Throwable $th) {
            session()->flash('error', "Erro ao excluir atributo. Código: ". $th->errorInfo[0]);
            return redirect()->back();
        }
    }

    protected function generateSlug($slug) {

        $slug = Str::slug($slug, '-');

        $slugs = PropertyAttribute::where('slug', 'like', "$slug%")->get();
        if ( $slugs->count() === 0 ) {
            return $slug;
        }

        $lastSlug = $slugs->sortBy('id')->last()->slug;
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));

        return $slug . '-' . ($lastSlugNumber + 1);
    }
}