<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Condominium;
use App\Models\District;
use App\Models\State;
use Illuminate\Support\Facades\Response;

class CondominiumController extends Controller
{
	public function index(Request $request)
	{
		$condominium = Condominium::paginate();
		return view('condominium.index', compact('condominium'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$states = State::select('id', 'name')->pluck('name', 'id');
    $district = District::select('id', 'name')->pluck('name', 'id');
		return view('condominium.create', ['states' => $states, 'district' => $district]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{

		$condominium = Condominium::findOrFail($id);
		$states = State::select('id', 'name')->pluck('name', 'id');

		return view('condominium.edit', ['condominium' => $condominium, 'states' => $states]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		Condominium::where('id', $id)
			->update(['name' => $request->name, 'district_id' => $request->district]);

		session()->flash('success', "Condomínio alterado com sucesso.");
		return redirect()->route('condominium.index');
	}


	public function store(Request $request)
	{
		Condominium::insert(['name' => $request->name, 'district_id' => $request->district]);

		session()->flash('success', "Condomínio cadastrado com sucesso.");
		return redirect()->route('condominium.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$condominium = Condominium::findOrFail($id);
		try{
		$condominium->delete();

		session()->flash('success', "Condomínio excluído com sucesso.");
		return redirect()->route('condominium.index');
		} catch (\Throwable $th) {
			session()->flash('error', "Erro ao excluir condomínio. Código: ". $th->errorInfo[0]);
			return redirect()->back();
		}
	}

}
