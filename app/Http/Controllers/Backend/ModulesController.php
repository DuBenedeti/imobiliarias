<?php

namespace App\Http\Controllers\Backend;

use App\Models\Modules;
use App\Models\ModulesSub;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class ModulesController extends Controller
{
    
    public function index()
    {
        $modules = Modules::orderBy('position', 'ASC')->paginate('30');
        return view('modules.index', ['modules' => $modules]);
    }

    public function create()
    {   
        return view('modules.create');
    }

    public function store(Request $request)
    {        
        $request['slug'] = $this->generateSlug($request['name']);

        Modules::create($request->all());

        DB::table('permissions')->insert([
            ['name' => $request['slug'], 'translate' => 'Gerenciar '.$request['name'], 'guard_name' =>'web']
        ]);

        session()->flash('success', 'Cadastro realizado com sucesso.');

        return redirect()->route('modules.index');
    }

    public function edit($id)
    {   
        $modules = Modules::findOrFail($id);
        return view('modules.edit', ['modules' => $modules]);
    }

    public function update(Request $request, $id)
    {
        $modules = Modules::findOrFail($id);

        $modules->update($request->all());

        session()->flash('success', 'Cadastro editado com sucesso.');
        return redirect()->route('modules.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modules = Modules::findOrFail($id);
        
        $modules->delete();

        DB::table('permissions')->where('name', $modules->slug)->delete();

        session()->flash('success', "Módulo excluído com sucesso.");
        return redirect()->back();
    }

    public function status()
    {
        $id = (int) Input::get('id');
        $status = Input::get('status');

        $code = 418; 

        if ( $id and (($status == 'Ativo') || ($status == 'Inativo')) ) {
            $module = Modules::findOrFail($id);
            $module->status = $status;
            if ($module->save()) $code = 200;

            DB::table('permissions')
            ->where('name', 'like', $module->slug)
            ->update(['status' => $status]);
        }

        return $code;
    }

    protected function generateSlug($slug) {

        $slug = Str::slug($slug);

        $slugs = Modules::where('slug', 'like', "$slug%")->get();
        if ( $slugs->count() === 0 ) {
            return $slug;
        }

        $lastSlug = $slugs->sortBy('id')->last()->slug;
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));

        return $slug . '-' . ($lastSlugNumber + 1);
    }

    public function order(Request $request) {
        $code = 468; //I'm a teapot!

        foreach ( $request->item as $order => $id ) {
            $modules = Modules::findOrFail($id);
            $modules->position = $order;
            if ( $modules->save() ) {
                $code = 200;
            }
        }

        return $code;
    }

    //**************  SUB MODULES */

    public function indexSub($id)
    {
        $modulesSub = ModulesSub::where('module_id', $id)->orderBy('position', 'ASC')->paginate('30');
        return view('modules.submodules.index', ['modules' => $modulesSub, 'id' => $id]);
    }

    public function createSub($id)
    {   
        return view('modules.submodules.create', ['id' => $id]);
    }

    public function storeSub(Request $request)
    {   
        $request->all();

        $request['slug'] = $this->generateSlug($request['name']);

        ModulesSub::create($request->all());

        return view('modules.submodules.create', ['id' => $request['module_id']]);
    }

    public function editSub($module_id, $id)
    {   
        $modules = ModulesSub::findOrFail($module_id);
        return view('modules.submodules.edit', ['id' => $id, 'modules' => $modules]);
    }

    public function updateSub(Request $request, $id)
    {
        $modules = ModulesSub::findOrFail($id);

        $modules->update($request->all());

        $modulesSub = ModulesSub::where('module_id', $modules->module_id)->orderBy('position', 'ASC')->paginate('30');

        return view('modules.submodules.index', ['modules' => $modulesSub, 'id' => $modules->module_id]);
    }

    public function orderSub(Request $request) {
        $code = 468; 

        foreach ( $request->item as $order => $id ) {
            $modules = ModulesSub::findOrFail($id);
            $modules->position = $order;
            if ( $modules->save() ) {
                $code = 200;
            }
        }

        return $code;
    }

    public function statusSub()
    {   
        $id = (int) Input::get('id');
        $status = Input::get('status');

        $code = 418; 

        if ( $id and (($status == 'Ativo') || ($status == 'Inativo')) ) {
            $module = ModulesSub::findOrFail($id);
            $module->status = $status;
            if ($module->save()) $code = 200;

            

        }
        return $code;
    }

    public function destroySub($id)
    {
        $modules = ModulesSub::findOrFail($id);
        
        $modules->delete();

        DB::table('permissions')->where('name', $modules->slug)->delete();

        session()->flash('success', "Módulo excluído com sucesso.");
        return redirect()->back();
    }

}
