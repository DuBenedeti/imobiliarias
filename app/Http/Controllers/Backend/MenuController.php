<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\MenuSub;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class MenuController extends Controller
{
    
    public function index()
    {
        $menu = Menu::orderBy('position', 'ASC')->paginate('30');
        $menusub = MenuSub::orderBy('position', 'ASC');
        return view('menu.index', ['menu' => $menu, 'menusub' => $menusub]);
    }

    public function create()
    {   
        return view('menu.create');
    }

    public function store(Request $request)
    {        
        $request['slug'] = $this->generateSlug($request['name']);

        Menu::create($request->all());

        

        session()->flash('success', 'Cadastro realizado com sucesso.');

        return redirect()->route('menu.index');
    }

    public function edit($id)
    {   
        $menu = Menu::findOrFail($id);
        return view('menu.edit', ['menu' => $menu]);
    }

    public function update(Request $request, $id)
    {
        $menu = Menu::findOrFail($id);

        $menu->update($request->all());

        session()->flash('success', 'Cadastro editado com sucesso.');
        return redirect()->route('menu.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);
        
        $menu->delete();

        $menu = MenuSub::where('menu_id', $id);
        
        $menu->delete();


        session()->flash('success', "Módulo excluído com sucesso.");
        return redirect()->back();
    }

    public function status(Request $request)
    {
        $id = (int) $request->id;
        $status = $request->status;

        $code = 418; 

        if ( $id and (($status == 'Ativo') || ($status == 'Inativo')) ) {
            $module = Menu::findOrFail($id);
            $module->status = $status;
            if ($module->save()) $code = 200;

           
        }

        return $code;
    }

    protected function generateSlug($slug) {

        $slug = Str::slug($slug, '-');

        $slugs = Menu::where('slug', 'like', "$slug%")->get();
        if ( $slugs->count() === 0 ) {
            return $slug;
        }

        $lastSlug = $slugs->sortBy('id')->last()->slug;
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));

        return $slug . '-' . ($lastSlugNumber + 1);
    }

    public function order(Request $request) {
        $code = 468; //I'm a teapot!

        foreach ( $request->item as $order => $id ) {
            $menu = Menu::findOrFail($id);
            $menu->position = $order;
            if ( $menu->save() ) {
                $code = 200;
            }
        }

        return $code;
    }

    //**************  SUB menu */

    public function indexSub($id)
    {
        $menuSub = menuSub::where('menu_id', $id)->orderBy('position', 'ASC')->paginate('30');
        return view('menu.submenu.index', ['menu' => $menuSub, 'id' => $id]);
    }

    public function createSub($id)
    {   
        return view('menu.submenu.create', ['id' => $id]);
    }

    public function storeSub(Request $request)
    {   
        $request->all();

        $request['slug'] = $this->generateSlug($request['name']);

        menuSub::create($request->all());
        $id = $request['menu_id'];
        $menuSub = menuSub::where('menu_id', $id)->orderBy('position', 'ASC')->paginate('30');
        $menu = Menu::findOrFail($id);
        $menuU['subMenu']='Sim';
        $menu->update($menuU);

        return redirect()->route('menu.indexSub', $id);
        // return view('menu.submenu.index', ['id' => $id, 'menu' => $menuSub]);
    }

    public function editSub($menu_id, $id)
    {   
        $menu = menuSub::findOrFail($menu_id);
        return view('menu.submenu.edit', ['id' => $id, 'menu' => $menu]);
    }

    public function updateSub(Request $request, $id)
    {
        $menu = menuSub::findOrFail($id);

        $menu->update($request->all());

        $menu_id = $menu['menu_id'];


        return redirect()->route('menu.indexSub', $menu_id);


    }

    public function orderSub(Request $request) {
        $code = 468; 

        foreach ( $request->item as $order => $id ) {
            $menu = menuSub::findOrFail($id);
            $menu->position = $order;
            if ( $menu->save() ) {
                $code = 200;
            }
        }



        return $code;
    }

    public function statusSub(Request $request)
    {   
        $id = (int) $request->id;
        $status = $request->status;

        $code = 418; 

        if ( $id and (($status == 'Ativo') || ($status == 'Inativo')) ) {
            $module = MenuSub::findOrFail($id);
            $module->status = $status;
            if ($module->save()) $code = 200;
        }
        
        return $code;
    }

    public function destroySub($id)
    {
        $menu = menuSub::findOrFail($id);
        
        $menu->delete();


        session()->flash('success', "Módulo excluído com sucesso.");
        return redirect()->back();
    }

}