<?php

namespace App\Http\Controllers\Backend;

use App\Models\CategoryBag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\SocialMedia;

class SocialMediaController extends Controller
{
    public function index()
    {
        $socialMedia = SocialMedia::get();
        return view('socialMedia.index', compact('socialMedia'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        SocialMedia::create($request->all());

        session()->flash('success', 'Rede social cadastrada com sucesso.');

        return redirect()->route('socialMedia.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Ver o usuario para ser atualizado ou falhar se não existir
        $socialMedia = SocialMedia::findOrFail($id);


        // Salva
        $socialMedia->update($request->all());

        // Retorna com mensagem
        session()->flash('success', 'Rede social editada com sucesso.');
        return redirect()->route('socialMedia.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Pega o Banner e deleta
        $socialMedia = SocialMedia::findOrFail($id);
        $socialMedia->delete();
        // Retorna e exibe mensagem
        session()->flash('success', "Rede social excluída com sucesso.");
        return redirect()->back();
    }


    public function status()
    {
        //

    }
}
