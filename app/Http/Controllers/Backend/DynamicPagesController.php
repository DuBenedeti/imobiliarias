<?php

namespace App\Http\Controllers\Backend;

use App\Models\CategoryBag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Data;
use App\Models\DynamicPages;
use App\Models\State;
use Illuminate\Support\Str;
use Image;

class DynamicPagesController extends Controller
{
    public function index()
    {
        $pages = DynamicPages::get();

        return view('dynamic-pages.index', compact('pages'));
    }

    public function create()
    {
        return view('dynamic-pages.create');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = $this->generateSlug($data['title']);
        DynamicPages::create($data);
        //    dd($request->all());
        session()->flash('success', 'Cadastro realizado com sucesso.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $page = DynamicPages::findOrFail($id);

        return view('dynamic-pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = DynamicPages::findOrFail($id);
        $data = $request->all();
        // $data['slug'] = $this->generateSlug($data['title']);

        $page->update($data);

        session()->flash('success', 'Cadastro realizado com sucesso.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = DynamicPages::findOrFail($id);
        $page->delete();
        session()->flash('success', 'Deletado com sucesso.');
        return redirect()->back();
    }

    protected function generateSlug($slug) {

        $slug = Str::slug($slug);

        $slugs = DynamicPages::where('slug', 'like', "$slug%")->get();
        if ( $slugs->count() === 0 ) {
            return $slug;
        }

        $lastSlug       = $slugs->sortBy('id')->last()->slug;
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));

        return $slug . '-' . ($lastSlugNumber + 1);
    }


    public function status(Request $request)
    {
        $id = (int) $request->id;
        $status = $request->status;
        // dd($request->all());

        $code = 418; //I'm a teapot!

        if ($id and (($status == 1) || ($status == 0))) {
            $module = DynamicPages::findOrFail($id);
            $module->status = $status;
            if ($module->save()) $code = 200;
        }

        return $code;
    }

    public function saveImage(Request $request)
    {
        // dd("tet");

        $photo = $request->file('file');
        $imagename = Str::slug('editor-' . uniqid());
        $imagename .= '.' . $request->file('file')->getClientOriginalExtension();


        $destinationPath = public_path('/assets/images/dynamic-pages');
        $photo->move($destinationPath, $imagename);


        $retorno = '/assets/images/dynamic-pages/' . $imagename;

        return $retorno;
    }
}
