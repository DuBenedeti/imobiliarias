<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
   public function login()
   {
       return view('login.index');
   }

   public function logar(Request $request)
   {
       // Verifica se possui o usuario com o e-mail específicado.
       if ($user = User::where('email', $request->email)->where('status', 'Ativo')->first()) {
           //Verifica se as senhas estão iguais
           if (Hash::check($request->password, $user->password)) {
               //Se as senhas estão iguais, loga.
               Auth::login($user);
               return redirect('/sistema');
           } else {
               //Senha incorreta
               return redirect()->back()->withErrors("Senha incorreta, verifique.")->withInput();
           }
       } else {
           // E-mail incorreto.
           return redirect()->back()->withErrors("E-mail incorreto, verifique.");
       }
   }

   public function logout()
   {
       Auth::logout();
       return redirect('/sistema');
   }
}
