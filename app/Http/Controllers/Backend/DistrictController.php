<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\District;
use App\Models\State;
use Illuminate\Support\Facades\Response;

class DistrictController extends Controller
{
	public function index(Request $request)
	{
		$district = District::paginate();
		return view('district.index', compact('district'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$states = State::select('id', 'name')->pluck('name', 'id');
		return view('district.create', ['states' => $states]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{

		$district = District::findOrFail($id);
		$states = State::select('id', 'name')->pluck('name', 'id');

		return view('district.edit', ['district' => $district, 'states' => $states]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		District::where('id', $id)
			->update(['name' => $request->name, 'cep' => $request->cep, 'city_id' => $request->city]);

		session()->flash('success', "Bairro alterado com sucesso.");
		return redirect()->route('district.index');
	}


	public function store(Request $request)
	{
		District::insert(['name' => $request->name, 'cep' => $request->cep, 'city_id' => $request->city]);

		session()->flash('success', "Bairro cadastrado com sucesso.");
		return redirect()->route('district.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$district = District::findOrFail($id);
		try {
			$district->delete();
			session()->flash('success', "Bairro excluído com sucesso.");
			return redirect()->route('district.index');
		} catch (\Throwable $th) {
			session()->flash('error', "Erro ao excluir bairro. Código: ". $th->errorInfo[0]);
			return redirect()->back();
		}

	}

	public function status()
	{ //
	}

	public function getCidades($idState)
	{
		$state = State::find($idState);
		$cities = $state->cities()->getQuery()->get(['id', 'name']);
		return Response::json($cities);
	}
  public function getBairros($idCity)
	{
    $city = City::find($idCity);
		$districts = $city->districts()->getQuery()->get(['id', 'name']);
		return Response::json($districts);
	}

  public function getCondominios($idDistrict)
	{
    $district = District::find($idDistrict);
		$condominiums = $district->condominiums()->getQuery()->get(['id', 'name']);
		return Response::json($condominiums);
	}
}
