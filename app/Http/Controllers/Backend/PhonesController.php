<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Phones;
use Illuminate\Http\Request;

class PhonesController extends Controller
{
    public function index()
    {
        $phones = Phones::get();
        return view('phones.index', compact('phones'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        Phones::create($request->all());

        session()->flash('success', 'Telefone cadastrado com sucesso.');

        return redirect()->route('phones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Ver o usuario para ser atualizado ou falhar se não existir
        $phones = Phones::findOrFail($id);


        // Salva
        $phones->update($request->all());

        // Retorna com mensagem
        session()->flash('success', 'Telefone editado com sucesso.');
        return redirect()->route('phones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Pega o Banner e deleta
        $phones = Phones::findOrFail($id);
        $phones->delete();
        // Retorna e exibe mensagem
        session()->flash('success', "Link excluído com sucesso.");
        return redirect()->back();
    }


    public function status()
    {
        //

    }
}