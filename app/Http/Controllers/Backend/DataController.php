<?php

namespace App\Http\Controllers\Backend;

use App\Models\CategoryBag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Data;
use App\Models\State;
use Illuminate\Support\Str;
use Image;

class DataController extends Controller
{
    public function index()
    {
        $states = State::select('id', 'name')->pluck('name', 'id');

        return view('data.index', compact('states'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Data::findOrFail($id);
        // dd($data);
        $states = State::select('id', 'name')->pluck('name', 'id');

        return view('data.index', compact('data', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        // Ver o usuario para ser atualizado ou falhar se não existir
        $data = Data::findOrFail($id);
        $dados = $request->all();
        $dados['city_id'] = $request->city;
        $dados['state_id'] = $request->state;
        $data->update($dados);
        
        if($request->logo_d){
          $image = self::saveImage($request, 'logo_d');
          $data->logo_d = $image;
          $data->save();
        }
        if($request->logo_footer){
          $image = self::saveImage($request, 'logo_footer');
          $data->logo_footer = $image;
          $data->save();
        }

        if($request->favicon){
          $image = self::saveImage($request, 'favicon');
          $data->favicon = $image;
          $data->save();
        }


        // Salva

        // Retorna com mensagem
        session()->flash('success', 'Cadastro editado com sucesso.');
        return redirect()->route('data.edit', $data->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function status()
    {
        //

    }
    public function saveImage($request, $name)
  {
    $photo = $request->file($name);

    $imagename = Str::slug($name . '-' . uniqid());
    $imagename .= '.' . $photo->getClientOriginalExtension();

    list($largura, $altura) = getimagesize($photo);

    if ($largura < $altura) {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else if ($largura == $altura) {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 560, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else if (($largura > 1000) && ($altura > 700)) {

      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
      $thumb_img = Image::make($thumb_img)->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    } else {
      $thumb_img = Image::make($photo->getRealPath())->fit(560, 373, function ($constraint) {
        $constraint->upsize();
      });
    }

    $destinationPath = public_path('/assets/images/data');
    $photo->move($destinationPath, $imagename);
    return $imagename;
  }
}
