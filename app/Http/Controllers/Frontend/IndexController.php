<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ajustes;
use App\Models\AttributesProperty;
use App\Models\Banner;
use App\Models\Contact;
use App\Models\Data;
use App\Models\District;
use App\Models\DynamicPages;
use App\Models\Property;
use App\Models\PropertyContact;
use App\Models\SocialMedia;
use App\Models\TipoImovel;
use App\Models\TrabalheConosco;
use App\Models\TypeBusiness;
use App\Rules\ReCaptcha;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\NodeVisitor\FirstFindingVisitor;

class IndexController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banners = Banner::where('status', 'Ativo')
            ->where('deleted_at', null)
            ->where('mobile', 'Não')
            ->orderBy('range', 'ASC')
            ->get();
        $ajustes = Ajustes::first();
        $venda = Property::orderBy('created_at', 'DESC')->where('type_business_id', 1)->status(1)->limit($ajustes->maxSale)->get();
        $aluguel = Property::orderBy('created_at', 'DESC')->where('type_business_id', 2)->status(1)->limit($ajustes->maxRent)->get();
        $tipoImovel = TipoImovel::orderBy('name', 'ASC')->get();
        $typeBusiness = TypeBusiness::get();

        $data = Data::first();
        $districts = District::where('city_id', $data->city_id)->get();

        return view('frontend.home.index', compact('banners', 'aluguel', 'venda', 'tipoImovel', 'typeBusiness', 'districts'));
    }

    public function properties(Request $request)
    {
        $min_price = getFloatFromInput((string) $request->min_price);
        $max_price = getFloatFromInput((string) $request->max_price);

        $properties = Property::price($min_price, $max_price)
            ->typeBusiness($request->type_business_id)
            ->typeProperty($request->type_property_id)
            ->district($request->district_id)
            ->status(1)
            ->order($request->orderBy)
            ->paginate(24);

        return view('frontend.properties.index', compact('properties'));
    }

    public function propertyShow($slug)
    {
        $property = Property::where('slug', $slug)->status(1)->first();

        if ($property == null) {
          return redirect()->route('home');
        }
        $properties = Property::where('type_property_id', $property->type_property_id)->where('slug', '!=', $property->slug)->status(1)->limit(4)->get();
        $propertyAttributes = AttributesProperty::where('property_id', $property->id)->get();

        return view('frontend.properties.show', compact('property', 'properties', 'propertyAttributes'));
    }

    public function sendContactProperty(Request $request, $propertyId)
    {
        $property = Property::where('id', $propertyId)->first();

        $contact = $request->all();
        $data = Data::first();
        $name = $contact['name'];
        $phone = $contact['phone'];
        $messagem = (string) $contact['message'];
        $email = $contact['email'];
        // $dateVisit = $contact['dateVisit'];
        // $date = Carbon::createFromDate($dateVisit);
        // $dateVisit = $date->calendar();
        // $contact['dateVisit'] = $dateVisit;
        $contact['property_id'] = $propertyId;
        $email_envio = $data->mail;

        try {
          PropertyContact::create($contact);
          session()->flash('success', 'Entraremos em contato para agendar uma visita');

        } catch (\Throwable $th) {
          session()->flash('error', 'Erro ao salvar mensagem!');

        }
        // try {
        //     //code...

        //     Mail::send(
        //         'frontend.templates.return_property_contact',
        //         [
        //             'name' => $name,
        //             'email' => $email,
        //             'phone' => $phone,
        //             'property' => $property,
        //             'messagem' => $messagem,
        //             'dateVisit' => $dateVisit
        //         ],
        //         function ($m) use ($name, $email_envio, $email, $property) {
        //             $m->from($email, $name);
        //             $m->to($email_envio)->subject('Contato para imóvel - ' . $property->getFrontTitle())->replyTo($email, $name);
        //         }
        //     );


        //     session()->flash('success', 'Entraremos em contato para agendar uma visita');
        // } catch (\Throwable $th) {
        //     session()->flash('error', 'Erro ao enviar mensagem!');
        // }
        return redirect()->back();
    }

    public function dynamicPage($slug)
    {
        $page = DynamicPages::where('slug', $slug)->where('status', 1)->first();
        return view('frontend.dynamic-pages.show', compact('page'));
    }

    public function contato()
  {
    return view('frontend.contato.index');
  }

  public function contactSave(Request $request)
  {
    $request->validate([
      'name' => 'required',
      'email' => 'required|email',
      'phone' => 'required',
      'subject' => 'required',
      'message' => 'required',
      'g-recaptcha-response' => ['required', new ReCaptcha]
    ]);

    $contacts = $request->all();

    $name = $contacts['name'];
    $email = $contacts['email'];
    $phone = $contacts['phone'];

    $data = Data::first();
    $email_envio = $data->mail;

    $subject = $contacts['subject'];
    $message = $contacts['message'];

    Contact::create($contacts);


    Mail::send(
      'frontend.templates.return_contact',
      [
        'name' => $name,
        'email' => $email,
        'phone' => $phone,
        'subject' => $subject,
        'messages' => $message
      ],
      function ($m) use ($name, $email_envio, $email, $subject, $data) {
        $m->from($email, $name);
        $m->to($email_envio)->subject('Contato '.$data->name.' - ' . $name . " - " . $subject . " - " . date("d/m/Y H:i"))->replyTo($email, $name);
        $m->cc($email)->subject('Contato '.$data->name.' - ' . $name . " - " . $subject . " - " . date("d/m/Y H:i"))->replyTo($email, $name);
      }
    );

    session()->flash('success', 'Mensagem enviada com sucesso...');

    return redirect()->route('site.contato');
  }
  public function trabalheConosco()
  {
    return view('frontend.trabalheConosco.index');
  }
  public function trabalheConoscoSave(Request $request)
  {
    $request->validate([
      'name' => 'required',
      'email' => 'required|email',
      'phone' => 'required',
      'g-recaptcha-response' => ['required', new ReCaptcha]
    ]);
    if (!$request['attachment']) {
      return view('frontend.trabalheConosco.index');
    } else {

      if (!is_null($request->file('attachment'))) {
        $filetype = $request->file('attachment')->getClientOriginalExtension();
        $file = $this->uploadCurriculo($request);
        $request['file'] = $file;
        $request['file_type'] = $filetype;
      }

      $contacts = $request->all();

      $data = Data::first();
      $email_envio = $data->mail;

      $name = $contacts['name'];
      $email = $contacts['email'];
      $phone = $contacts['phone'];
      $archive = $contacts['attachment'];


      $trabalheConoscos = TrabalheConosco::create($request->all());

      try {
        //code...

        Mail::send(
            'frontend.templates.return_trabalheConosco',
            [
              'name' => $name,
              'email' => $email,
              'phone' => $phone,
              'archive' => $archive
            ],
            function ($m) use ($name, $email_envio, $email, $data) {
                $m->from($email, $name);
                $m->to($email_envio)->subject('Trabalhe Conosco - '.$data->name.' - ' . $name . " - " . date("d/m/Y H:i"))->replyTo($email, $name);
            }
        );


        session()->flash('success', 'Mensagem enviada com sucesso...');
    } catch (\Throwable $th) {
        session()->flash('error', 'Erro ao enviar mensagem!');
    }

      return redirect()->route('site.trabalheConosco');
    }
    
  }
  public function uploadCurriculo(Request $request)
  {
    $path = public_path('/assets/curriculos/');
    $uploadedFile = $request->file('attachment');
    $filename = Str::slug($request->name) . '-' . uniqid();
    $filename .= '.' . $request->file('attachment')->getClientOriginalExtension();
    $uploadedFile->move($path, $filename);
    return $filename;
  }
}
