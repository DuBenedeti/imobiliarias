<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Property;

class IntegrationsController extends Controller
{

    public function xml(Request $request)
  {

    if (!$request->get('token') AND $request->get('specialties') != 'INT-PORTALDACIDADE') {
      abort(404);
    }
    $properties = Property::where('status', 1)->get();
    $header= '<?xml version="1.0" encoding="UTF-8"?>';
        return response()->view('frontend.integrations.portaldacidade', [
            'properties' => $properties, 
            'header' => $header
        ])->header('Content-Type', 'text/xml');
 
  }
}
