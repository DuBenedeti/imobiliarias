<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PropertiesAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('properties_attributes', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('type_field');
        $table->integer('type_property_id')->unsigned();
        $table->foreign('type_property_id')->references('id')->on('type_property');
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('properties_attributes');
    }
}
