<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('properties', function (Blueprint $table) {
        $table->increments('id');
        $table->string('place');
        $table->string('external_id')->nullable();
        $table->string('number')->nullable();
        $table->integer('district_id')->unsigned();
        $table->foreign('district_id')->references('id')->on('district');
        $table->string('zipcode')->nullable();
        $table->double('price', 12, 2);
        $table->double('meters', 10, 2);
        $table->text('details')->nullable();
        $table->string('image');
        $table->integer('type_business_id')->unsigned();
        $table->foreign('type_business_id')->references('id')->on('type_business');
        $table->integer('type_property_id')->unsigned();
        $table->foreign('type_property_id')->references('id')->on('type_property');
        $table->boolean('status');
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('properties');
    }
}
