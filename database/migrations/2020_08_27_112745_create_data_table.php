<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('type');
            $table->string('corporateName')->nullable();
            $table->string('cnpj');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('logo_m');
            $table->string('logo_d');
            $table->string('mail');
            $table->string('phone');
            $table->string('creci')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
