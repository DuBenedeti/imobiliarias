<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\Type;

class ChangeMetersPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up() {
      if (!Type::hasType('double')) {
          Type::addType('double', FloatType::class);
      }
      Schema::table('properties', function (Blueprint $table) {
        $table->double('meters', 10, 2)->nullable()->change();
      }); 
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if (!Type::hasType('double')) {
        Type::addType('double', FloatType::class);
      }
      Schema::table('properties', function (Blueprint $table) {
        $table->double('meters', 10, 2)->change();
      }); 
    }
}
