<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colors', function (Blueprint $table) {
            $table->id();
            $table->string('botoes');
            $table->string('icones');
            $table->string('chapeu');
            $table->string('links');
            $table->string('barraTopo');
            $table->string('textoTopo');
            $table->string('textoTopoHover');
            $table->string('topIconesMidiasSociais');
            $table->string('topIconesMidiasSociaisHover');
            $table->string('iconesLinks');
            $table->string('barraRodape1');
            $table->string('barraRodape2');
            $table->string('textoRodape');
            $table->string('textoRodapeHover');
            $table->string('footerIconesMidiasSociais');
            $table->string('footerIconesMidiasSociaisHover');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colors');
    }
}
