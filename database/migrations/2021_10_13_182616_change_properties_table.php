<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('properties', function (Blueprint $table) {
        $table->double('total_area', 10, 2);
        $table->string('slug')->nullable();
        $table->string('video')->nullable();
        $table->softDeletes('deleted_at', 0);
      });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('properties', function (Blueprint $table) {
        $table->dropColumn(['slug', 'video', 'deleted_at']);
      });
    }
}
