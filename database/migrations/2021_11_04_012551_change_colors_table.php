<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('colors', function (Blueprint $table) {
            $table->string('barraFiltro')->nullable();
            $table->string('iconMenu')->nullable();
            $table->string('fundoMenu')->nullable();
            $table->string('textoMenu')->nullable();
            $table->string('hoverMenu')->nullable();
            $table->string('lineMenu')->nullable();
            $table->string('hoverBotaoConteudo')->nullable();
            $table->string('textoBotaoConteudo')->nullable();
            $table->string('hoverTextoBotaoConteudo')->nullable();
      });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('colors', function (Blueprint $table) {
        $table->dropColumn(['barraFiltro', 'iconMenu', 'fundoMenu', 'textoMenu', 'hoverMenu', 'lineMenu', 'hoverBotaoConteudo', 'textoBotaoConteudo', 'hoverTextoBotaoConteudo']);
      });
    }
}
