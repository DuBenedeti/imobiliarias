<?php

use Illuminate\Database\Seeder;

class TypePropertyTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\Models\TipoImovel::create([
      'name' => "Apartamento",
      'slug' => "apartamento",
      'plural_name' => "apartamentos",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Casa",
      'slug' => "casa",
      'plural_name' => "Casas",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Sobrado",
      'slug' => "sobrado",
      'plural_name' => "Sobrado",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Sala Comercial",
      'slug' => "sala-comercial",
      'plural_name' => "Salas Comerciais",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Salão Comercial",
      'slug' => "salao-comercial",
      'plural_name' => "Salões Comerciais",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Chácara",
      'slug' => "chacara",
      'plural_name' => "Chácaras",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Fazenda",
      'slug' => "fazenda",
      'plural_name' => "Fazendas",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Sítio",
      'slug' => "sitio",
      'plural_name' => "Sítios",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Kitnet",
      'slug' => "kitnet",
      'plural_name' => "Kitnetes",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Loft",
      'slug' => "loft",
      'plural_name' => "Lofts",
    ]);
    \App\Models\TipoImovel::create([
      'name' => "Terreno",
      'slug' => "terreno",
      'plural_name' => "Terrenos",
    ]);
  }
}
