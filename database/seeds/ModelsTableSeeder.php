<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_type' => 'App\User',
            'model_id' => 1
        ]);
        DB::table('model_has_roles')->insert([
            'role_id' => 2,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);

        DB::table('model_has_permissions')->insert([
            'permission_id' => 1,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 2,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 3,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 4,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 5,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 6,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 7,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 8,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 9,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 10,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
        DB::table('model_has_permissions')->insert([
            'permission_id' => 11,
            'model_type' => 'App\User',
            'model_id' => 2
        ]);
    }
}
