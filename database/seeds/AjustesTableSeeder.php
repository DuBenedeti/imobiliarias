<?php

use App\Models\Ajustes;
use Illuminate\Database\Seeder;

class AjustesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ajustes::create([
            'maxRent'=> 4,
            'maxSale'=> 4
        ]);

    }
}
