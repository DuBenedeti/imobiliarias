<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ModuleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DataTableSeeder::class);
        $this->call(TypePropertyTableSeeder::class);
        $this->call(ColorTableSeeder::class);
        $this->call(TypeBusinessTableSeeder::class);
        $this->call(AjustesTableSeeder::class);
    }
}
