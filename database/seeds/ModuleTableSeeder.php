<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\Models\Modules::create([
      'name' => 'Usuários',
      'position' => 1,
      'url' => '/users',
      'icon' => 'user',
      'slug' => 'users'
    ]);
    \App\Models\Modules::create([
      'name' => 'Banners',
      'position' => 2,
      'url' => '/banners',
      'icon' => 'image',
      'slug' => 'banners'
    ]);
    \App\Models\Modules::create([
      'name' => 'Tipo de imóvel',
      'position' => 4,
      'url' => '/type-property',
      'icon' => 'home',
      'slug' => 'tipo-de-imovel'
    ]);
    \App\Models\Modules::create([
      'name' => 'Contato',
      'position' => 10,
      'url' => '/contact',
      'icon' => 'envelope',
      'slug' => 'contato'
    ]);
    \App\Models\Modules::create([
      'name' => 'Módulos',
      'position' => 0,
      'url' => '/modules',
      'icon' => 'sitemap',
      'slug' => 'modulos'
    ]);
    \App\Models\Modules::create([
      'name' => 'Trabalhe Conosco',
      'position' => 11,
      'url' => '/work-with-us',
      'icon' => 'briefcase',
      'slug' => 'trabalhe-conosco'
    ]);
    \App\Models\Modules::create([
      'name' => 'Bairros',
      'position' => 7,
      'url' => '/district',
      'icon' => 'map-marker-alt',
      'slug' => 'bairros'
    ]);
    \App\Models\Modules::create([
      'name' => 'Condomínios',
      'position' => 8,
      'url' => '/condominium',
      'icon' => 'map-marker-alt',
      'slug' => 'condominio'
    ]);
    \App\Models\Modules::create([
      'name' => 'Atributo dos imóveis',
      'position' => 5,
      'url' => '/properties-attribute',
      'icon' => 'home',
      'slug' => 'atributo-dos-imoveis'
    ]);
    \App\Models\Modules::create([
      'name' => 'Imóveis',
      'position' => 3,
      'url' => '/properties',
      'icon' => 'home',
      'slug' => 'imoveis'
    ]);
    \App\Models\Modules::create([
      'name' => 'Páginas dinâmicas',
      'position' => 9,
      'url' => '/dynamic-pages',
      'icon' => 'fas fa-pager',
      'slug' => 'imoveis'
    ]);
    \App\Models\Modules::create([
      'name' => 'Contato dos imóveis',
      'position' => 6,
      'url' => '/property-contacts',
      'icon' => 'envelope',
      'slug' => 'contato-dos-imoveis'
    ]);
  }
}
