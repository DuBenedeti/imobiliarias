<?php

use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Models\Colors::create([
        'botoes' => "#000000",
        'icones' => "#000000",
        'chapeu' => "#000000",
        'links' => "#000000",
        'barraTopo' => "#000000",
        'textoTopo' => "#000000",
        'textoTopoHover' => "#000000",
        'topIconesMidiasSociais' => "#000000",
        'topIconesMidiasSociaisHover' => "#000000",
        'iconesLinks' => "#000000",
        'barraRodape1' => "#000000",
        'barraRodape2' => "#000000",
        'textoRodape' => "#000000",
        'textoRodapeHover' => "#000000",
        'footerIconesMidiasSociais' => "#000000",
        'footerIconesMidiasSociaisHover' => "#000000",
    ]);
    }
}
