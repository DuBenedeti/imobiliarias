<?php

use Illuminate\Database\Seeder;

class TypeBusinessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
  {
    \App\Models\TypeBusiness::create([
      'name' => "Compra",
    ]);
    \App\Models\TypeBusiness::create([
      'name' => "Aluguel",
    ]);
    \App\Models\TypeBusiness::create([
      'name' => "Temporada",
    ]);
  }
}
