<?php

use Illuminate\Database\Seeder;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Data::create([
            'name' => "Prestige Agência Web",
            'corporateName' => "Prestige Agência Web",
            'cnpj' => "09.625.213/9611-32",
            'address' => "Rua aa",
            'city_id' => "4299",
            'state_id' => "16",
            'type' => "PJ",
            'mail' => "eduardo@prestigetecnologia.com.br",
            'logo_d' => "",
            'phone' => "(44) 98442-6624",
            'creci' => "123456",
        ]);
    }
}
