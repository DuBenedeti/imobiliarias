<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $role = \Spatie\Permission\Models\Role::create([
      'id' => 1,
      'name' => 'Admin'
    ]);

    \Spatie\Permission\Models\Role::create([
      'id' => 2,
      'name' => 'Usuário'
    ]);

    \Spatie\Permission\Models\Permission::create([
      'id' => 1,
      'name' => 'users',
      'translate' => 'Gerenciar Usuário',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 2,
      'name' => 'banners',
      'translate' => 'Gerenciar Banners',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 3,
      'name' => 'tipo-de-imovel',
      'translate' => 'Gerenciar Tipos de Imóveis',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 4,
      'name' => 'contato',
      'translate' => 'Gerenciar Contato',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 5,
      'name' => 'modulos',
      'translate' => 'Gerenciar Módulos',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 6,
      'name' => 'trabalhe-conosco',
      'translate' => 'Gerenciar Trabalhe Conosco',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 7,
      'name' => 'bairros',
      'translate' => 'Gerenciar Bairros',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 8,
      'name' => 'condominio',
      'translate' => 'Gerenciar Condomínio',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 9,
      'name' => 'atributo-dos-imoveis',
      'translate' => 'Gerenciar Atributo dos Imoveis',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 10,
      'name' => 'imoveis',
      'translate' => 'Gerenciar Imóveis',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 11,
      'name' => 'paginas-dinamicas',
      'translate' => 'Gerenciar Páginas dinâmicas',
      'guard_name' => 'web'
    ]);
    \Spatie\Permission\Models\Permission::create([
      'id' => 12,
      'name' => 'contato-dos-imoveis',
      'translate' => 'Gerenciar Contato dos imóveis',
      'guard_name' => 'web'
    ]);


    $role->givePermissionTo('users');
    $role->givePermissionTo('banners');
    $role->givePermissionTo('tipo-de-imovel');
    $role->givePermissionTo('contato');
    $role->givePermissionTo('modulos');
    $role->givePermissionTo('trabalhe-conosco');
    $role->givePermissionTo('bairros');
    $role->givePermissionTo('condominio');
    $role->givePermissionTo('atributo-dos-imoveis');
    $role->givePermissionTo('imoveis');
    $role->givePermissionTo('paginas-dinamicas');
  }
}
