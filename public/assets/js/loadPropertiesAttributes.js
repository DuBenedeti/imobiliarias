var propertyType = document.querySelector("#type_property_id").value;

if(propertyType != 0){
  getAttributesProperty()
  
}


$("select[name=type_property_id]").change(function () {
    var idTypeProperty = $(this).val();
    $("div[name=boolean-attributes]").empty();
    $("div[name=money-attributes]").empty();
    $("div[name=number-attributes]").empty();
    $("div[name=text-attributes]").empty();
    $("div[name=boolean-attributes]").append(
      "Carregando atributos"
    )
    if(idTypeProperty == propertyType ){
      getAttributesProperty()
    }else{
      $("div[name=boolean-attributes]").empty();
      $.get("/sistema/get-property-attributes/type/" + idTypeProperty, function (types) {
          $.each(types, function (key, value) {
            if(value.type_field == 'boolean'){
              $("div[name=boolean-attributes]").append(
                "<div class='checkbox'><input type='checkbox' name='attributes["+value.id+"]' id=boolean_" + value.id + " value=" + value.id + "><label for=boolean_" + value.id + "> " + value.name + "</label></div>"
                );
            }
            if(value.type_field == 'money'){
              $("div[name=money-attributes]").append(
                "<div class='col-md-2 col-xs-12'><div class='form-group'><label for='money_"+value.id+"'>"+ value.name +" <strong>(R$)</strong></label><input data-mask='000.000.000,00' data-mask-reverse='true' type='text' id='money_"+value.id+"' name='money["+value.id+"]' class='form-control money-mask'></div></div>"
                );
            }

            if(value.type_field == 'quantity'){
              $("div[name=number-attributes]").append(
                "<div class='col-md-2 col-xs-12'><div class='form-group'><label for='quantity_"+value.id+"'>"+ value.name +"</label><input type='number' id='quantity_"+value.id+"' name='quantity["+value.id+"]' class='form-control'></div></div>"
                );
            }

            if(value.type_field == 'text'){
              $("div[name=text-attributes]").append(
                "<div class='col-md-12 col-xs-12'><div class='form-group'><label for='text_"+value.id+"'>"+ value.name +"</label><textarea type='text' id='text_"+value.id+"' name='text["+value.id+"]' class='form-control'></textarea></div></div>"
                );
            }
          });
          $('.money-mask').mask('000.000.000,00', {reverse: true});
      });
    }
});


function getAttributesProperty(){
  $("div[name=boolean-attributes]").append(
    "Carregando atributos"
  )
  var propertyId = document.querySelector("#property_id").value;
  $.get("/sistema/get-property-attributes/type/" + propertyType, function (types) {
    $("div[name=boolean-attributes]").empty();
    $("div[name=money-attributes]").empty();
    $("div[name=number-attributes]").empty();
    $("div[name=text-attributes]").empty();
    $.each(types, function (key, type) {
      if(type.type_field == 'boolean'){
        $("div[name=boolean-attributes]").append(
          "<div class='checkbox'><input type='checkbox' name='attributes["+type.id+"]' id=boolean_" + type.id + " value=" + type.id + "><label for=boolean_" + type.id + "> " + type.name + "</label></div>"
          );
      }
      if(type.type_field == 'money'){
        $("div[name=money-attributes]").append(
          "<div class='col-md-2 col-xs-12'><div class='form-group'><label for='money_"+type.id+"'>"+ type.name +" <strong>(R$)</strong></label><input data-mask='000.000.000,00' data-mask-reverse='true' type='text' id='money_"+type.id+"' name='money["+type.id+"]' class='form-control money-mask'></div></div>"
          );
      }

      if(type.type_field == 'quantity'){
        $("div[name=number-attributes]").append(
          "<div class='col-md-2 col-xs-12'><div class='form-group'><label for='quantity_"+type.id+"'>"+ type.name +"</label><input type='number' id='quantity_"+type.id+"' name='quantity["+type.id+"]' class='form-control'></div></div>"
          );
      }

      if(type.type_field == 'text'){
        $("div[name=text-attributes]").append(
          "<div class='col-md-12 col-xs-12'><div class='form-group'><label for='text_"+type.id+"'>"+ type.name +"</label><textarea type='text' id='text_"+type.id+"' name='text["+type.id+"]' class='form-control'></textarea></div></div>"
          );
      }
    });
    $('.money-mask').mask('000.000.000,00', {reverse: true});
  });
  $.get("/sistema/get-attributes-property/"+propertyId, function (types) {
    $.each(types, function (key, type) {
      if(document.querySelector("#boolean_"+type.attribute_id)){
        document.querySelector("#boolean_"+type.attribute_id).checked = true
      }
      if(document.querySelector("#money_"+type.attribute_id)){
        document.querySelector("#money_"+type.attribute_id).value = type.value
      }
      if(document.querySelector("#quantity_"+type.attribute_id)){
        document.querySelector("#quantity_"+type.attribute_id).value = type.value
      }
      if(document.querySelector("#text_"+type.attribute_id)){
        document.querySelector("#text_"+type.attribute_id).value = type.value
      }
    });
  });
}