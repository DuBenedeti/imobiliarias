var cityValue = document.querySelector("#city").getAttribute("data-default");
var stateValue = document.querySelector("#state").getAttribute("data-default");

if (stateValue != "") {
    $.get("/sistema/get-cidades/" + stateValue, function (cities) {
        $("select[name=city]").empty();
        $.each(cities, function (key, value) {
            if (value.id == parseInt(cityValue)) {
                $("select[name=city]").append(
                    "<option value=" +
                        value.id +
                        " selected >" +
                        value.name +
                        "</option>"
                );
            } else {
                $("select[name=city]").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            }
        });
    });
}

$("select[name=state]").change(function () {
    var idState = $(this).val();

    $.get("/sistema/get-cidades/" + idState, function (cities) {
        $("select[name=city]").empty();
        $.each(cities, function (key, value) {
            $("select[name=city]").append(
                "<option value=" + value.id + ">" + value.name + "</option>"
            );
        });
    });
});
