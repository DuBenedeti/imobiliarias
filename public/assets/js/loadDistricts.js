var cityValue = document.querySelector("#city").getAttribute("data-default");
var stateValue = document.querySelector("#state").getAttribute("data-default");
var districtValue = document.querySelector("#district").getAttribute("data-default");

if (cityValue != "" && stateValue != "" ) {
    $.get("/sistema/get-bairros/" + cityValue, function (bairros) {
        $.each(bairros, function (key, value) {
            if (value.id == parseInt(districtValue)) {
                $("select[name=district]").append(
                    "<option value=" +
                        value.id +
                        " selected >" +
                        value.name +
                        "</option>"
                );
            } else {
                $("select[name=district]").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            }
        });
    });
}

$("select[name=city]").change(function () {
    var idCity = $(this).val();

    $.get("/sistema/get-bairros/" + idCity, function (bairros) {
        $("select[name=district]").empty();
        $("select[name=district]").append(
          "<option value='0'>Selecione um bairro</option>"
        );
        $.each(bairros, function (key, value) {
            $("select[name=district]").append(
                "<option value=" + value.id + ">" + value.name + "</option>"
            );
        });
    });
});
