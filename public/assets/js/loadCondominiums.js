var cityValue = document.querySelector("#city").getAttribute("data-default");
var stateValue = document.querySelector("#state").getAttribute("data-default");
var districtValue = document.querySelector("#district").getAttribute("data-default");
var condominiumValue = document.querySelector("#condominium").getAttribute("data-default");

if (cityValue != "" && stateValue != "" && districtValue != "") {

    $.get("/sistema/get-condominios/" + districtValue, function (condominios) {
        $.each(condominios, function (key, value) {
            if (value.id == parseInt(condominiumValue)) {
                $("select[name=condominium]").append(
                    "<option value=" +
                        value.id +
                        " selected >" +
                        value.name +
                        "</option>"
                );
            } else {
                $("select[name=condominium]").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            }
        });
    });
}

$("select[name=district]").change(function () {
    var idDistrict = $(this).val();

    $.get("/sistema/get-condominios/" + idDistrict, function (condominios) {
        $("select[name=condominium]").empty();
        $("select[name=condominium]").append(
          "<option value='0'>Selecione um condomínio</option>"
        );
        $.each(condominios, function (key, value) {
            $("select[name=condominium]").append(
                "<option value=" + value.id + ">" + value.name + "</option>"
            );
        });
    });
});
